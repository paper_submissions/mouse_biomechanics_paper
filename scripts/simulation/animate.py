import os
import pathlib
import sys
from argparse import ArgumentParser

import bpy
import numpy as np
import pandas as pd
from farms_blender.core import muscles, pose, scene, sdf, utils
from farms_blender.core.display import display_farms_types
from farms_blender.core.camera import create_camera
from farms_blender.core.objects import objs_of_farms_types
from farms_data.io import yaml

# Clear the world
utils.clear_world()
# Load animat
model_name, *_, model_objs = sdf.load_sdf(
    "/home/tatarama/projects/work/phd/submissions/mouse_biomechanics_paper/data/models/sdf/right_hindlimb.sdf",
    resources_scale=1e-2
)
# Elements to display. Can be changed later
display = {
    'visual': True,
    'collision': False,
    'link': False,
    'inertial': False,
    'com': False,
    'muscle': True,
    'joint': False,
    'joint_axis': False,
}
display_farms_types(**display)
# Load muscle
muscle_objs = muscles.load_muscles(
    model_name=model_name,
    muscle_config="/home/tatarama/projects/work/phd/submissions/mouse_biomechanics_paper/data/config/muscles/ab.yaml",
    muscle_radius=1e-4
)

# Read results
joint_positions = pd.read_hdf("./Results/physics/joint_positions.h5")
base_positions = pd.read_hdf("./Results/physics/base_position.h5")
muscle_activations = pd.read_hdf("./Results/muscles/activations.h5")

# Animate
print(joint_positions.keys())

frames = np.arange(0, len(joint_positions))

frame_start = 0
frame_end = len(joint_positions)
frame_step = 20

joint_objs = {
        obj["farms_name"]: obj
        for obj, _ in objs_of_farms_types(joint=True)
}

# from IPython import embed; embed()

for frame in frames[frame_start:frame_end:frame_step]:
    print(frame)
    # Set pose
    for joint_name in joint_positions.keys():
        pose.set_joint_pose(
            joint_objs[joint_name], joint_positions[joint_name][frame],
        )
    # Set muscle activation
    for muscle_name, values in muscle_activations.items():
        muscle_obj = bpy.data.objects[
            muscle_name.replace("stim_", "")
        ]
        muscle_obj.data.materials[0].diffuse_color[0] = values[frame]
        muscle_obj.data.materials[0].keyframe_insert(data_path="diffuse_color", frame=frame)

    # Set key frame for display objects
    for obj in model_objs:
        obj.keyframe_insert(data_path="location", frame=frame)
        obj.keyframe_insert(data_path="rotation_euler", frame=frame)

# Add sun
sun = bpy.data.lights.new(name="sun", type='SUN')
sun.energy = 2.0
sun.angle = 3.14
sun_obj = bpy.data.objects.new(name="sun", object_data=sun)
bpy.context.collection.objects.link(sun_obj)

# background light
bpy.data.worlds['World'].node_tree.nodes['Background'].inputs[1].default_value = 1.0

# Add cameras
cam = create_camera(
    name="Camera_S", location=(-0.107971, 0, -0.02723),
    rotation_euler=(np.deg2rad(90), 0, np.deg2rad(-90))
)
bpy.context.scene.camera = cam
cam.data.lens=50
cam.data.clip_start=1e-3
cam = create_camera(
    name="Camera_F", location=(0, 0.107971, -0.02723),
    rotation_euler=(np.deg2rad(90), 0, np.deg2rad(180))
)
bpy.context.scene.camera = cam
cam.data.lens=50
cam.data.clip_start=1e-3
cam = create_camera(
    name="Camera_T", location=(0, 0, 0.107971),
    rotation_euler=(0, 0, 0)
)
bpy.context.scene.camera = cam
cam.data.lens=50
cam.data.clip_start=1e-3

# Enable multiple views
bpy.data.scenes["Scene"].render.use_multiview = True
bpy.data.scenes['Scene'].render.views_format = 'MULTIVIEW'
bpy.ops.scene.render_view_add()
bpy.context.scene.render.views[0].camera_suffix = "_S"
bpy.context.scene.render.views[1].camera_suffix = "_F"
bpy.context.scene.render.views[2].camera_suffix = "_T"

# Output
bpy.context.scene.eevee.taa_render_samples = 4
bpy.context.scene.render.film_transparent = True
bpy.data.scenes['Scene'].render.resolution_x = 1080
bpy.data.scenes['Scene'].render.resolution_y = 1920
bpy.data.scenes['Scene'].render.fps = 1000
bpy.data.scenes['Scene'].frame_start = frame_start
bpy.data.scenes['Scene'].frame_end = frame_end
bpy.data.scenes['Scene'].frame_step = frame_step
bpy.data.scenes['Scene'].render.filepath = "/tmp/test.mp4"
bpy.data.scenes['Scene'].render.image_settings.file_format = 'FFMPEG'
bpy.data.scenes['Scene'].render.image_settings.views_format = 'INDIVIDUAL'
bpy.data.scenes['Scene'].render.ffmpeg.format = 'MPEG4'
bpy.data.scenes['Scene'].render.ffmpeg.constant_rate_factor = 'LOW'
bpy.ops.render.render(animation=True)

# poses = yaml.read_yaml(pose_config_path)['joints']

# pose.set_model_pose(poses, units='degrees')
