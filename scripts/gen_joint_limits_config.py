from farms_data.io.io import write_yaml

limb_joint_limits = {
    #: HIND
    "Hip_flexion": [-30.0, 50.0],
    "Hip_adduction": [-40.0, 20.0],
    "Hip_rotation": [-30.0, 10.0],
    "Knee_flexion": [-145.0, -40.0],
    "Ankle_flexion": [-50.0, 50.0],
    "Ankle_inversion": [-10.0, 10.0],
    "Ankle_adduction": [-30.0, 30.0],
    **{"Metatarsus{}_flexion".format(j): [-5.0, 5.0] for j in range(1, 6)},
    **{"BPhalange{}_flexion".format(j): [-5.0, 5.0] for j in range(1, 6)},
    #: FORE
    "Scapula_rotation": [-10.0, 10.0],
    "Shoulder_flexion": [-45.0, 45.0],
    "Shoulder_adduction": [-40.0, 14.0],
    "Shoulder_rotation": [-36.0, 3.0],
    "Elbow_flexion": [60.0, 153.0],
    "Elbow_supination": [-90.0, 5.0],
    "Wrist_flexion": [-20.0, 50.0],
    "Wrist_adduction": [-20.0, 20.0],
    # "Wrist_inversion": [-20.0, 20.0],
    **{"MetaCarpus{}_flexion".format(j): [-5.0, 5.0] for j in range(1, 6)},
    **{"FPhalange{}_flexion".format(j): [-5.0, 5.0] for j in range(1, 6)},
}
spine_joint_limits = {
    **{"Cervical{}_flexion".format(j): [-9.0, 9.0] for j in range(1, 9)},
    **{"Cervical{}_bending".format(j): [-5.0, 5.0] for j in range(1, 9)},
    **{"Cervical{}_rotation".format(j): [-5.0, 5.0] for j in range(1, 9)},
    **{"Thoracic{}_flexion".format(j): [-5.0, 5.0] for j in range(1, 14)},
    **{"Thoracic{}_bending".format(j): [-5.0, 5.0] for j in range(1, 14)},
    **{"Thoracic{}_rotation".format(j): [-5.0, 5.0] for j in range(1, 14)},
    **{"Lumbar{}_flexion".format(j): [-6.0, 6.0] for j in range(1, 7)},
    **{"Lumbar{}_bending".format(j): [-5.0, 5.0] for j in range(1, 7)},
    **{"Lumbar{}_rotation".format(j): [-5.0, 5.0] for j in range(1, 7)},
}
head_joint_limits = {
    "Head_flexion": [-10.0, 10.0],
    "Head_adduction": [-10.0, 10.0],
    "Head_rotation": [-10.0, 10.0]
}
tail_joint_limits = {
    **{"Tail{}_flexion".format(j): [-20.0, 20.0] for j in range(1, 24)},
    **{"Tail{}_bending".format(j): [-20.0, 20.0] for j in range(1, 24)},
    **{"Tail{}_rotation".format(j): [-20.0, 20.0] for j in range(1, 24)},
}

joint_limits = {
    **spine_joint_limits,
    **head_joint_limits,
    **tail_joint_limits,
    **{f"R{joint}": value
       for joint, value in limb_joint_limits.items()
    },
    **{f"L{joint}": [value[0]*-1, value[1]*-1] if any(
               [dof in joint
                for dof in (
                        "adduction", "abduction", "rotation", "inversion",
                        "supination"
                )]
       ) else value
       for joint, value in limb_joint_limits.items()
    }
}


def main():
    """Main"""
    write_yaml(joint_limits, "../data/config/joint_limits.yaml")


if __name__ == '__main__':
    main()
