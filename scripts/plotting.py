""" Generate plots """

import matplotlib.pyplot as plt
from matplotlib.colors import to_rgba
import numpy as np
import os
import pandas as pd
import h5py
import yaml

from tqdm import tqdm

import farms_pylog as pylog

#: Set plot properties
plt.rcParams.update({'font.size': 14})
plt.rc('font', family='serif')
plt.rc('xtick', labelsize='x-small')
plt.rc('ytick', labelsize='x-small')

############### HINDLIMB ###############
HIP_FLEXION_RANGE = [-5, 50]
HIP_ADDUCTION_RANGE = [-40, 20]
HIP_ROTATION_RANGE = [-10, 30]
KNEE_FLEXION_RANGE = [-110, -60]
ANKLE_FLEXION_RANGE = [-50, 50]
ANKLE_INVERSION_RANGE = [-20, 40]


def save_figure(figure_handle, file_path, file_name, **kwargs):
    """Save figure

    Parameters
    ----------
    figure_handle : 

    name : 

    **kwargs : 


    Returns
    -------
    out : 

    """
    if kwargs.pop('pdf', True):
        figure_handle.savefig(
            os.path.join(file_path, file_name+'.pdf'),
            dpi=kwargs.get('dpi', 300),
            bbox_inches=kwargs.get('bbox_inches', 'tight')
        )
    if kwargs.pop('png', True):
        figure_handle.savefig(
            os.path.join(file_path, file_name+'.png'),
            dpi=kwargs.get('dpi', 300),
            bbox_inches=kwargs.get('bbox_inches', 'tight')
        )


def plot_hind_limb_muscle_properties():
    """ Plot hind limb muscle moment arm and muscle tendon lengths """
    #: Read hindlimb muscle-joint function
    with open(
            "../data/config/hind_limb_muscles_joint_function.yaml", "r"
    ) as stream:
        hindlimb_muscle_joint = yaml.load(stream, yaml.SafeLoader)
    #: Plotting
    plot_hind_limb_moment_arm_bullet(hindlimb_muscle_joint)
    plot_hind_limb_moment_arm_osim(hindlimb_muscle_joint)
    plot_hind_limb_moment_arm_bullet_osim(hindlimb_muscle_joint)
    plot_hind_limb_moment_arm_bullet_osim_scaled(hindlimb_muscle_joint)
    plot_hind_limb_muscle_tendon_length_bullet(hindlimb_muscle_joint)
    plot_hind_limb_muscle_tendon_length_osim(hindlimb_muscle_joint)
    plot_hind_limb_muscle_tendon_length_bullet_osim(hindlimb_muscle_joint)


def plot_hind_limb_moment_arm_joint_function():
    """ Plot hind limb moment arm based on joint function  """
    with open(
            "../data/config/hind_limb_muscles_joint_function.yaml", "r"
    ) as stream:
        hindlimb_muscle_joint = yaml.load(stream, yaml.SafeLoader)
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    #: Iterate over the data
    pylog.info("Generating hind_limb_moment_arm_joint_function")
    data_path = "../data/hind_limb_muscle_moment_arms/moment_arm_pybullet"
    for joint, muscles in hindlimb_muscle_joint.items():
        fig, ax = plt.subplots()
        ax.set_xlabel(joint)
        ax.set_ylabel("Moment arm [m]")
        ax.grid(False)
        ax.set_title("Moment Arm of all muscles over {}".format(joint))
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "moment_arm"
            )
            data = pd.read_hdf(os.path.join(data_path, file_name))
            ax.plot(np.rad2deg(data[joint]), data[muscle]*1e3, linewidth=2)
            # plt.draw()
        ax.plot(
            np.rad2deg(data[joint]), np.zeros(np.shape(data[joint])),
            '--r', linewidth=2
        )
        plt.text(ax.get_xlim()[0], ax.get_ylim()[1], 'Flexor')
        plt.text(ax.get_xlim()[0], ax.get_ylim()[0], 'Extensor')
        ax.fill_between(np.rad2deg(data[joint]), np.zeros(
            np.shape(data[joint])), ax.get_ylim()[1],
            facecolor=to_rgba('lightcyan'))
        ax.fill_between(np.rad2deg(data[joint]), np.zeros(
            np.shape(data[joint])), ax.get_ylim()[0],
            facecolor=to_rgba('mistyrose'))
        legend = ax.legend(
            tuple(muscles), loc=(1.05, 0.)
        )

        frame = legend.get_frame()
        frame.set_facecolor('1.0')
        frame.set_edgecolor('1.0')
        save_figure(
            figure_handle=fig,
            file_path="../figures/",
            file_name="release_moment_arm_{}".format(joint)
        )
        plt.close()


def plot_hind_limb_moment_arm_comparison():
    """ Plot hind limb moment arm comparison   """
    compare_muscles = {
        "PECT": "Hip_flexion",
        "BFA": "Hip_flexion",
        "SM": "Knee_flexion",
        "VI": "Knee_flexion",
        "MG": "Ankle_flexion",
        "TA": "Ankle_flexion"
    }
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    #: Iterate over the data
    pylog.info("Generating hind_limb_moment_arm_joint_function")
    data_path_bullet = "../data/hind_limb_muscle_moment_arms/moment_arm_pybullet"
    data_path_osim = "../data/hind_limb_muscle_moment_arms/moment_arm_opensim"
    for muscle, joint in compare_muscles.items():
        fig, ax = plt.subplots()
        ax.set_xlabel(joint)
        ax.set_ylabel("Moment arm [m]")
        ax.grid(True)
        ax.set_title("Moment Arm comparison of {}".format(muscle))
        file_name = file_name_format.format(
            muscle, joint, "moment_arm"
        )
        data = pd.read_hdf(os.path.join(data_path_osim, file_name))
        ax.plot(np.rad2deg(data[joint])[::-1], -1 *
                data[muscle][::-1], ".", markersize=3)
        file_name = file_name_format.format(
            muscle, joint, "moment_arm"
        )
        data = pd.read_hdf(os.path.join(data_path_bullet, file_name))
        ax.plot(np.rad2deg(data[joint]), data[muscle], linewidth=2)
        plt.draw()
        fig.savefig(
            "../figures/release_moment_arm_comparison_{}_{}.pdf".format(
                muscle, joint),
            dpi=300,
            bbox_inches='tight'
        )
        fig.savefig(
            "../figures/release_moment_arm_comparison_{}_{}.svg".format(
                muscle, joint),
            dpi=300,
            bbox_inches='tight'
        )
        plt.close()

############### HIND-LIMB MUSCLE MOMENT ARM (PYBULLET) ###############


def plot_hind_limb_moment_arm_bullet(hindlimb_muscle_joint):
    """ Plot hind  limb moment arm for bullet. """
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    #: Iterate over the data
    pylog.info("Generating muscle moment arm plots of bullet")
    data_path = "../data/hind_limb_muscle_moment_arms/moment_arm_pybullet"
    if not os.path.isdir(data_path):
        os.mkdir(data_path)
    for joint, muscles in hindlimb_muscle_joint.items():
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "moment_arm"
            )
            data = pd.read_hdf(os.path.join(data_path, file_name))
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.plot(np.rad2deg(data[joint]), data[muscle], linewidth=4)
            ax.set_xlabel(joint)
            ax.set_ylabel(muscle+" Moment Arm")
            ax.grid(True)
            ax.set_title("Moment Arm {}-{} (Bullet)".format(muscle, joint))
            plt.tight_layout()
            plt.draw()
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/pybullet/{}_{}.pdf".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/pybullet/{}_{}.svg".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            plt.close()


############### HIND-LIMB MUSCLE MOMENT ARM (OPENSIM) ###############
def plot_hind_limb_moment_arm_osim(hindlimb_muscle_joint):
    """ Plot hind  limb moment arm for osim. """
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    pylog.info("Generating muscle moment arm plots of opensim")
    data_path = "../data/hind_limb_muscle_moment_arms/moment_arm_opensim"
    if not os.path.isdir(data_path):
        os.mkdir(data_path)
    for joint, muscles in hindlimb_muscle_joint.items():
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "moment_arm"
            )
            data = pd.read_hdf(os.path.join(data_path, file_name))
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.plot(np.rad2deg(data[joint]), data[muscle], linewidth=4)
            ax.set_xlabel(joint)
            ax.set_ylabel(muscle+" Moment Arm")
            ax.grid(True)
            ax.set_title("Moment Arm {}-{} (Osim)".format(muscle, joint))
            plt.tight_layout()
            plt.draw()
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/opensim/{}_{}.pdf".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/opensim/{}_{}.svg".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            plt.close()


############### HIND-LIMB MUSCLE MOMENT ARM (PYBULLET-OPENSIM) ###############
def plot_hind_limb_moment_arm_bullet_osim(hindlimb_muscle_joint):
    """ Plot hind  limb moment arm for bullet and opensim. """
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    pylog.info("Generating muscle moment arm plots of opensim-pybullet")
    data_path_osim = "../data/hind_limb_muscle_moment_arms/moment_arm_opensim"
    data_path_bullet = "../data/hind_limb_muscle_moment_arms/moment_arm_pybullet"
    if not os.path.isdir(data_path_osim):
        os.mkdir(data_path_osim)
    if not os.path.isdir(data_path_bullet):
        os.mkdir(data_path_bullet)
    for joint, muscles in hindlimb_muscle_joint.items():
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "moment_arm"
            )
            data = pd.read_hdf(os.path.join(data_path_osim, file_name))
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.plot(np.rad2deg(data[joint])[::-1], -
                     1*data[muscle][::-1], linewidth=2)
            data = pd.read_hdf(os.path.join(data_path_bullet, file_name))
            plt.plot(np.rad2deg(data[joint]), data[muscle], linewidth=2)
            ax.set_xlabel(joint)
            ax.set_ylabel(muscle+" Moment Arm")
            ax.grid(True)
            ax.set_title("Moment Arm {}-{} (Osim)".format(muscle, joint))
            plt.legend(("Opensim", "Bullet"))
            plt.tight_layout()
            plt.draw()
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/bullet-opensim/{}_{}.pdf".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/bullet-opensim/{}_{}.svg".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            plt.close()


############### HIND-LIMB MUSCLE MOMENT ARM (PYBULLET-OPENSIM) ###############
def plot_hind_limb_moment_arm_bullet_osim_scaled(hindlimb_muscle_joint):
    """ Plot hind  limb moment arm for bullet and opensim. """
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    pylog.info("Generating muscle moment arm plots of opensim-pybullet")
    data_path_osim = "../data/hind_limb_muscle_moment_arms/moment_arm_opensim"
    data_path_bullet = "../data/hind_limb_muscle_moment_arms/moment_arm_pybullet"
    if not os.path.isdir(data_path_osim):
        os.mkdir(data_path_osim)
    if not os.path.isdir(data_path_bullet):
        os.mkdir(data_path_bullet)
    osim_scale_factor = 17.56e-3  # : Thigh length
    bullet_scale_factor = 25.66e-3  # : Thigh length
    for joint, muscles in hindlimb_muscle_joint.items():
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "moment_arm"
            )
            data = pd.read_hdf(os.path.join(data_path_osim, file_name))
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.plot(
                data[joint][::-1], -1*data[muscle][::-1]/osim_scale_factor,
                linewidth=2
            )
            data = pd.read_hdf(os.path.join(data_path_bullet, file_name))
            plt.plot(
                data[joint], data[muscle]/bullet_scale_factor, linewidth=2
            )
            ax.set_xlabel(joint)
            ax.set_ylabel(muscle+" Moment Arm")
            ax.grid(True)
            ax.set_title("Moment Arm {}-{} (Osim)".format(muscle, joint))
            plt.legend(("Opensim", "Bullet"))
            plt.tight_layout()
            plt.draw()
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/bullet-opensim-scaled/{}_{}.pdf".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            fig.savefig(
                "../figures/hind_limb_muscle_moment_arms/bullet-opensim-scaled/{}_{}.svg".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            plt.close()


############### HIND-LIMB MUSCLE TENDON LENGTH (PYBULLET) ###############
#: Iterate over the data
def plot_hind_limb_muscle_tendon_length_bullet(hindlimb_muscle_joint):
    """ Plot hind  limb muscle tendon length for bullet. """
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    pylog.info("Generating muscle tendon plots of bullet")
    data_path = "../data/hind_limb_muscle_tendon_lengths/muscle_tendon_length_pybullet"
    if not os.path.isdir(data_path):
        os.mkdir(data_path)
    for joint, muscles in hindlimb_muscle_joint.items():
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "muscle_tendon_length"
            )
            data = pd.read_hdf(os.path.join(data_path, file_name))
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.plot(np.rad2deg(data[joint]), data[muscle], linewidth=4)
            ax.set_xlabel(joint)
            ax.set_ylabel(muscle+" Muscle tendon length")
            ax.grid(True)
            ax.set_title(
                "Muscle Tendon Length {}-{} (PYBULLET)".format(muscle, joint))
            plt.tight_layout()
            plt.draw()
            fig.savefig(
                "../figures/hind_limb_muscle_tendon_lengths/pybullet/{}_{}.pdf".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            fig.savefig(
                "../figures/hind_limb_muscle_tendon_lengths/pybullet/{}_{}.svg".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            plt.close()


# ############### HIND-LIMB MUSCLE TENDON LENGTH (OPENSIM) ###############
def plot_hind_limb_muscle_tendon_length_osim(hindlimb_muscle_joint):
    """ Plot hind  limb muscle tendon length for osim. """
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    #: Iterate over the data
    pylog.info("Generating muscle tendon plots of opensim")
    data_path = "../data/hind_limb_muscle_tendon_lengths/muscle_tendon_length_opensim"
    if not os.path.isdir(data_path):
        os.mkdir(data_path)
    for joint, muscles in hindlimb_muscle_joint.items():
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "muscle_tendon_length"
            )
            data = pd.read_hdf(os.path.join(data_path, file_name))
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.plot(np.rad2deg(data[joint]), data[muscle], linewidth=4)
            ax.set_xlabel(joint)
            ax.set_ylabel(muscle+" Muscle tendon length")
            ax.grid(True)
            ax.set_title(
                "Muscle Tendon Length {}-{} (OPENSIM)".format(muscle, joint))
            plt.tight_layout()
            plt.draw()
            fig.savefig(
                "../figures/hind_limb_muscle_tendon_lengths/opensim/{}_{}.pdf".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            fig.savefig(
                "../figures/hind_limb_muscle_tendon_lengths/opensim/{}_{}.svg".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            plt.close()


############### HIND-LIMB MUSCLE MOMENT ARM (PYBULLET-OPENSIM) ###############
def plot_hind_limb_muscle_tendon_length_bullet_osim(hindlimb_muscle_joint):
    """ Plot hind  limb muscle tendon length for bullet osim. """
    #: Default file format naming
    file_name_format = "{}_{}_{}.h5"
    pylog.info("Generating muscle tendon plots of opensim-pybullet")
    data_path_osim = "../data/hind_limb_muscle_tendon_lengths/muscle_tendon_length_opensim"
    data_path_bullet = "../data/hind_limb_muscle_tendon_lengths/muscle_tendon_length_pybullet"
    data_path_scale_factors = "../data/config/hind_limb_muscle_scale_factors.yaml"
    if not os.path.isdir(data_path_osim):
        os.mkdir(data_path_osim)
    if not os.path.isdir(data_path_bullet):
        os.mkdir(data_path_bullet)
    #: Read scale factors
    with open(data_path_scale_factors, "r") as stream:
        muscle_scale_factors = yaml.load(stream, yaml.SafeLoader)

    for joint, muscles in hindlimb_muscle_joint.items():
        for muscle in muscles:
            file_name = file_name_format.format(
                muscle, joint, "muscle_tendon_length"
            )
            data = pd.read_hdf(os.path.join(data_path_osim, file_name))
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.plot(np.rad2deg(data[joint]), data[muscle] *
                     muscle_scale_factors[muscle], linewidth=2)
            data = pd.read_hdf(os.path.join(data_path_bullet, file_name))
            plt.plot(np.rad2deg(data[joint]), data[muscle], linewidth=2)
            ax.set_xlabel(joint)
            ax.set_ylabel(muscle+" Muscle tendon length")
            ax.grid(True)
            ax.set_title(
                "Muscle Tendon Length {}-{} (Osim-Bullet)".format(muscle, joint)
            )
            plt.legend(("Opensim", "Bullet"))
            plt.tight_layout()
            plt.draw()
            fig.savefig(
                "../figures/hind_limb_muscle_tendon_lengths/bullet-opensim/{}_{}.pdf".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            fig.savefig(
                "../figures/hind_limb_muscle_tendon_lengths/bullet-opensim/{}_{}.svg".format(
                    muscle, joint
                ),
                dpi=300,
                bbox_inches='tight'
            )
            plt.close()


if __name__ == '__main__':
    plot_hind_limb_moment_arm_joint_function()
    # plot_hind_limb_muscle_properties()
    # plot_hind_limb_moment_arm_comparison()
