import yaml
import pandas as pd
import numpy as np


def export_yaml(data, file_path):
    """ Internal method that dumps the data to yaml file.

    Parameters
    ----------
    data : <dict>
        Dictionary containing the data to dump
    file_path : <str>
        File path to dump the data

    Returns
    -------
    out : <None>
    """
    with open(file_path, 'w') as file:
        to_write = yaml.dump(
            data, default_flow_style=None,
            explicit_start=True, indent=2, width=80
        )
        file.write(to_write)


def generate_muscle_length_scale_factors(
        muscle_joint, default_pose, export_path=None
):
    """
    Parameters
    ----------
    muscle_joint : <dict>
        Dict of muscle joint used to compute the scaling factor

    Returns
    -------
    scale_factor : <float>
        Scale factor for length between osim and farms model

    """
    scale_factors = {}
    for muscle, joint in muscle_joint.items():
        file_path = (
            "../data/hind_limb_muscle_tendon_lengths/"
            "muscle_tendon_length_{}/{}_{}_muscle_tendon_length.h5"
        )
        muscle_tendon_length_osim_path = (
            "../data/hind_limb_muscle_moment_arms/moment_arm_opensim/"
        )
        muscle_length_osim = pd.read_hdf(
            file_path.format("opensim", muscle, joint)
        )
        muscle_length_bullet = pd.read_hdf(
            file_path.format("pybullet", muscle, joint)
        )
        #: Get the zero angle length
        osim_zero_length = np.interp(
            np.deg2rad(default_pose[joint]),
            muscle_length_osim[joint],
            muscle_length_osim[muscle]
        )
        bullet_zero_length = np.interp(
            np.deg2rad(default_pose[joint]),
            muscle_length_bullet[joint],
            muscle_length_bullet[muscle]
        )
        #: Compute scaling factor
        scale_factors[muscle] = float(bullet_zero_length/osim_zero_length)

    #: Export scale factors
    if export_path:
        export_yaml(scale_factors, export_path)
    return scale_factors


def scale_muscle_config_file(muscle_config, scale_factors):
    """ Scale the muscle config based on the scale factors. """
    for muscle in muscles_config["muscles"].values():
        muscle_root_name = muscle["name"].split("_", maxsplit=2)[-1]
        scale_factor = scale_factors[muscle_root_name]
        # muscle["l_ce0"] = muscle["l_ce0"]*scale_factor
        muscle["l_opt"] = muscle["l_opt"]*scale_factor
        muscle["l_slack"] = muscle["l_slack"]*scale_factor

    #: export farms muscle config
    export_yaml(
        muscles_config,
        "../data/config/fixed_hindlimb_muscle_scaled_config.yaml"
    )


if __name__ == '__main__':
    export_path = "../data/config/fixed_hind_limb_muscle_scale_factors.yaml"
    with open(
            "../data/config/hind_limb_muscle_joint.yaml", "r"
    ) as stream:
        muscles_joints = yaml.load(stream, yaml.SafeLoader)
    with open(
            "../data/config/hind_limb_default_pose.yaml", "r"
    ) as stream:
        default_pose = yaml.load(stream, yaml.SafeLoader)
    scale_factors = generate_muscle_length_scale_factors(
        muscles_joints, default_pose, export_path
    )
    #: Rescale
    with open(
            "../data/config/fixed_hindlimb_muscle_config.yaml", "r"
    ) as stream:
        muscles_config = yaml.load(stream, yaml.SafeLoader)
    scale_muscle_config_file(muscles_config, scale_factors)
