""" Script to extract mouse data from opensim. """

import os

from opensim_extract_data import extract_muscle_data

muscle_joint = [
    ['BFa', 'hip_flx'],
    ['Pec', 'hip_flx'],
    ['SM', 'knee_flx'],
    ['VI', 'knee_flx'],
    ['EDL', 'ankle_add'],
    ['MG', 'ankle_flx'],
    ['TA', 'ankle_flx'],
]

if __name__ == '__main__':
    osim_filepath = "../data/models/rat_hindlimb_2008/rat_hindlimb_v41.osim"
    export_path = "../data/results/muscle_analysis/rat_hindlimb_osim.h5"
    extract_muscle_data(osim_filepath, export_path, muscle_joint)
