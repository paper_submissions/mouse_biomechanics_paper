""" Script to extract mouse data from opensim. """

import os

import h5py

from opensim_extract_data import extract_muscle_data

if __name__ == '__main__':

    data = h5py.File("../data/results/muscle_analysis/main.h5", 'r')
    muscle_joint = [
        [muscle, list(muscle_data.keys())]
        for muscle, muscle_data in data["HIND"].items()
    ]

    osim_filepath = "../data/models/mouse_hindlimb_2018/Mouse_hindlimb_model_2018_v41.osim"
    export_path = "../data/results/muscle_analysis/mouse_hindlimb_osim.h5"
    extract_muscle_data(osim_filepath, export_path, muscle_joint)
