#!/usr/bin/env python

""" Need paraview python to run this script! """

from paraview.simple import *
import argparse
import os

#: Argparse
parser = argparse.ArgumentParser()
parser.add_argument('-d', '--directory', type=str, required=True,
                    help="Enter the path of the directory containing vtk files",
                    )

args = parser.parse_args()

#: Check if directories exists
if (os.path.isdir(args.directory)):
    print("Converting vtk files to stl.... ")
else:
    raise ValueError("The entered path is not a directory")


def recursive_directory_convert(directory):
    """Function to recursively loop over subfolders"""
    for folder in os.listdir(directory):
        current_path = os.path.join(directory, folder)
        if os.path.isdir(current_path):
            recursive_directory_convert(current_path)
        else:
            if(os.path.splitext(os.path.split(current_path)[-1])[-1] == ".vtk"):
                vtk_mesh = LegacyVTKReader(FileNames=[current_path])
                export_path = current_path.replace("vtk", "stl")
                print("Exporting {}".format(export_path))
                SaveData(export_path, proxy=vtk_mesh)


#: Loop over the subfolders inside the main folder
recursive_directory_convert(args.directory)
