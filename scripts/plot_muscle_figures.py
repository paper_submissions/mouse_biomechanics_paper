""" Generate hindlimb muscle plots """

import os
from itertools import cycle, count
from pathlib import Path

from cycler import cycler
import farms_pylog as pylog
import h5py
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib import cm
from matplotlib.colors import LinearSegmentedColormap, to_rgba, Normalize
from matplotlib.offsetbox import AnnotationBbox, OffsetImage
import matplotlib.gridspec as gridspec
from matplotlib.patches import Patch
from mpl_toolkits.axes_grid1 import make_axes_locatable
import seaborn as sns
from tqdm import tqdm

from farms_data.io.yaml import read_yaml
from farms_utils.plotting import PlotType, get_fig_size

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
DATA_PATH = SCRIPT_PATH.joinpath("..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
RESULTS_PATH = DATA_PATH.joinpath("results")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")
FIGURE_PATH = SCRIPT_PATH.joinpath("..", "figures")
FIGURE_DOF_PATH = FIGURE_PATH.joinpath("dof")

# Colors
colors = np.asarray(
    [(0.00392156862745098, 0.45098039215686275, 0.6980392156862745),
     (0.8705882352941177, 0.5607843137254902, 0.0196078431372549),
     (0.00784313725490196, 0.6196078431372549, 0.45098039215686275),
     (0.8352941176470589, 0.3686274509803922, 0.0),
     (0.8, 0.47058823529411764, 0.7372549019607844),
     (0.792156862745098, 0.5686274509803921, 0.3803921568627451),
     (0.984313725490196, 0.6862745098039216, 0.8941176470588236),
     (0.5803921568627451, 0.5803921568627451, 0.5803921568627451),
     (0.9254901960784314, 0.8823529411764706, 0.2),
     (0.33725490196078434, 0.7058823529411765, 0.9137254901960784)]
)

custom_cmap = sns.color_palette("flare", as_cmap=True)

custom_cycler = (
    cycler(color=[cm.get_cmap('tab20')(c) for c in range(20)]) # +
    # cycler(linestyle=[
    #     lstyle
    #     for _, lstyle in zip(
    #             range(len(colors)),
    #             cycle(['-', '--', ':', '-.',])
    #     )
    # ])
)

#: Load default plot params
with open(CONFIG_PATH.joinpath("plotting.yaml"), 'r') as stream:
    plot_params = yaml.load(stream, yaml.FullLoader)
plot_params['text.usetex'] = False
plot_params['axes.prop_cycle'] = custom_cycler
#: Set plot properties
plt.rcParams.update(plot_params)

#: Export path


# replace muscle names
replace_muscle_names = {
    "cranial": "(cr)", "mid": "(mi)", "caudal": "(ca)",
    "dorsal": "(dr)", "ventral": "(ve)"
}

# replacement joint names
replace_joint_names = {
    "Hip_flexion": "HF", "Hip_adduction": "HA", "Hip_rotation": "HR",
    "Knee_flexion": "KF",
    "Ankle_flexion": "AF", "Ankle_adduction": "AA", "Ankle_inversion": "AI",
    "Shoulder_flexion": "SF", "Shoulder_adduction": "SA", "Shoulder_rotation": "SR",
    "Elbow_flexion": "EF", "Elbow_supination": "ES",
    "Wrist_flexion": "WF", "Wrist_adduction": "WA", "Wrist_inversion": "WI"
}

# replace dof names
replace_dof_names = {
    "hip_flexion": "HF", "hip_extension": "HE", "hip_abduction": "HAB",
    "hip_adduction": "HAD", "hip_internal_rotation": "HIR",
    "hip_external_rotation": "HER", "knee_flexion": "KF", "knee_extension": "KE",
    "ankle_dorsiflexion": "AD", "ankle_plantarflexion": "AP", "ankle_adduction": "AAD",
    "ankle_abduction": "ABD", "ankle_invertion": "AIN", "ankle_evertion": "AEV",
    "shoulder_flexion": "SF", "shoulder_extension": "SE",
    "shoulder_adduction": "SAD", "shoulder_abduction": "SBD",
    "shoulder_internal_rotation": "SIR", "shoulder_external_rotation": "SER",
    "elbow_flexion": "EF", "elbow_extension": "EE",
    "elbow_supination": "ES", "elbow_pronation": "EP", "wrist_flexion": "WF",
    "wrist_extension": "WE", "wrist_adduction": "WAD", "wrist_abduction": "WBD",
    "wrist_invertion": "WI", "wrist_evertion": "WE"
}


def adapt_muscle_names_for_plot(names: list):
    """
    Adapt muscle names for plotting
    """
    return [
        name.replace(
            f"_{name.split('_')[-1]}",
            replace_muscle_names.get(name.split('_')[-1])
        )
        if replace_muscle_names.get(name.split('_')[-1])
        else name
        for name in names]


def adapt_joint_names_for_plot(names: list) -> list:
    """ Adapt joint names for plotting by using short hand notation.

    Parameters
    ----------
    names : <list>
        List of original names

    Returns
    -------
    out : <list>
        List of adapted names
    """
    return [
        replace_joint_names[joint] for joint in names
    ]


def save_figure(figure_handle, file_path, file_name, extra_artists=None, **kwargs):
    """Save figure

    Parameters
    ----------
    figure_handle :

    name :

    **kwargs :

    Returns
    -------
    out :

    """
    # check for filepath
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    if not extra_artists:
        extra_artists = []
    # if "extra_artists" in kwargs:
    #     extras = [a.offsetbox for a in kwargs.get('extra_artists')]
    # if "legend" in kwargs:
    #     legend = kwargs.get("legend")
    #     extras += legend if type(legend) is list else [legend]
    pylog.debug(f"Exporting {file_path} - {file_name}")
    if kwargs.pop('pdf', True):
        figure_handle.savefig(
            file_path.joinpath(file_name+'.pdf'),
            dpi=kwargs.get('dpi', 300),
            bbox_inches=kwargs.get('bbox_inches', 'tight'),
            bbox_extra_artists=extra_artists
        )
    if kwargs.pop('png', True):
        figure_handle.savefig(
            file_path.joinpath(file_name+'.png'),
            dpi=kwargs.get('dpi', 300),
            bbox_inches=kwargs.get('bbox_inches', 'tight'),
            bbox_extra_artists=extra_artists
        )


def compute_muscle_joint_group(joint_name, moment, muscle_joint_group):
    moment_min, moment_max = np.min(moment), np.max(moment)
    moment_min_sign, moment_max_sign = (
        np.sign(moment_min), np.sign(moment_max)
    )
    if moment_min_sign == moment_max_sign:
        return {
            group['name']: np.max([abs(moment_max), abs(moment_min)])
            for group in muscle_joint_group[joint_name]
            if group['sign'] == moment_max_sign
        }
    else:
        return {
            group['name']: abs(moment)
            for moment_sign, moment in zip(
                (moment_min_sign, moment_max_sign), (moment_min, moment_max)
            )
            for group in muscle_joint_group[joint_name]
            if group['sign'] == moment_sign
        }


def add_joint_dof_images(ax: plt.Axes, joint_name: str):
    """
    Add images of joint dof under the given axis

    Parameters
    ----------
    ax: <plt.Axes>
        Matplotlib figure axis handle
    joint_name: <str>
        Name of the joint to add the image for
    """
    # axes limits
    ax_xlim = ax.get_xlim()
    ax_ylim = ax.get_ylim()
    # Add images
    im_path_min = FIGURE_DOF_PATH.joinpath(f"{joint_name.lower()}_min.png")
    im_path_max = FIGURE_DOF_PATH.joinpath(f"{joint_name.lower()}_max.png")
    img_min = mpimg.imread(im_path_min)
    img_max = mpimg.imread(im_path_max)
    im_min = OffsetImage(img_min, zoom=0.075)
    ax_lim_points = ax.transData.transform([ax_xlim, ax_ylim])
    ab_min = AnnotationBbox(
        im_min, (ax_xlim[0], ax_ylim[0]),
        xybox=(0.0, -17.5),
        frameon=False, xycoords="data", boxcoords="offset points",
        annotation_clip=False, box_alignment=(0., 1.)
    )
    im_max = OffsetImage(img_max, zoom=0.075)
    ab_max = AnnotationBbox(
        im_max, (ax_xlim[1], ax_ylim[0]),
        xybox=(0.0, -17.5),
        frameon=False, xycoords="data", boxcoords="offset points",
        annotation_clip=False, box_alignment=(1., 1.)
    )
    ax.add_artist(ab_min)
    ax.add_artist(ab_max)
    return ab_min, ab_max


def plot_muscle_groups(ax: plt.Axes, limb: str = "FORE",  group_by: str = "moment"):
    """Plot the group of hindlimb and forelimb muscles."""
    # Safety check
    limb = limb.upper()
    # Read data
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), 'r')
    # Read muscle group config
    muscle_joint_group = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("muscle_groups_definition.yaml")
    )
    # Generate joint dof names
    joint_dof_names = {
        side: [
            dof['name']
            for joint_name, joint in value.items()
            for dof in joint
        ]
        for side, value in muscle_joint_group.items()
    }
    # Compute moment arm and groups
    muscle_moments = {}
    for side, side_data in data.items():
        muscle_moments[side] = {}
        for muscle, muscle_data in side_data.items():
            _unsorted_moments = {
                key: value
                for joint in muscle_data.attrs["active_joints"]
                for key, value in compute_muscle_joint_group(
                        joint.lower(), muscle_data[joint][group_by],
                        muscle_joint_group[side.lower()]
                ).items()
            }
            # sort the moments
            muscle_moments[side][muscle] = dict(
                sorted(
                    _unsorted_moments.items(),
                    key=lambda x: x[1],
                    reverse=True
                )
            )
    # Muscle ordered list
    muscle_group_by_span = {
        'HIND': {
            'Hip': [],
            'Hip_Knee': [],
            'Knee': [],
            'Ankle_Knee': [],
            'Ankle': [],
        },
        'FORE': {
            'Shoulder': [],
            'Elbow_Shoulder': [],
            'Elbow': [],
            'Elbow_Wrist': [],
            'Wrist': [],
        },
    }
    for side, side_data in data.items():
        for muscle, muscle_data in side_data.items():
            spans = '_'.join(
                dict.fromkeys([
                    joint.split('_')[0]
                    for joint in muscle_data.keys()
                ]).keys())
            muscle_group_by_span[side][spans].append(muscle)
    # Compute max dof moment
    max_dof_moment = {
        side: {
            name: np.max(
                [
                    moments.get(name, -1)
                    for muscle, moments in muscle_moments[side.upper()].items()
                ]
            )
            for name in dofs
        }
        for side, dofs in joint_dof_names.items()
    }
    # Create grid
    num_hind_muscles = len(muscle_moments[limb])
    num_hind_groups = len(joint_dof_names[limb.lower()])
    muscle_group_map = np.ones((num_hind_groups, num_hind_muscles, 4))*1.0
    background_map = np.ones((num_hind_groups, num_hind_muscles, 4))*1.0
    # Labels for major ticks
    xlabels = [
        muscle
        for muscle, muscles in muscle_group_by_span[limb].items()
        for muscle in muscles
    ]
    ylabels = joint_dof_names[limb.lower()]
        # replace_dof_names[
        #     name[:-2] + 'ion'
        #     # '_'.join(name.replace('or', 'ion').split('_'))
        # ]
        # for name in

    # Generate map index
    xaxis_index = {
        muscle: j
        for j, muscle in enumerate(xlabels)
    }
    yaxis_index = {
        name: j
        for j, name in enumerate(joint_dof_names[limb.lower()])
    }
    # add grey color for all the joints the muscle spans
    for muscle, muscle_data in data[limb].items():
        for joint in muscle_data.keys():
            for dof in muscle_joint_group[limb.lower()][joint.lower()]:
                muscle_group_map[yaxis_index[dof['name']]][xaxis_index[muscle]] = [
                    0.75, 0.75, 0.75, 0.75]

    # Major ticks
    ax.set_xticks(np.arange(0, num_hind_muscles, 1))
    ax.set_yticks(np.arange(0, num_hind_groups, 1))

    # Labels for major ticks
    ax.set_xticklabels(
        adapt_muscle_names_for_plot(xlabels),
        rotation='vertical'
    )
    ax.set_yticklabels(ylabels)

    # Minor ticks
    ax.set_xticks(np.arange(-.5, num_hind_muscles, 1), minor=True)
    ax.set_yticks(np.arange(-.5, num_hind_groups, 1), minor=True)

    ax.tick_params(which='minor', length=0)
    # repeat axis
    # ax.tick_params(labelright=True)
    # compute extents
    _extent = (0, muscle_group_map.shape[1], muscle_group_map.shape[0], 0)
    # Generate data
    cmap_name = 'cool'
    for muscle, dof_moments in muscle_moments[limb].items():
        for dof, moment in dof_moments.items():
            norm_moment = moment/max_dof_moment[limb.lower()][dof]
            muscle_group_map[yaxis_index[dof]][xaxis_index[muscle]] = custom_cmap(norm_moment)
    # colorbar
    im = ax.imshow(
        muscle_group_map, aspect='equal', alpha=1.0,
        cmap=cm.get_cmap(cmap_name)
    )
    im.set_zorder(0)
    # add background for readability
    for dof, bcolor in zip(joint_dof_names[limb.lower()], cycle((0.5, 1.0))):
        background_map[yaxis_index[dof]][:] = (
            *[bcolor]*3, 0.3
        )
    bim = ax.imshow(
        background_map, interpolation='none', aspect='equal', alpha=0.3
    )
    bim.set_zorder(2)
    # Gridlines based on minor ticks
    ax.grid(
        which='minor', color='w', linestyle='-', linewidth=2, zorder=1,
        axis='x'
    )
    ax.grid(
        which='minor', color='w', linestyle='-', linewidth=2, zorder=3,
        axis='y'
    )


def generate_muscle_groups_paper_figure():
    """
    Generate figure for muscle groups muscle figure.
    """

    # Setup figure size
    fig_size = get_fig_size(
        PlotType.PUBLICATION, journal_name="ieee_access",
        columns="double", height_fraction=1.35
    )
    # Create figure
    fig = plt.figure(figsize=fig_size)
    # Setup gridspec
    gs = gridspec.GridSpec(nrows=3, ncols=2, figure=fig, hspace=0.2)
    # Create subplots axes from gridspec
    # gss = gridspec.GridSpecFromSubplotSpec(
    #     2, 1, subplot_spec=gs[:2, :], hspace=0.0
    # )
    ax_hind_moment_arm = fig.add_subplot(gs[0, :])
    ax_hind_moment = fig.add_subplot(gs[1, :], sharex=ax_hind_moment_arm)
    # gss = gridspec.GridSpecFromSubplotSpec(
    #     1, 2, subplot_spec=gs[-1, :], hspace=0.0
    # )
    gs.update(wspace=0.3) # set the spacing between axes.
    ax_fore_moment_arm = fig.add_subplot(gs[2, 0])
    ax_fore_moment = fig.add_subplot(gs[2, 1]# , sharey=ax_fore_moment_arm
                                     )
    # Create plots
    plot_muscle_groups(ax_hind_moment_arm, limb="HIND", group_by="moment_arm")
    plot_muscle_groups(ax_hind_moment, limb="HIND", group_by="moment")
    plot_muscle_groups(ax_fore_moment_arm, limb="FORE", group_by="moment_arm")
    plot_muscle_groups(ax_fore_moment, limb="FORE", group_by="moment")
    # Additional configuration
    ax_hind_moment.xaxis.tick_top()
    plt.setp(ax_hind_moment.get_xticklabels(), visible=False)
    ax_hind_moment.spines['bottom'].set_visible(False)
    ax_hind_moment.spines['top'].set_visible(True)
    ax_fore_moment_arm.yaxis.tick_left()
    plt.setp(ax_fore_moment.get_yticklabels(), visible=False)
    # ax_fore_moment_arm.spines['left'].set_visible(False)
    ax_fore_moment.spines['left'].set_visible(False)
    ax_fore_moment.set_yticks([])
    # plt.show()
    # Locating current axes
    # Alternative : https://matplotlib.org/stable/gallery/axes_grid1/demo_colorbar_with_inset_locator.html#sphx-glr-gallery-axes-grid1-demo-colorbar-with-inset-locator-py
    # axins = inset_axes(ax2,
    #                width="5%",  # width = 5% of parent_bbox width
    #                height="50%",  # height : 50%
    #                loc='lower left',
    #                bbox_to_anchor=(1.05, 0., 1, 1),
    #                bbox_transform=ax2.transAxes,
    #                borderpad=0,
    #                    )
    # Add Color label
    divider = make_axes_locatable(ax_hind_moment)
    colorbar_axes = divider.append_axes("bottom", size="5%", pad=0.05)
    cbar = fig.colorbar(
        cm.ScalarMappable(
            norm=Normalize(vmin=0.0, vmax=1.0), cmap=custom_cmap
        ),
        cax=colorbar_axes, ticks=np.linspace(0, 1, 6),
        orientation="horizontal"
    )
    cbar.ax.set_xticklabels(['ϵ', '0.2', '0.4', '0.6', '0.8', '1.0'])
    # Add labels
    ax_text = fig.add_subplot(gs[0, :], label='(A)')
    ax_text.axis("off")
    ax_text.text(0.5, 1.025, "(A)", transform=ax_hind_moment_arm.transAxes)
    ax_text = fig.add_subplot(gs[1, :], label='(B)')
    ax_text.axis("off")
    ax_text.text(0.5, 1.05, "(B)", transform=ax_hind_moment.transAxes)
    ax_text = fig.add_subplot(gs[2, 0], label='(C)')
    ax_text.axis("off")
    ax_text.text(0.5, 1.025, "(C)", transform=ax_fore_moment_arm.transAxes)
    ax_text = fig.add_subplot(gs[2, 1], label='(D)')
    ax_text.axis("off")
    ax_text.text(0.5, 1.025, "(D)", transform=ax_fore_moment.transAxes)
    save_figure(
        figure_handle=fig,
        file_path=FIGURE_PATH.joinpath(f"muscle_grouping"),
        file_name=f"muscle_grouping",
        extra_artists=[cbar.ax],
    )


def plot_muscle_grouping_examples(
        ax: plt.Axes, limb: str = "HIND",  dof_name: str = "",
        group_by: str = "moment", **kwargs
):
    """ Plot all muscle grouping examples """
    # safety
    limb = limb.upper()
    dof_name = dof_name.lower()

    # Read data
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), 'r')
    # Read muscle group config
    muscle_joint_group = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("muscle_groups_definition.yaml")
    )
    # Compute moment arm and groups
    muscle_moments = {}
    for muscle, muscle_data in data[limb].items():
        _unsorted_moments = {
            key: value
            for joint in muscle_data.attrs["active_joints"]
            for key, value in compute_muscle_joint_group(
                    joint.lower(), muscle_data[joint][group_by],
                    muscle_joint_group[limb.lower()]
            ).items()
        }
        # sort the moments
        muscle_moments[muscle] = dict(
            sorted(
                _unsorted_moments.items(),
                key=lambda x: x[1],
                reverse=True
            )
        )

    # Plot examples
    def get_joint_from_dof(dof_name, joint_groups):
        for joint, dofs in joint_groups.items():
            for dof in dofs:
                if dof['name'] == dof_name:
                    return joint

    dof_muscles = {
        dof: []
        for muscle, dofs in muscle_moments.items()
        for dof in dofs.keys()
    }
    for muscle, dofs in muscle_moments.items():
        for dof in dofs.keys():
            dof_muscles[dof].append(muscle)
    for muscle in dof_muscles[dof_name]:
        joint = get_joint_from_dof(
            dof_name, muscle_joint_group[limb.lower()]
        ).capitalize()
        ax.plot(
            np.rad2deg(data[limb][muscle][joint]['primary_joint_angles']),
            np.asarray(data[limb][muscle][joint][group_by])*1e3
        )
    ax.set_xlabel(f"{dof_name} [deg]")
    ax.set_ylabel(
        f"{group_by} [{'mm' if group_by == 'moment_arm' else 'N-mm'}]"
    )
    # ax.set_xlim(left=ax_xlim[0], right=ax_xlim[1])
    # add grid
    # ax.grid(True, linestyle='--')
    # ax.autoscale(enable=True, axis='x', tight=True)
    # Legends
    legend = ax.legend(
        adapt_muscle_names_for_plot(dof_muscles[dof_name]), loc=(1.0, 0.0)
    )
    # ax.add_artist(legend)
    # Add images
    extra_artists = [legend]
    if kwargs.pop("dof_images", False):
        ab_min, ab_max = add_joint_dof_images(ax, joint)
        extra_artists.extend([ab_min.offsetbox, ab_max.offsetbox])
    return extra_artists


def generate_muscle_groups_example_paper_figure():
    """ Generate examples figure for muscle grouping based on moment and
    moment-arm """
    # Setup figure size
    fig_size = get_fig_size(
        PlotType.PUBLICATION, journal_name="ieee_access", columns="double",
        fraction=1.25
    )
    # Create figure
    fig = plt.figure(figsize=fig_size)
    # Setup gridspec
    gs = gridspec.GridSpec(
        nrows=2, ncols=2, figure=fig, wspace=0.3, hspace=0.5
    )
    # Add axes
    ax_hind_moment_arm = fig.add_subplot(gs[0, 0])
    ax_hind_moment = fig.add_subplot(gs[1, 0])
    ax_fore_moment_arm = fig.add_subplot(gs[0, 1], sharey=ax_hind_moment_arm)
    ax_fore_moment = fig.add_subplot(gs[1, 1])
    # Create plots
    extra_artists = []
    artists_hind_ma = plot_muscle_grouping_examples(
        ax_hind_moment_arm, limb="HIND", dof_name="hip_flexor",
        group_by="moment_arm", dof_images=True
    )
    artists_hind_m = plot_muscle_grouping_examples(
        ax_hind_moment, limb="HIND", dof_name="hip_flexor",
        group_by="moment"
    )
    artists_fore_ma = plot_muscle_grouping_examples(
        ax_fore_moment_arm, limb="FORE", dof_name="elbow_extensor",
        group_by="moment_arm", dof_images=True
    )
    artists_fore_m = plot_muscle_grouping_examples(
        ax_fore_moment, limb="FORE", dof_name="elbow_extensor",
        group_by="moment"
    )
    extra_artists = [
        *artists_fore_m, *artists_fore_ma, *artists_hind_m,
        *artists_hind_ma
    ]
    # Additional configuration
    ax_hind_moment.xaxis.tick_top()
    ax_fore_moment.xaxis.tick_top()

    ax_hind_moment.spines['bottom'].set_visible(False)
    ax_hind_moment.spines['top'].set_visible(True)
    ax_fore_moment.spines['bottom'].set_visible(False)
    ax_fore_moment.spines['top'].set_visible(True)

    ax_fore_moment_arm.set_ylabel("")
    ax_fore_moment.set_ylabel("")
    ax_hind_moment.set_xlabel("")
    ax_fore_moment.set_xlabel("")

    ax_fore_moment_arm.get_shared_y_axes().join(
        ax_fore_moment_arm, ax_hind_moment_arm
    )

    # Change limits
    ax_hind_moment.set_xlim([-50, 50])
    ax_hind_moment_arm.set_xlim([-50, 50])

    # Add labels
    ax_text = fig.add_subplot(gs[0, 0], label='(A)')
    extra_artists.extend([ax_text])
    ax_text.axis("off")
    ax_text.text(0.5, 1.025, "(A)", transform=ax_hind_moment_arm.transAxes)
    ax_text = fig.add_subplot(gs[0, 1], label='(B)')
    extra_artists.extend([ax_text])
    ax_text.axis("off")
    ax_text.text(0.5, 1.05, "(B)", transform=ax_fore_moment_arm.transAxes)
    ax_text = fig.add_subplot(gs[1, 0], label='(C)')
    extra_artists.extend([ax_text])
    ax_text.axis("off")
    ax_text.text(0.5, -0.05, "(C)", transform=ax_hind_moment.transAxes)
    ax_text = fig.add_subplot(gs[1, 1], label='(D)')
    extra_artists.extend([ax_text])
    ax_text.axis("off")
    ax_text.text(0.5, -0.05, "(D)", transform=ax_fore_moment.transAxes)

    save_figure(
        figure_handle=fig,
        file_path=FIGURE_PATH.joinpath(f"muscle_grouping", "examples"),
        file_name=f"hip_flexor_elbow_extensor",
        extra_artists=extra_artists
    )


def plot_osim_muscle_groups(group_by='moment'):
    """Plot the group of hindlimb muscles from opensim """
    # Read data
    data = h5py.File(
        MUSCLE_RESULTS_PATH.joinpath("mouse_hindlimb_osim.h5"), 'r'
    )
    # Read muscle group config
    muscle_joint_group = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("muscle_groups_definition.yaml")
    )['hind']
    # Generate joint dof names
    joint_dof_names = [
            dof['name']
            for joint_name, joint in muscle_joint_group.items()
            for dof in joint
    ]

    # Compute moment arm and groups
    muscle_moments = {}
    for muscle, muscle_data in data.items():
        _unsorted_moments = {key : value
            for joint in muscle_data.keys()
            for key, value in compute_muscle_joint_group(
                joint.lower(), muscle_data[joint][group_by][joint],
                muscle_joint_group).items()
        }
        # sort the moments
        muscle_moments[muscle] = dict(
            sorted(
                _unsorted_moments.items(),
                key=lambda x : x[1],
                reverse=True
            )
        )
    # Muscle ordered list
    muscle_group_by_span = {
            'Hip': [],
            'Hip_Knee':[],
            'Knee':[],
            'Ankle_Knee':[],
            'Ankle':[],
    }
    for muscle, muscle_data in data.items():
        spans = '_'.join(
            dict.fromkeys([
                joint.split('_')[0]
                for joint in  muscle_data.keys()
            ]).keys())
        muscle_group_by_span[spans].append(muscle)
    # Compute max dof moment
    max_dof_moment = {
        name: np.max(
            [
                moments.get(name, -1)
                for muscle, moments in muscle_moments.items()
            ]
        )
        for name in joint_dof_names
    }
    # Create grid
    num_hind_muscles = len(muscle_moments)
    num_hind_groups = len(joint_dof_names)
    muscle_group_map = np.ones((num_hind_groups, num_hind_muscles, 4))*1.0
    background_map = np.ones((num_hind_groups, num_hind_muscles, 4))*1.0
    # Labels for major ticks
    xlabels = [
        muscle
        for muscle, muscles in muscle_group_by_span.items()
        for muscle in muscles
    ]
    ylabels = [
        ' '.join(name.replace('or', 'ion').split('_'))
        for name in joint_dof_names
    ]
    # Generate map index
    xaxis_index = {
        muscle : j
        for j, muscle in enumerate(xlabels)
    }
    yaxis_index = {
        name : j
        for j, name in enumerate(joint_dof_names)
    }
    # add grey color for all the joints the muscle spans
    for muscle, muscle_data in data.items():
        for joint in muscle_data.keys():
            for dof in muscle_joint_group[joint.lower()]:
                muscle_group_map[yaxis_index[dof['name']]][xaxis_index[muscle]] = [
                    0.8, 0.8, 0.8, 0.3]
    # Plot
    num_subplots = 1
    fig, axs = plt.subplots(
        num_subplots, 1,
        figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="double", fraction=1.0,
            subplots=(num_subplots, 1)
        )
    )
    # Major ticks
    axs.set_xticks(np.arange(0, num_hind_muscles, 1))
    axs.set_yticks(np.arange(0, num_hind_groups, 1))

    # Labels for major ticks
    axs.set_xticklabels(xlabels, rotation='vertical')
    axs.set_yticklabels(ylabels)

    # Minor ticks
    axs.set_xticks(np.arange(-.5, num_hind_muscles, 1), minor=True)
    axs.set_yticks(np.arange(-.5, num_hind_groups, 1), minor=True)

    axs.tick_params(which='minor', length=0)
    # repeat axis
    # axs.tick_params(labelright=True)
    # compute extents
    extent = (0, muscle_group_map.shape[1], muscle_group_map.shape[0], 0)
    # Generate data
    cmap_name = 'cool'
    for muscle, dof_moments in muscle_moments.items():
        for dof, moment in dof_moments.items():
            norm_moment = moment/max_dof_moment[dof]
            muscle_group_map[yaxis_index[dof]][xaxis_index[muscle]] = getattr(
                cm, cmap_name)(norm_moment
            )
    # colorbar
    im = axs.imshow(
        muscle_group_map, interpolation='none', aspect='equal', alpha=1.0,
        cmap=cm.get_cmap(cmap_name)
    )
    im.set_zorder(0)
    # Locating current axes
    divider = make_axes_locatable(axs)
    # creating new axes on the right side of current axes(ax).
    # The width of cax will be 5% of ax and the padding between cax and ax
    # will be fixed at 0.05 inch.
    colorbar_axes = divider.append_axes("right", size="2%", pad=0.1)
    # cbar_ax = fig.add_axes([0.95, 0.15, 0.025, 0.6])
    cbar = fig.colorbar(im, cax=colorbar_axes, ticks=np.linspace(0, 1, 6))
    cbar.ax.set_yticklabels(['ϵ', '0.2', '0.4', '0.6', '0.8', '1.0'])
    # add background for readability
    for dof, bcolor in zip(joint_dof_names, cycle((0.5, 1.0))):
        background_map[yaxis_index[dof]][:] = (
            *[bcolor]*3, 0.3
        )
    bim = axs.imshow(
        background_map, interpolation='none', aspect='equal', alpha=0.3
    )
    bim.set_zorder(2)
    # Gridlines based on minor ticks
    axs.grid(
        which='minor', color='w', linestyle='-', linewidth=2, zorder=1,
        axis='x'
    )
    axs.grid(
        which='minor', color='w', linestyle='-', linewidth=2, zorder=3,
        axis='y'
    )
    save_figure(
        figure_handle=fig,
        file_path=FIGURE_PATH.joinpath(f"osim_{group_by}_by_dof"),
        file_name=f"osim_muscle_group_{group_by}",
        legend=[cbar.ax,]
    )
    plt.close()


def plot_moment_arm():
    """ Plot muscle moment arms. """
    # Read joint locomotion_range
    locomotion_range = read_yaml("../data/config/locomotion_range.yaml")
    # Read muscle function grouping
    muscle_function = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("muscle_groups_definition.yaml")
    )
    # define function to joint dof name
    get_dof_name = lambda joint, direction : (
        joint[0]['name']
        if joint[0]['sign'] == direction else joint[1]['name']
    )
    # data
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), 'r')
    # function
    num_subplots = 1
    for limb_name, limb_data in data.items():
        for muscle_name, muscle_data in limb_data.items():
            for joint_name, joint_data in muscle_data.items():
                fig, axs = plt.subplots(
                    num_subplots, 1,
                    figsize=get_fig_size(
                        PlotType.PUBLICATION, fraction=1.0,
                        height_fraction=1.0,
                        subplots=(num_subplots, 1)
                    )
                )
                axs.set_ylabel(f"{muscle_name} Moment arm [mm]")
                axs.grid(False)
                pylog.debug(f"{muscle_name}_{joint_name}")
                axs.set_xlabel(f"{joint_name} [deg]")
                joint_angle = np.rad2deg(joint_data['primary_joint_angles'])
                moment_arm = np.asarray(joint_data['moment_arm'])*1e3
                axs.plot(joint_angle, moment_arm)
                axs.plot(joint_angle, np.zeros(np.shape(joint_angle)), '--r')
                if joint_name in locomotion_range.get(limb_name, []):
                    joint_range = locomotion_range[limb_name][joint_name]
                    moment_range = axs.get_ylim()
                    axs.plot(
                        np.ones((10,))*joint_range[0],
                        np.linspace(moment_range[0], moment_range[1], 10),
                        '--k'
                    )
                    axs.plot(
                        np.ones((10,))*joint_range[1],
                        np.linspace(moment_range[0], moment_range[1], 10),
                        '--k'
                    )
                plt.text(
                    axs.get_xlim()[0], axs.get_ylim()[1],
                    get_dof_name(
                        muscle_function[limb_name.lower()][joint_name.lower()],
                        np.sign(axs.get_ylim()[1])
                    ).split('_')[-1]
                )
                plt.text(
                    axs.get_xlim()[0], axs.get_ylim()[0],
                    get_dof_name(
                        muscle_function[limb_name.lower()][joint_name.lower()],
                        np.sign(axs.get_ylim()[0])
                    ).split('_')[-1]
                )
                axs.fill_between(joint_angle, np.zeros(
                    np.shape(joint_angle)), axs.get_ylim()[1],
                    facecolor=to_rgba('lightcyan'))
                axs.fill_between(joint_angle, np.zeros(
                    np.shape(joint_angle)), axs.get_ylim()[0],
                    facecolor=to_rgba('mistyrose'))
                #: Add images
                im_path_min = os.path.join(FIGURE_PATH, "{}_min.png")
                im_path_max = os.path.join(FIGURE_PATH, "{}_max.png")
                img_min = mpimg.imread(im_path_min.format(joint_name.lower()))
                img_max = mpimg.imread(im_path_max.format(joint_name.lower()))
                im_min = OffsetImage(img_min, zoom=0.15)
                ax_lim_points = axs.transData.transform([axs.get_xlim(), axs.get_ylim()])
                ab_min = AnnotationBbox(
                    im_min, (axs.get_xlim()[0], axs.get_ylim()[0]),
                    xybox=(0.0, -25.0),
                    frameon=False,
                    xycoords="data", boxcoords="offset points",
                    annotation_clip=False, box_alignment=(0., 1.)
                )
                im_max = OffsetImage(img_max, zoom=0.15)
                ab_max = AnnotationBbox(
                    im_max, (axs.get_xlim()[1], axs.get_ylim()[0]),
                    xybox=(0.0, -25.0),
                    frameon=False, xycoords="data",
                    boxcoords="offset points", annotation_clip=False,
                    box_alignment=(1., 1.)
                )
                axs.add_artist(ab_min)
                axs.add_artist(ab_max)
                save_figure(
                    figure_handle=fig,
                    file_path=FIGURE_PATH.joinpath("moment_arm"),
                    file_name="release_moment_arm_{}_{}".format(
                        muscle_name, joint_name
                    ),
                    extra_artists=[ab_min, ab_max],
                )
                plt.close()
    data.close()


def plot_moment_arm_groups():
    """ Plot hindlimb muscle moment arms. """
    # Read joint locomotion_range
    locomotion_range = read_yaml("../data/config/locomotion_range.yaml")
    # Read muscle function grouping
    muscle_function = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("muscle_groups_definition.yaml")
    )
    # define function to joint dof name
    get_dof_name = lambda joint, direction : (
        joint[0]['name']
        if joint[0]['sign'] == direction else joint[1]['name']
    )
    # data
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), 'r')
    # group muscles based on groups
    muscle_joint_group = {
        side.upper() : {
            dof['name'] : []
                for joint, joint_values in side_values.items()
                for dof in joint_values
        }
        for side, side_values in muscle_function.items()
    }
    for side, side_data in data.items():
        for muscle, muscle_data in side_data.items():
            for joint, joint_data in muscle_data.items():
                joint_groups = compute_muscle_joint_group(
                    joint.lower(), joint_data['moment'],
                    muscle_function[side.lower()]
                )
                for group in joint_groups.keys():
                    muscle_joint_group[side][group].append([muscle, joint])

    for limb_name, values in muscle_joint_group.items():
        for group_name, muscle_joint in values.items():
            fig, ax = plt.subplots(
                1, 1,
                figsize=get_fig_size(
                    PlotType.PUBLICATION, fraction=1.0, subplots=(1, 1)
                )
            )
            ax.grid(False)
            for muscle_name, joint_name in muscle_joint:
                muscle_data = data[limb_name][muscle_name]
                pylog.debug(f"{muscle_name}_{joint_name}")
                # ax.set_title("{} muscles".format(group_name))
                ax.set_xlabel(f"{joint_name} [deg]")
                ax.set_ylabel(f"moment arm [mm]")
                joint_angle = np.rad2deg(
                    muscle_data[joint_name]['primary_joint_angles']
                )
                moment_arm = np.asarray(
                    muscle_data[joint_name]['moment_arm']
                )*1e3
                ax.plot(joint_angle, moment_arm)
            ax.plot(
                joint_angle, np.zeros(np.shape(joint_angle)),
                '--r'
            )
            if joint_name in locomotion_range.get(limb_name, []):
                joint_range = locomotion_range[limb_name][joint_name]
                moment_range = ax.get_ylim()
                ax.plot(
                    np.ones((10,))*joint_range[0],
                    np.linspace(moment_range[0], moment_range[1], 10),
                    '--k'
                )
                ax.plot(
                    np.ones((10,))*joint_range[1],
                    np.linspace(moment_range[0], moment_range[1], 10),
                    '--k'
                )
            #: axes limits
            ax_xlim = [min(joint_angle), max(joint_angle)]
            ax_ylim = ax.get_ylim()
            ax.set_xlim(left=ax_xlim[0], right=ax_xlim[1])
            fill_top = ax.fill_between(joint_angle, np.zeros(
                np.shape(joint_angle)), ax_ylim[1],
                facecolor=to_rgba('lavender'))
            fill_bot = ax.fill_between(joint_angle, np.zeros(
                np.shape(joint_angle)), ax_ylim[0],
                facecolor=to_rgba('mistyrose'))
            #: Legends
            legend_1 = ax.legend(
                [muscle.replace('_', '\n') for muscle, _ in muscle_joint],
                loc=(1.0, 0.0)
            )
            legend_2 = ax.legend(
                (fill_top, fill_bot),
                ('Flexor', 'Extensor'),
                loc=(1.0, 0.9)
            )
            ax.add_artist(legend_1)
            #: Add images
            im_path_min = os.path.join(FIGURE_PATH, "{}_min.png")
            im_path_max = os.path.join(FIGURE_PATH, "{}_max.png")
            img_min = mpimg.imread(im_path_min.format(joint_name.lower()))
            img_max = mpimg.imread(im_path_max.format(joint_name.lower()))
            im_min = OffsetImage(img_min, zoom=0.1)
            ax_lim_points = ax.transData.transform([ax_xlim, ax_ylim])
            ab_min = AnnotationBbox(
                im_min, (ax_xlim[0], ax_ylim[0]),
                xybox=(0.0, -20.0),
                frameon=False,
                xycoords="data", boxcoords="offset points",
                annotation_clip=False, box_alignment=(0., 1.)
            )
            im_max = OffsetImage(img_max, zoom=0.1)
            ab_max = AnnotationBbox(
                im_max, (ax_xlim[1], ax_ylim[0]),
                xybox=(0.0, -20.0),
                frameon=False, xycoords="data",
                boxcoords="offset points", annotation_clip=False,
                box_alignment=(1., 1.)
            )
            ax.add_artist(ab_min)
            ax.add_artist(ab_max)
            save_figure(
                figure_handle=fig,
                file_path=FIGURE_PATH.joinpath("moment_arm_grouped"),
                file_name="release_moment_arm_{}".format(group_name),
                extra_artists=[ab_min, ab_max],
                legend=[legend_1, legend_2]
            )
            plt.close()
    data.close()


def plot_moment_arm_box_plot_subplots():
    """ Plot muscle moment arms as box plots as subplots"""
    # Read joint muscle relationship
    joint_muscle = read_yaml("../data/config/joint_muscle_relationship.yaml")

    data = h5py.File(os.path.join(DATA_PATH, "default_all_muscles.h5"), 'r')
    # Initialize
    labels = [
        # f"{muscle.replace('_', '-')}-{joint.replace('_', ' ')}"
        f"{muscle.replace('_', '-')[:5]}"
        for limb, muscle_joint in joint_muscle.items()
        for muscle_group, muscle_group_data in muscle_joint.items()
        for muscle, joint in muscle_group_data
    ]
    num_muscles_subplot = 25
    num_subplots = round(len(labels)/num_muscles_subplot)
    subplot = -1
    counter = 0
    plot_data = [[] for j in range(num_subplots)]
    fig, axs = plt.subplots(
        num_subplots, 1,
        figsize=get_fig_size(
            PlotType.PUBLICATION, fraction=1.0,
            subplots=(num_subplots, 1)
        )
    )
    #: Generate plots
    for limb, muscle_joint in joint_muscle.items():
        for muscle_group, muscle_group_data in muscle_joint.items():
            for muscle, joint in muscle_group_data:
                joint_data = data[limb][muscle]['mono'][joint]
                if counter % num_muscles_subplot == 0:
                    subplot += 1
                plot_data[subplot].append(joint_data['moment_arm'])
                counter += 1

    for subplot in range(num_subplots):
        axs[subplot].boxplot(plot_data[subplot], showfliers=False)
        axs[subplot].set_xticks(np.arange(num_muscles_subplot))
        axs[subplot].set_xticklabels(
            labels[
                subplot*num_muscles_subplot:(subplot+1)*num_muscles_subplot
            ], rotation='vertical'
        )
    save_figure(
            figure_handle=fig,
            file_path=FIGURE_PATH,
            file_name=f"release_muscle_moment_arm",
            # legend=legend
    )
    plt.close()
    data.close()


def plot_moment_arm_box_plot():
    """ Plot  muscle moment arms as box plots"""
    # Read joint muscle relationship
    joint_muscle = read_yaml("../data/config/joint_muscle_relationship.yaml")

    data = h5py.File(os.path.join(DATA_PATH, "default_all_muscles.h5"), 'r')
    # Initialize
    labels = [
        # f"{muscle.replace('_', '-')}-{joint.replace('_', ' ')}"
        f"{muscle.replace('_', '-')[:5]}"
        for limb, muscle_joint in joint_muscle.items()
        for muscle_group, muscle_group_data in muscle_joint.items()
        for muscle, joint in muscle_group_data
    ]
    muscle_group_colors = {
        f"{muscle_group}" : colors[j%len(colors)]
        for limb, muscle_joint in joint_muscle.items()
        for j, (muscle_group, muscle_group_data) in enumerate(muscle_joint.items())
    }
    num_muscles_subplot = 25
    num_subplots = round(len(labels)/num_muscles_subplot)
    subplot = -1
    counter = 0
    plot_data = [[] for j in range(num_subplots)]
    plot_data_color = [[] for j in range(num_subplots)]
    legend_elements = [{} for j in range(num_subplots)]
    figs = []
    for j in range(num_subplots):
        fig, ax = plt.subplots(
            1, 1,
            figsize=get_fig_size(
            PlotType.PUBLICATION, fraction=0.5,
            subplots=(1, 1)
            )
        )
        figs.append(fig)
    #: Generate plots
    for limb, muscle_joint in joint_muscle.items():
        for muscle_group, muscle_group_data in muscle_joint.items():
            for muscle, joint in muscle_group_data:
                joint_data = data[limb][muscle]['mono'][joint]
                if counter % num_muscles_subplot == 0:
                    subplot += 1
                plot_data[subplot].append(np.asarray(joint_data['moment_arm'])*1e3)
                plot_data_color[subplot].append(muscle_group_colors[muscle_group])
                counter += 1
                legend_elements[subplot][muscle_group] = Patch(
                        facecolor=muscle_group_colors[muscle_group],
                        edgecolor='k', label=muscle_group
                )

    for subplot in range(num_subplots):
        num_muscles = len(plot_data[subplot])+1
        fig = figs[subplot]
        ax = fig.get_axes()[0]
        bplots = ax.boxplot(
            plot_data[subplot], showfliers=False, patch_artist=True,
            boxprops=dict(linewidth=0.5), meanprops=dict(linewidth=0.5),
            flierprops=dict(linewidth=0.5), medianprops=dict(linewidth=0.5)
        )
        ax.plot(
            np.arange(num_muscles), np.zeros((num_muscles,)),
            '--k', linewidth=1.0, alpha=0.3
        )
        ax.set_ylabel("Moment arm [mm]")
        # ax.set_xticks(np.arange(num_muscles_subplot))
        ax.set_xticklabels(
            labels[
                subplot*num_muscles_subplot:(subplot+1)*num_muscles_subplot
            ], rotation='vertical'
        )
        #: colors
        for color, patch in zip(plot_data_color[subplot], bplots['boxes']):
            patch.set_facecolor(color)
        #: legends
        legend = ax.legend(
            handles=legend_elements[subplot].values(), loc='upper right'
        )
        save_figure(
                figure_handle=fig,
                file_path=FIGURE_PATH,
                file_name=f"release_muscle_moment_arm_box_plot_{subplot}",
                legend=legend
        )
        plt.close()
    data.close()


def plot_bi_articular_moment_arm():
    """ Plot hindlimb muscle moment arms. """
    joint_muscle = read_yaml("../data/config/joint_muscle_relationship.yaml")
    locomotion_range = read_yaml("../data/config/locomotion_range.yaml")

    data = h5py.File(os.path.join(DATA_PATH, "default_all_muscles.h5"), 'r')
    for limb_name, limb_data in data.items():
        for muscle_name, muscle_data in limb_data.items():
            if not muscle_data.attrs['is_biarticular']:
                continue
            for primary_joint_name, joint_data in muscle_data['bi'].items():
                pylog.debug(
                    f"Exporting biarticular moment arm figure of "
                    "{muscle_name} and {primary_joint_name}"
                )
                secondary_joint_name = joint_data.attrs.get("secondary_joint_name")
                fig, ax = plt.subplots(
                    1, 1, figsize=get_fig_size(
                        PlotType.PUBLICATION, fraction=0.5, subplots=(1,1)
                    )
                )
                ax.set_ylabel(f"{muscle_name} moment arm [mm]")
                ax.set_xlabel(f"{primary_joint_name} [deg]")
                ax.grid(False)
                # ax.set_title("Moment Arm of {} ".format(primary_joint_name))
                joint_angle = np.rad2deg(joint_data['primary_joint_angles'])
                moment_arm = np.asarray(joint_data['moment_arm'])*1e3
                lines = ax.plot(joint_angle, moment_arm)
                ax.plot(
                    joint_angle, np.zeros(np.shape(joint_angle)),
                    '--r'
                )
                if primary_joint_name in locomotion_range.get(limb_name, []):
                    joint_range = locomotion_range[limb_name][primary_joint_name]
                    moment_range = ax.get_ylim()
                    ax.plot(
                        np.ones((10,))*joint_range[0],
                        np.linspace(moment_range[0], moment_range[1], 10),
                        '--k'
                    )
                    ax.plot(
                        np.ones((10,))*joint_range[1],
                        np.linspace(moment_range[0], moment_range[1], 10),
                        '--k'
                    )

                #: axes limits
                ax_xlim = [min(joint_angle), max(joint_angle)]
                ax_ylim = ax.get_ylim()
                ax.set_xlim(left=ax_xlim[0], right=ax_xlim[1])
                fill_top = ax.fill_between(joint_angle, np.zeros(
                    np.shape(joint_angle)), ax_ylim[1],
                    facecolor=to_rgba('lightcyan'))
                fill_bot = ax.fill_between(joint_angle, np.zeros(
                    np.shape(joint_angle)), ax_ylim[0],
                    facecolor=to_rgba('mistyrose'))
                #: Legends
                legend_1 = ax.legend(
                    lines,
                    np.round(
                        np.rad2deg(joint_data['secondary_joint_angles']),
                        2
                    ),
                    loc=(1.0, 0.0)
                )
                legend_2 = ax.legend(
                    (fill_top, fill_bot),
                    ('Flexor', 'Extensor'),
                    loc=(1.0, 0.9)
                )
                ax.add_artist(legend_1)
                #: Add images
                im_path_min = os.path.join(FIGURE_PATH, "{}_min.png")
                im_path_max = os.path.join(FIGURE_PATH, "{}_max.png")
                img_min = mpimg.imread(im_path_min.format(primary_joint_name.lower()))
                img_max = mpimg.imread(im_path_max.format(primary_joint_name.lower()))
                im_min = OffsetImage(img_min, zoom=0.15)
                ax_lim_points = ax.transData.transform([ax_xlim, ax_ylim])
                ab_min = AnnotationBbox(
                    im_min, (ax_xlim[0], ax_ylim[0]),
                    xybox=(0.0, -25.0),
                    frameon=False,
                    xycoords="data", boxcoords="offset points",
                    annotation_clip=False, box_alignment=(0., 1.)
                )
                im_max = OffsetImage(img_max, zoom=0.15)
                ab_max = AnnotationBbox(
                    im_max, (ax_xlim[1], ax_ylim[0]),
                    xybox=(0.0, -25.0),
                    frameon=False, xycoords="data",
                    boxcoords="offset points", annotation_clip=False,
                    box_alignment=(1., 1.)
                )
                ax.add_artist(ab_min)
                ax.add_artist(ab_max)
                save_figure(
                    figure_handle=fig,
                    file_path=FIGURE_PATH,
                    file_name="release_biarticular_moment_arm_{}_{}_{}".format(
                        muscle_name, primary_joint_name, secondary_joint_name,
                    ),
                    extra_artists=[ab_min, ab_max],
                    legend=[legend_1, legend_2]
                )
                plt.close()
    data.close()


def generate_hind_limb_moment_arm_comparison_paper_figure():
    """ Plot hind limb moment arm comparison   """
    mouse_muscle_joint = {
        "Hip_flexion" : ["PECT", "BFA"],
        "Knee_flexion" : ["SM", "VI"],
        "Ankle_flexion" : ["MG", "TA"],
    }

    # rat_muscle_joint = {
    #     "hip_flx" : ["PEC", "BFa"],
    #     "knee_flx" : ["SM", "VI"],
    #     "ankle_flx" : ["MG", "TA"],
    # }

    #: Iterate over the data
    data_bullet_mouse = h5py.File(
        "../data/results/muscle_analysis/main.h5", "r"
    )
    data_osim_mouse = h5py.File(
        "../data/results/muscle_analysis/mouse_hindlimb_osim.h5", "r"
    )
    # data_osim_rat = h5py.File(
    #     "../data/results/muscle_analysis/rat_hindlimb_osim.h5", "r"
    # )
    bullet_mouse_thigh_length = 24.5
    osim_mouse_thigh_length = 16.25
    # osim_rat_thigh_length = 35.00
    num_subplots = 3
    fig, axs = plt.subplots(
        1, num_subplots, figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="double", subplots=(1, num_subplots)
        )
    )

    for subplot, (mouse_joint, mouse_muscles) in enumerate(mouse_muscle_joint.items()):
        axs[subplot].set_xlabel(f"{mouse_joint} [deg] \n ({chr(65+subplot)})")
        axs[subplot].grid(False)
        line_style = ['-', ':']
        line_color = colors[:2]
        for mouse_muscle, color in zip(mouse_muscles, line_color):
            #: Mouse-bullet
            data = data_bullet_mouse[
                f"/HIND/{mouse_muscle}/{mouse_joint}"
            ]
            axs[subplot].plot(
                np.rad2deg(data["primary_joint_angles"]),
                np.asarray(data["moment_arm"])*1e3/bullet_mouse_thigh_length,
                color=color, linestyle=line_style[0]
            )
            #: Mouse-osim
            data = data_osim_mouse[
                f"/{mouse_muscle}/{mouse_joint}"
            ]
            if "Knee" in mouse_joint:
                joint_angles = np.rad2deg(data["joint_angles"][mouse_joint])
                idx = np.argmin(abs(joint_angles + 40))
                axs[subplot].plot(
                    joint_angles[:idx],
                   np.asarray(data["moment_arm"][mouse_joint])[:idx]*1e3/osim_mouse_thigh_length,
                    color=color, linestyle=line_style[1]
                )
                axs[subplot].set_xlim([-160, -40])
            elif "Hip" in mouse_joint:
                joint_angles = np.rad2deg(data["joint_angles"][mouse_joint])
                idx = np.argmin(abs(joint_angles + 30))
                axs[subplot].plot(
                    joint_angles[idx:],
                   np.asarray(data["moment_arm"][mouse_joint])[idx:]*1e3/osim_mouse_thigh_length,
                    color=color, linestyle=line_style[1]
                )
                axs[subplot].set_xlim([-60, 60])
            else:
                axs[subplot].plot(
                    np.rad2deg(data["joint_angles"][mouse_joint]),
                    np.asarray(data["moment_arm"][mouse_joint])*1e3/osim_mouse_thigh_length,
                    color=color, linestyle=line_style[1]
                )
                axs[subplot].set_xlim([-60, 60])
        # Add zero line
        axs[subplot].plot(
            axs[subplot].get_xlim(), [0.0, 0.0], 'k--', linewidth=0.5,
        )
        legend = axs[subplot].legend(
            [
                f"{muscle} {sim}"
                for muscle in mouse_muscles
                for sim in ['', 'osim']
            ]
        )
        # Set axis limits
        axs[0].set_ylim([-0.3, 0.1])

        # Share axis
        axs[0].get_shared_y_axes().join(axs[0], axs[1])
        axs[0].get_shared_y_axes().join(axs[0], axs[2])
        # Add subfigure label
        axs[0].set_ylabel("Moment arm/thigh length")
    save_figure(
        figure_handle=fig,
        file_path=FIGURE_PATH.joinpath("moment_arm_compare"),
        file_name=f"moment_arm_comparison",
        legend=legend
    )
    plt.close()
    data_osim_mouse.close()
    # data_osim_rat.close()
    data_bullet_mouse.close()


def plot_active_force_joint_angle():
    """ Plot muscle active force- joint angle curves. """
    # Read joint muscle relationship
    joint_muscle = read_yaml("../data/config/joint_muscle_relationship.yaml")
    locomotion_range = read_yaml("../data/config/locomotion_range.yaml")

    data = h5py.File(os.path.join(DATA_PATH, "default_all_muscles.h5"), 'r')
    for limb_name, values in joint_muscle.items():
        limb_data = data[limb_name]
        for group_name, muscle_joint in values.items():
            # fig, ax = plt.subplots()
            fig, ax = plt.subplots()
            ax.set_ylabel("Moment arm [mm]")
            ax.grid(False)
            for muscle_name, joint_name in muscle_joint:
                muscle_data = limb_data[muscle_name]
                pylog.debug(f"{muscle_name}_{joint_name}")
                ax.set_title("Active force of {} muscles".format(group_name))
                ax.set_xlabel(joint_name)
                joint_angle = np.rad2deg(
                    muscle_data['mono'][joint_name]['primary_joint_angles']
                )
                active_force = np.asarray(
                    muscle_data['mono'][joint_name]['active_force']
                )
                ax.plot(joint_angle, active_force)
            if joint_name in locomotion_range.get(limb_name, []):
                joint_range = locomotion_range[limb_name][joint_name]
                moment_range = ax.get_ylim()
                ax.plot(
                    np.ones((10,))*joint_range[0],
                    np.linspace(moment_range[0], moment_range[1], 10),
                    '--k'
                )
                ax.plot(
                    np.ones((10,))*joint_range[1],
                    np.linspace(moment_range[0], moment_range[1], 10),
                    '--k'
                )
            legend = ax.legend([muscle for muscle, _ in muscle_joint], loc=(1.05, 0.))
            #: Add images
            im_path_min = os.path.join(FIGURE_PATH, "{}_min.png")
            im_path_max = os.path.join(FIGURE_PATH, "{}_max.png")
            img_min = mpimg.imread(im_path_min.format(joint_name.lower()))
            img_max = mpimg.imread(im_path_max.format(joint_name.lower()))
            im_min = OffsetImage(img_min, zoom=0.15)
            ax_lim_points = ax.transData.transform([ax.get_xlim(), ax.get_ylim()])
            ab_min = AnnotationBbox(
                im_min, (ax.get_xlim()[0], ax.get_ylim()[0]),
                xybox=(0.0, -25.0),
                frameon=False,
                xycoords="data", boxcoords="offset points",
                annotation_clip=False, box_alignment=(0., 1.)
            )
            im_max = OffsetImage(img_max, zoom=0.15)
            ab_max = AnnotationBbox(
                im_max, (ax.get_xlim()[1], ax.get_ylim()[0]),
                xybox=(0.0, -25.0),
                frameon=False, xycoords="data",
                boxcoords="offset points", annotation_clip=False,
                box_alignment=(1., 1.)
            )
            ax.add_artist(ab_min)
            ax.add_artist(ab_max)
            save_figure(
                figure_handle=fig,
                file_path=FIGURE_PATH,
                file_name="release_active_force_joint_angle{}".format(group_name),
                extra_artists=[ab_min, ab_max],
                legend=legend
            )
            plt.close()
    data.close()


def plot_fiber_length_range(ax: plt.Axes):
    """ Plot muscle fiber length ranges. """

    # Data
    data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), 'r')

    # Read muscle names by dof
    muscles_by_dof = read_yaml(MUSCLE_CONFIG_PATH.joinpath("names_by_dof.yaml"))

    fiber_range = {}
    for limb, limb_data in data.items():
        limb_muscle_range = {}
        for muscle, muscle_data in limb_data.items():
            l_opt = muscle_data.attrs['l_opt']
            min_fiber_length = [
                np.min(joint_data['muscle_fiber_length'])/l_opt
                for joint_data in muscle_data.values()
            ]
            max_fiber_length = [
                np.max(joint_data['muscle_fiber_length'])/l_opt
                for joint_data in muscle_data.values()
            ]
            limb_muscle_range[muscle] = [
                np.min(min_fiber_length), np.max(max_fiber_length)
            ]
        fiber_range[limb] = limb_muscle_range
    labels = []
    counter = 0
    for side, dofs in muscles_by_dof.items():
        for dof, muscles in dofs.items():
            for muscle in muscles:
                frange = fiber_range[side][muscle]
                ax.barh(
                    y=(counter)*0.75,
                    width=frange[1]-frange[0],
                    height=0.25, left=frange[0],
                    color=colors[-1], edgecolor='black', linewidth=0.5
                )
                counter += 1
                labels.append(muscle)

    ax.plot(
        np.ones((10,)),
        np.linspace(ax.get_ylim()[0], ax.get_ylim()[1], 10),
        '--k', zorder=-1, linewidth=1.0, alpha=0.3
    )
    ax.set_ylim([-0.5, counter*0.75])
    ax.set_yticks(np.arange(0, counter)*0.75)
    ax.set_yticklabels(
        adapt_muscle_names_for_plot(labels)
    )
    # ax.set_xticks((ax.get_xlim()[0], 1.0, ax.get_xlim()[1]))
    ax.invert_yaxis()
    ax.set_xticks([])
    # ax.set_xlabel(r'$\tilde{l}_{ce}$')
    data.close()


def plot_force_length_curve(ax):
    """ Plot de-groote force-length curve"""
    # Add force-length curve
    #: Force-Length Constants
    b1 = [0.815, 0.433, 0.1]
    b2 = [1.055, 0.717, 1.0]
    b3 = [0.162, -0.030, 0.354]
    b4 = [0.063, 0.2, 0.0]

    kpe = 4.0
    e0 = 0.6

    l_ce = np.linspace(0.2, 1.6, 100)
    active_force = np.zeros(np.shape(l_ce))
    passive_force = np.zeros(np.shape(l_ce))
    for step, _l_ce in enumerate(l_ce):
        _active_force = 0.0
        for j in range(3):
            _num = -0.5*(_l_ce - b2[j])**2
            _den = (b3[j] + b4[j]*_l_ce)**2
            _active_force += b1[j]*np.exp(_num/_den)
        active_force[step] = _active_force
        passive_force[step] = (
            np.exp(kpe*(_l_ce - 1)/e0) - 1.0)/(np.exp(kpe) - 1.0) if _l_ce > 1.0 else 0.0
    ax.plot(l_ce, active_force)
    ax.plot(l_ce, passive_force)
    ax.plot(l_ce, active_force+passive_force, ':')
    ax.plot((l_ce[0], 1.0), (1.0, 1.0), ':k', linewidth=1.0, alpha=0.3)
    ax.plot((1.0, 1.0), (0.5, 1.0), ':k', linewidth=1.0, alpha=0.3)
    ax.set_xlabel('$l̃_{ce}$')
    ax.set_ylabel('force')
    ax.set_xticks((0.2, 1.0, 1.8))
    legend=ax.legend(
        ('Active', 'Passive', 'Total'),
        loc='lower center',
        framealpha=0.0
    )
    ax.grid(False)
    return [legend]


def generate_fiber_length_range_paper_figure():
    """ Generate figure showing fiber length range for paper. """
    # Create figure
    fig = plt.figure(
        constrained_layout=True, figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="single", height_fraction=2.25
        )
    )
    # Create gridspec
    gs = fig.add_gridspec(4, 1, hspace=0.0)
    ax_fl = fig.add_subplot(gs[:3, 0])
    ax_curve = fig.add_subplot(gs[-1, 0])
    # Make plots
    plot_fiber_length_range(ax_fl)
    plot_force_length_curve(ax_curve)
    # Add labels
    extra_artists = []
    ax_text = fig.add_subplot(gs[:3, 0], label='(A)')
    extra_artists.extend([ax_text])
    ax_text.axis("off")
    ax_text.text(0.475, -0.025, "(A)", transform=ax_fl.transAxes)
    ax_text = fig.add_subplot(gs[-1, 0], label='(B)')
    extra_artists.extend([ax_text])
    ax_text.axis("off")
    ax_text.text(0.475, -0.5, "(B)", transform=ax_curve.transAxes)
    save_figure(
        figure_handle=fig,
        file_path=FIGURE_PATH.joinpath("fiber_length"),
        file_name="muscle_fiber_length_range",
        extra_artists=extra_artists
    )
    plt.close()

def plot_active_force_length():
    """ Plot muscle active force-length curves. """
    # Read joint muscle relationship
    joint_muscle = read_yaml("../data/config/joint_muscle_relationship.yaml")

    data = h5py.File(os.path.join(DATA_PATH, "default_all_muscles.h5"), 'r')
    for limb_name, values in joint_muscle.items():
        limb_data = data[limb_name]
        for group_name, muscle_joint in values.items():
            fig, ax = plt.subplots()
            ax.set_ylabel("Moment arm [mm]")
            ax.grid(False)
            for muscle_name, joint_name in muscle_joint:
                muscle_data = limb_data[muscle_name]
                pylog.debug(f"Active force of {muscle_name}_{joint_name}")
                ax.set_title("Active force of {} muscles".format(group_name))
                ax.set_xlabel(joint_name)
                fiber_length = np.asarray(
                    muscle_data['mono'][joint_name]['muscle_fiber_length']
                )/muscle_data.attrs['l_opt']
                active_force = np.asarray(
                    muscle_data['mono'][joint_name]['active_force']
                )/muscle_data.attrs['f_max']
                ax.plot(fiber_length, active_force)
            legend = ax.legend([muscle for muscle, _ in muscle_joint], loc=(1.05, 0.))
            save_figure(
                figure_handle=fig,
                file_path=FIGURE_PATH,
                file_name="release_active_force_fiber_length{}".format(group_name),
                legend=legend
            )
            plt.close()
    data.close()


def plot_origin_insertion_sensitivity():
    """Plot figures for insertion and origin sensitivity"""
    muscle_joint_relationship = {
        "HIND": [
            ["GM_dorsal", "Hip_rotation"],
            ["PMA", "Hip_flexion"],
            ["SM", "Hip_flexion"],
            ["SM", "Knee_flexion"],
            ["RF", "Knee_flexion"],
            ["TA", "Ankle_flexion"],
            ["LG", "Ankle_flexion"]
        ],
        "FORE": [
            ["BBL", "Shoulder_flexion"],
            ["BBL", "Elbow_flexion"],
            ["FCU", "Wrist_flexion"],
            ["ECU", "Wrist_adduction"],
            ["PQU", "Elbow_supination"]
        ]
    }
    muscle_full_name = {
        "GM_dorsal" : "Gluteus maximus (dorsal)",
        "PMA" : "Psoas Major",
        "SM" : "Semitendinosus",
        "RF" : "Rectus Femoris",
        "TA" : "Tibialis Anterior",
        "LG" : "Lateral Gastrocnemius",
        "BBL" : "Biceps Brachii Long head",
        "FCU" : "Flexor carpi ulnaris",
        "ECU" : "Extensor carpi ulnaris",
        "PQU" : "Pronator quadratus"
    }
    offsets_style = {
        "no_offset" : "-",
        "origin_positive_offset" : '-',
        "origin_negative_offset" : "--",
        "insertion_positive_offset" : "-",
        "insertion_negative_offset" : "--"
    }
    offsets_color = {
        "no_offset" : "black",
        "origin_positive_offset" : 'coral',
        "origin_negative_offset" : "coral",
        "insertion_positive_offset" : "mediumslateblue",
        "insertion_negative_offset" : "mediumslateblue"
    }
    offset_legends = {
        "no_offset" : "no_offset",
        "origin_positive_offset" : f"origin+0.5mm",
        "origin_negative_offset" : f"origin-0.5mm",
        "insertion_positive_offset" : f"insert+0.5mm",
        "insertion_negative_offset" : f"insert-0.5mm",
    }
    #: Read data
    data = h5py.File(
        os.path.join(DATA_PATH, "muscle_attachment_sensitivity_data.h5"), 'r'
    )
    #: Generate plots
    for limb, muscle_joint in muscle_joint_relationship.items():
        for elem in muscle_joint:
            muscle, joint = elem[:]
            fig, ax = plt.subplots(
                1, 1,
                figsize=get_fig_size(
                    PlotType.PUBLICATION, fraction=0.5, subplots=(1, 1)
                )
            )
            ax.set_title(f"{muscle_full_name[muscle]}")
            ax.set_ylabel(f"{joint.replace('_', ' ')} moment [N-mm]")
            ax.set_xlabel(f"{joint.replace('_', ' ')} [deg]")
            ax.grid(False)
            for offset, style in offsets_style.items():
                offset_data = data[limb][muscle][offset][joint]
                ax.plot(
                    np.rad2deg(offset_data["primary_joint_angles"]),
                    np.asarray(offset_data["moment"])*1e3,
                    linestyle=style, color=offsets_color[offset]
                )
            legend = ax.legend(offset_legends.values())
            save_figure(
                figure_handle=fig,
                file_path=FIGURE_PATH,
                file_name=f"release_muscle_attachment_sensitivity_{muscle}_{joint}",
                legend=legend
            )
            plt.close()


def plot_parameter_sensitivity():
    """Plot figures for muscle parameter sensitivity"""
    muscle_joint_relationship = {
        "HIND": [
            ["GM_dorsal", "Hip_rotation"],
            ["PMA", "Hip_flexion"],
            ["SM", "Hip_flexion"],
            ["SM", "Knee_flexion"],
            ["RF", "Knee_flexion"],
            ["TA", "Ankle_flexion"],
            ["LG", "Ankle_flexion"],
        ],
        "FORE": [
            ["BBL", "Shoulder_flexion"],
            ["BBL", "Elbow_flexion"],
            ["FCU", "Wrist_flexion"],
            ["ECU", "Wrist_adduction"],
            ["PQU", "Elbow_supination"]
        ]
    }
    muscle_full_name = {
        "GM_dorsal" : "Gluteus maximus (dorsal)",
        "PMA" : "Psoas Major",
        "SM" : "Semitendinosus",
        "RF" : "Rectus Femoris",
        "TA" : "Tibialis Anterior",
        "LG" : "Lateral Gastrocnemius",
        "BBL" : "Biceps Brachii Long head",
        "FCU" : "Flexor carpi ulnaris",
        "ECU" : "Extensor carpi ulnaris",
        "PQU" : "Pronator quadratus",
    }
    offsets_style = {
        "original" : "k",
        "f_max" : "r",
        "l_opt" : "--b",
        "l_slack" : ".g",
        "pennation" : "-.m"
    }
    #: Read data
    data = h5py.File(
        os.path.join(DATA_PATH, "muscle_parameter_sensitivity_data.h5"), 'r'
    )
    #: Generate plots
    for limb, muscle_joint in muscle_joint_relationship.items():
        for elem in muscle_joint:
            muscle, joint = elem[:]
            fig, ax = plt.subplots(
                1, 1,
                figsize=get_fig_size(
                    PlotType.PUBLICATION, fraction=0.5, subplots=(1, 1)
                )
            )
            ax.set_title(f"{muscle_full_name[muscle]}")
            ax.set_ylabel(f"{joint.replace('_', ' ')} moment [N-mm]")
            ax.set_xlabel(f"{joint.replace('_', ' ')} [deg]")
            ax.grid(False)
            for offset, style in offsets_style.items():
                offset_data = data[limb][muscle][offset][joint]
                ax.plot(
                    np.rad2deg(offset_data["primary_joint_angles"]),
                    np.asarray(offset_data["moment"])*1e3,
                    style
                )
            legend = ax.legend(offsets_style.keys())
            save_figure(
                figure_handle=fig,
                file_path=FIGURE_PATH,
                file_name=f"release_muscle_parameter_sensitivity_{muscle}_{joint}",
                legend=legend
            )
            plt.close()


def plot_salib_parameter_sensitivity():
    """Plot figures for muscle parameter sensitivity"""
    # Read data
    data = h5py.File(
        MUSCLE_RESULTS_PATH.joinpath(
            "muscle_parameter_salib_sensitivity.h5"
        ), 'r'
    )
    # Read names by dof config for ordering muscles
    names_by_dof = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("names_by_dof.yaml")
    )

    # Labels
    muscle_labels = [
        f"{adapt_muscle_names_for_plot([muscle])[0]}-{replace_joint_names[joint]}"
        for side, dofs in names_by_dof.items()
        for dof, muscles in dofs.items()
        for muscle in muscles
        for joint in data[side][muscle].keys()
    ]

    # Initialize
    num_muscles_subplot = 62
    num_subplots = int(np.ceil(len(muscle_labels)/num_muscles_subplot))
    subplot = 0
    counter = count(start=0, step=1)
    s1 = []
    fig, axs = plt.subplots(
        1, num_subplots,
        figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="single", height_fraction=3.0,
            subplots=(1, num_subplots)
        )
    )
    cmap_name = 'cool'
    plot_data = np.empty((num_subplots*num_muscles_subplot, 4, 4))

    #: Generate plots
    for side, dofs in names_by_dof.items():
        for dof, muscles in dofs.items():
            for muscle in muscles:
                for joint, sensitivity in data[side][muscle].items():
                    plot_data[next(counter)] = np.asarray(
                        getattr(cm, cmap_name)(sensitivity['sobol_S1'])
                    )
    # reshape plot data to match subplots
    plot_data = np.reshape(plot_data, (num_subplots, num_muscles_subplot, 4, 4))
    # plot
    for subplot in range(num_subplots):
        im = axs[subplot].imshow(
            plot_data[subplot], interpolation='none',
            aspect='equal', alpha=1.0, cmap=cm.get_cmap(cmap_name)
        )
        # Minor ticks
        axs[subplot].set_xticks(np.arange(-.5, 4, 1), minor=True)
        axs[subplot].set_yticks(np.arange(-.5, num_muscles_subplot, 1),
                                minor=True)
        axs[subplot].tick_params(which='minor', length=0)
        # Major ticks
        axs[subplot].set_xticks(([0, 1, 2, 3]))
        axs[subplot].set_xticklabels(
            (['f_max', 'l_opt', 'l_slack', 'penn']), rotation="vertical"
        )
        axs[subplot].set_yticks(np.arange(num_muscles_subplot))
        axs[subplot].set_yticklabels(
            muscle_labels[
                subplot*num_muscles_subplot:(subplot+1)*num_muscles_subplot
            ], rotation="horizontal"
        )
        axs[subplot].grid(
            which='minor', color='w', linestyle='-', linewidth=2,
            axis='x'
        )
        axs[subplot].grid(
            which='minor', color='w', linestyle='-', linewidth=2,
            axis='y'
        )
    #: color bar
    cbar_ax = fig.add_axes([0.0, 0.0, 1.0, 0.02])
    fig.colorbar(im, cax=cbar_ax, orientation="horizontal")
    # Gridlines based on minor ticks
    save_figure(
            figure_handle=fig,
            file_path=FIGURE_PATH.joinpath("sensitivity", "parameters"),
            file_name=f"release_muscle_parameter_salib_sensitivity",
            # legend=legend
        )
    plt.close()


def plot_salib_attachment_sensitivity():
    """Plot figures for muscle attachment sensitivity"""
    # Read data
    data = h5py.File(
        MUSCLE_RESULTS_PATH.joinpath(
            "muscle_attachment_salib_sensitivity.h5"
        ), 'r'
    )
    # Read names by dof config for ordering muscles
    names_by_dof = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("names_by_dof.yaml")
    )

    # Labels
    muscle_labels = [
        f"{adapt_muscle_names_for_plot([muscle])[0]}-{replace_joint_names[joint]}"
        for side, dofs in names_by_dof.items()
        for dof, muscles in dofs.items()
        for muscle in muscles
        for joint in data[side][muscle].keys()
    ]

    # Initialize
    num_muscles_subplot = 62
    num_subplots = int(np.ceil(len(muscle_labels)/num_muscles_subplot))
    subplot = 0
    counter = count(start=0, step=1)
    s1 = []
    fig, axs = plt.subplots(
        1, num_subplots,
        figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="single", height_fraction=3.0,
            subplots=(1, num_subplots)
        )
    )
    cmap_name = 'cool'
    plot_data = np.empty((num_subplots*num_muscles_subplot, 2, 4))

    #: Generate plots
    for side, dofs in names_by_dof.items():
        for dof, muscles in dofs.items():
            for muscle in muscles:
                for joint, sensitivity in data[side][muscle].items():
                    plot_data[next(counter)] = np.asarray(
                        getattr(cm, cmap_name)(sensitivity['sobol_S1'])
                    )
    # reshape plot data to match subplots
    plot_data = np.reshape(plot_data, (num_subplots, num_muscles_subplot, 2, 4))
    # plot
    for subplot in range(num_subplots):
        im = axs[subplot].imshow(
            plot_data[subplot], interpolation='none',
            aspect='equal', alpha=1.0, cmap=cm.get_cmap(cmap_name)
        )
        # Minor ticks
        axs[subplot].set_xticks(np.arange(-.5, 2, 1), minor=True)
        axs[subplot].set_yticks(np.arange(-.5, num_muscles_subplot, 1),
                                minor=True)
        axs[subplot].tick_params(which='minor', length=0)
        # Major ticks
        axs[subplot].set_xticks(([0, 1]))
        axs[subplot].set_xticklabels(
            (['origin', 'insertion']), rotation="vertical"
        )
        axs[subplot].set_yticks(np.arange(num_muscles_subplot))
        axs[subplot].set_yticklabels(
            muscle_labels[
                subplot*num_muscles_subplot:(subplot+1)*num_muscles_subplot
            ], rotation="horizontal"
        )
        axs[subplot].grid(
            which='minor', color='w', linestyle='-', linewidth=2,
            axis='x'
        )
        axs[subplot].grid(
            which='minor', color='w', linestyle='-', linewidth=2,
            axis='y'
        )
    #: color bar
    cbar_ax = fig.add_axes([0.0, 0.0, 1.0, 0.02])
    fig.colorbar(im, cax=cbar_ax, orientation="horizontal")
    # Gridlines based on minor ticks
    save_figure(
            figure_handle=fig,
            file_path=FIGURE_PATH.joinpath("sensitivity", "attachments"),
            file_name=f"release_muscle_attachment_salib_sensitivity",
            # legend=legend
        )
    plt.close()


def plot_salib_sensitivity():
    """Plot figures for muscle sensitivity"""
    # Read data
    attach_data = h5py.File(
        MUSCLE_RESULTS_PATH.joinpath(
            "muscle_attachment_salib_0.5mm_sensitivity.h5"
        ), 'r'
    )
    param_data = h5py.File(
        MUSCLE_RESULTS_PATH.joinpath(
            "muscle_parameter_salib_sensitivity.h5"
        ), 'r'
    )
    # Read names by dof config for ordering muscles
    names_by_dof = read_yaml(
        MUSCLE_CONFIG_PATH.joinpath("names_by_dof.yaml")
    )

    # Labels
    muscle_labels = [
        f"{adapt_muscle_names_for_plot([muscle])[0]}-{replace_joint_names[joint]}"
        for side, dofs in names_by_dof.items()
        for dof, muscles in dofs.items()
        for muscle in muscles
        for joint in attach_data[side][muscle].keys()
    ]
    print(len(muscle_labels))
    # Initialize
    num_muscles_subplot = 45
    num_subplots = int(np.ceil(len(muscle_labels)/num_muscles_subplot))
    subplot = 0
    counter = count(start=0, step=1)
    s1 = []
    fig, axs = plt.subplots(
        1, num_subplots,
        figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="double", fraction=1.25, height_fraction=1.0,
            subplots=(1, num_subplots)
        )
    )
    plot_data = np.zeros((num_subplots*num_muscles_subplot, 6, 4))

    #: Generate plots
    for side, dofs in names_by_dof.items():
        for dof, muscles in dofs.items():
            for muscle in muscles:
                for joint in attach_data[side][muscle].keys():
                    plot_data[next(counter)] = np.asarray(
                        custom_cmap(
                            [np.sum(attach_data[side][muscle][joint]['sobol_S1'][:3]),
                             np.sum(attach_data[side][muscle][joint]['sobol_S1'][3:]),
                             *param_data[side][muscle][joint]['sobol_S1'][:3],
                             0.0]
                        )
                    )
    # reshape plot data to match subplots
    plot_data = np.reshape(
        plot_data, (num_subplots, num_muscles_subplot, 6, 4)
    )
    # plot
    # add empty column
    plot_data[:, :, -1] = np.zeros(np.shape(plot_data[:, :, -1]))
    # swap empty column
    plot_data = plot_data [:, :, [0, 1, 5, 2, 3, 4]]
    for subplot in range(num_subplots):
        im = axs[subplot].imshow(
            plot_data[subplot], interpolation='none',
            aspect='equal', alpha=1.0, cmap=custom_cmap
        )
        # Major ticks
        xtick_major_labels = (
            'P_p', 'P_c', '', 'F_max', 'l_opt', 'l_slack'
        )
        axs[subplot].set_xticks((range(len(xtick_major_labels))))
        axs[subplot].set_xticklabels(
            xtick_major_labels, rotation="vertical"
        )
        axs[subplot].set_yticks(range(num_muscles_subplot))
        axs[subplot].set_yticklabels(
            muscle_labels[
                subplot*num_muscles_subplot:(subplot+1)*num_muscles_subplot
            ], rotation="horizontal"
        )
        # Minor ticks
        axs[subplot].set_xticks(
            np.arange(-.5, len(xtick_major_labels)+1, 1), minor=True
        )
        axs[subplot].set_yticks(np.arange(-.5, num_muscles_subplot, 1),
                                minor=True)
        axs[subplot].tick_params(which='minor', length=0)
        # Grid
        axs[subplot].grid(
            which='minor', color='w', linestyle='-', linewidth=2,
            axis='x'
        )
        axs[subplot].grid(
            which='minor', color='w', linestyle='-', linewidth=2,
            axis='y'
        )
    #: color bar
    cbar_ax = fig.add_axes([0.25, 0.035, 0.5, 0.015])
    fig.colorbar(im, cax=cbar_ax, orientation="horizontal")
    # Gridlines based on minor ticks
    save_figure(
            figure_handle=fig,
            file_path=FIGURE_PATH.joinpath("sensitivity", "combined"),
            file_name=f"muscle_salib_sensitivity",
            # legend=legend
    )
    plt.close()


def plot_attachment_sensitivity_examples(fig, gs, muscle_name: str):
    """ plot_attacment_sensitivity_examples """
    attach_data = h5py.File(MUSCLE_RESULTS_PATH.joinpath(
            "muscle_attachment_sensitivity_example.h5"
        ), 'r')

    # Get muscle data
    side = "HIND" if muscle_name in attach_data["HIND"] else "FORE"
    muscle_data = attach_data[side][muscle_name]

    num_joints = len(muscle_data.keys())
    # Subdivide the axis into multiple blocks
    gss = gridspec.GridSpecFromSubplotSpec(
        1, num_joints, subplot_spec=gs, hspace=0.2
    )
    for ax_num, (joint_name, analysis) in enumerate(muscle_data.items()):
        axis = fig.add_subplot(gss[ax_num])
        angles = np.rad2deg(analysis['joint_angles'])
        axis.plot(
            angles, np.asarray(analysis['default_moment_arm'])*1e3,

            color='k', label="original"
        )
        for j, param in enumerate(("origin", "insertion")):
            color = list(to_rgba(f'C{2*j}'))
            facecolor = [*color[:3], 0.15]
            edgecolor = [*color[:3], 0.75]
            axis.fill_between(
                angles,
                np.min(analysis[param]['moment_arms'], axis=1)*1e3,
                np.max(analysis[param]['moment_arms'], axis=1)*1e3,
                facecolor=facecolor, label=param,
                edgecolor=edgecolor,
            )
        # axis.set_xlabel(f"{adapt_joint_names_for_plot((joint_name,))[0]}")
    fig.add_subplot(gss[0]).set_ylabel("Moment-arm [mm]")
    fig.add_subplot(gss[-1]).legend(("original", "origin", "insertion"))


def plot_parameter_sensitivity_examples(fig, gs, muscle_name: str):
    """ plot_attacment_sensitivity_examples """
    attach_data = h5py.File(
        MUSCLE_RESULTS_PATH.joinpath(
            "muscle_parameter_example_sensitivity.h5"
        ), 'r'
    )

    # Get muscle data
    side = "HIND" if muscle_name in attach_data["HIND"] else "FORE"
    muscle_data = attach_data[side][muscle_name]

    num_joints = len(muscle_data.keys())
    # Subdivide the axis into multiple blocks
    gss = gridspec.GridSpecFromSubplotSpec(
        1, num_joints, subplot_spec=gs
    )
    for ax_num, (joint_name, analysis) in enumerate(muscle_data.items()):
        axis = fig.add_subplot(gss[ax_num])
        angles = np.rad2deg(analysis['joint_angles'])
        axis.plot(
            angles, np.asarray(analysis['default_moment'])*1e3,
            color='k', label="original"
        )
        for j, param in enumerate(("f_max", "l_opt", "l_slack")):
            color = list(to_rgba(f'C{2*j}'))
            facecolor = [*color[:3], 0.15]
            edgecolor = [*color[:3], 0.75]
            axis.fill_between(
                angles,
                np.min(analysis[param]['moments'], axis=1)*1e3,
                np.max(analysis[param]['moments'], axis=1)*1e3,
                facecolor=facecolor, edgecolor=edgecolor
            )
        axis.set_xlabel(f"{adapt_joint_names_for_plot((joint_name,))[0]}")
    fig.add_subplot(gss[0]).set_ylabel("Moment [N-mm]")
    fig.add_subplot(gss[-1]).legend(("original", "f_max", "l_opt", "l_slack"))


def generate_muscle_sensitivity_example_paper_figure():
    """ Generate examples figure for muscle sensitivity """
    # Setup figure size
    fig_size = get_fig_size(
        PlotType.PUBLICATION, journal_name="ieee_access", columns="double",
        fraction=1.5
    )
    # Create figure
    fig = plt.figure(figsize=fig_size)
    # Setup gridspec
    gs = gridspec.GridSpec(
        nrows=2, ncols=1, figure=fig, # wspace=0.3, hspace=0.5
    )
    # Muscle name
    muscle_name = "GA"
    # Create plots
    extra_artists = []
    plot_attachment_sensitivity_examples(
        fig, gs[0, 0], muscle_name=muscle_name
    )
    plot_parameter_sensitivity_examples(
        fig, gs[1, 0], muscle_name=muscle_name
    )
    # Share axis


    # Save plots
    save_figure(
        figure_handle=fig,
        file_path=FIGURE_PATH.joinpath(f"sensitivity", "examples"),
        file_name=f"{muscle_name}",
        extra_artists=extra_artists
    )
    plt.close()


def plot_degroote_force_length_velocity_curves(**kwargs):
    """ Generate force-length and force-velocity curve from degroote
    formulation

    Parameters
    ----------
    **kwargs : <dict>

    Returns
    -------
    out :

    """
    #: Force-Length Constants
    b1 = kwargs.pop('b1', [0.815, 0.433, 0.1])
    b2 = kwargs.pop('b2', [1.055, 0.717, 1.0])
    b3 = kwargs.pop('b3', [0.162, -0.030, 0.354])
    b4 = kwargs.pop('b4', [0.063, 0.2, 0.0])

    kpe = kwargs.pop('kpe', 4.0)
    e0 = kwargs.pop('e0', 0.6)

    l_ce = np.linspace(0.4, 1.6, 100)
    active_force = np.zeros(np.shape(l_ce))
    passive_force = np.zeros(np.shape(l_ce))
    for step, _l_ce in enumerate(l_ce):
        _active_force = 0.0
        for j in range(3):
            _num = -0.5*(_l_ce - b2[j])**2
            _den = (b3[j] + b4[j]*_l_ce)**2
            _active_force += b1[j]*np.exp(_num/_den)
        active_force[step] = _active_force
        passive_force[step] = (
            np.exp(kpe*(_l_ce - 1)/e0) - 1.0)/(np.exp(kpe) - 1.0) if _l_ce > 1.0 else 0.0

    #: Force-Velocity Constants
    d1 = kwargs.pop('d1', -0.318)
    d2 = kwargs.pop('d2', -8.149)
    d3 = kwargs.pop('d3', -0.374)
    d4 = kwargs.pop('d4', 0.886)

    v_ce = np.linspace(-1.0, 1.0, 100)
    force = np.zeros(np.shape(v_ce))
    for step, _v_ce in enumerate(v_ce):
        exp1 = d2*_v_ce + d3
        exp2 = ((d2*_v_ce + d3)**2) + 1.
        force[step] = d1*np.log(exp1 + np.sqrt(exp2)) + d4

    fig, (ax1, ax2) = plt.subplots(
        2, 1,
        figsize=get_fig_size(
            PlotType.PUBLICATION, fraction=0.5, subplots=(2, 1)
        )
    )
    ax1.plot(l_ce, active_force)
    ax1.plot(l_ce, passive_force)
    ax1.plot(l_ce, active_force+passive_force, ':')
    ax1.plot((l_ce[0], 1.0), (1.0, 1.0), ':k')
    ax1.plot((1.0, 1.0), (0.5, 1.0), ':k')
    ax1.set_xlabel('$l̃_{ce}$')
    ax1.set_ylabel('force')
    # ax1.set_title('Force-Length Curve')
    legend=ax1.legend(
        ('Active', 'Passive', 'Total'),
        loc='lower center',
        framealpha=0.0
    )
    ax1.grid(False)

    ax2.plot(v_ce, force)
    ax2.set_xlabel('$ṽ_{ce}$')
    ax2.set_ylabel('force')
    ax2.plot((v_ce[0], 0.0), (1.0, 1.0), ':k')
    ax2.plot((0.0, 0.0), (0.0, 1.0), ':k')
    # ax2.set_title('Force-Velocity Curve')
    ax2.grid(False)
    save_figure(
        figure_handle=fig,
        file_path=FIGURE_PATH,
        file_name="force_length_velocity_curve",
        legend=legend
    )


if __name__ == '__main__':
    # plot_osim_muscle_groups(group_by="moment_arm")
    # plot_osim_muscle_groups(group_by="moment")
    # plot_moment_arm()
    # plot_origin_insertion_sensitivity()
    # plot_parameter_sensitivity()
    # plot_degroote_force_length_velocity_curves()
    # plot_salib_parameter_sensitivity()
    # plot_salib_attachment_sensitivity()
    # plot_attachment_sensitivity_examples()
    # plot_parameter_sensitivity_examples()
    # plot_salib_sensitivity()

    # generate_muscle_groups_paper_figure()
    # generate_muscle_groups_example_paper_figure()
    # generate_hind_limb_moment_arm_comparison_paper_figure()
    # generate_fiber_length_range_paper_figure()
    generate_muscle_sensitivity_example_paper_figure()

    # deprecated
    # plot_moment_arm_groups()
    # plot_moment_arm_box_plot()
    # plot_bi_articular_moment_arm()
    # plot_active_force_joint_angle()
    # plot_active_force_length()
