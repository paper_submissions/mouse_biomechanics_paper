""" Generate hindlimb muscle plots """

import os

import h5py
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib.colors import to_rgba
from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from tqdm import tqdm


def plot_moment_arm_box_plot():
    """ Plot  muscle moment arms as box plots. """
    # Read joint muscle relationship
    data = h5py.File("default_all_muscles.h5", 'r')
    # Initialize
    labels = [
        # f"{muscle.replace('_', '-')}-{joint.replace('_', ' ')}"
        f"{muscle.replace('_', '-')}"
        for limb, muscle_joint in data.items()
        for muscle, muscle_data in muscle_joint.items()
        for joint, joint_data in muscle_data['mono'].items()
    ]
    num_muscles_subplot = 25
    num_subplots = round(len(labels)/num_muscles_subplot)
    subplot = -1
    counter = 0
    plot_data = [[] for j in range(num_subplots)]
    fig, axs = plt.subplots(
        num_subplots, 1, figsize=(4.5, 6)
    )
    #: Generate plots
    for limb, muscle_joint in data.items():
        for muscle, muscle_data in muscle_joint.items():
            for joint, joint_data in muscle_data['mono'].items():
                if counter % num_muscles_subplot == 0:
                    subplot += 1
                plot_data[subplot].append(joint_data['moment_arm'])
                counter += 1

    for subplot in range(num_subplots):
        axs[subplot].boxplot(plot_data[subplot], showfliers=False)
        axs[subplot].set_xticks(np.arange(num_muscles_subplot))
        axs[subplot].set_xticklabels(
            labels[
                subplot*num_muscles_subplot:(subplot+1)*num_muscles_subplot
            ], rotation='vertical'
        )
    plt.show()
    data.close()

def main():
    plot_moment_arm_box_plot()


if __name__ == '__main__':
    main()
