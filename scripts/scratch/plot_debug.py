#!/usr/bin/env python

import h5py
import matplotlib.pyplot as plt
import numpy as np

# data = h5py.File("../../data/results/muscle_analysis/main.h5", 'r')

# side = "HIND"
# muscles = ["GM_ventral", "GM_mid", "GM_dorsal"]
# joints = ["Hip_adduction"]*len(muscles)
# attribute = "moment_arm"

# for muscle, joint in zip(muscles, joints):
#     plt.plot(
#         np.rad2deg(data[side][muscle][joint]['primary_joint_angles']),
#         data[side][muscle][joint]['moment_arm']
#     )

# plt.legend(muscles)
# plt.show()

# data.close()

data = h5py.File("../../data/results/muscle_analysis/muscle_attachment_salib_sensitivity.h5", 'r')

from IPython import embed; embed()
