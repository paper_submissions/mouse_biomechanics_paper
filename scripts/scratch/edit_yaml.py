""" Edit yaml files """
from argparse import ArgumentParser
from copy import deepcopy
from pprint import pprint

import yaml
import numpy as np

from farms_data.io.yaml import read_yaml, write_yaml

import farms_pylog as pylog


def setup_full_forelimb_muscle_config_from_dev():
    """ Setup full forelimb muscle config using the dev config file  """
    data = read_yaml("../../data/config/muscles/left_forelimb.yaml")
    new_data = deepcopy(data)
    for name, muscle in data['muscles'].items():
        new_name = name.replace('LEFT', 'RIGHT')
        new_data['muscles'][new_name] = muscle
        new_data['muscles'][new_name]['name'] = new_name
        for point in muscle['waypoints']:
            point[0]['link'] = point[0]['link'].replace('L', 'R', 1)
            point[1]['point'][0] *= -1
    #: Write the new data
    write_yaml(new_data, "../../data/config/muscles/forelimb.yaml")


def setup_full_hindlimb_muscle_config_from_dev():
    """ Setup full hindlimb muscle config using the dev config file  """
    data = read_yaml(
        "../../data/config/fixed_hindlimb_muscle_scaled_config.yaml")
    new_data = deepcopy(data)
    for name, muscle in data['muscles'].items():
        new_name = name.replace('RIGHT', 'LEFT')
        new_data['muscles'][new_name] = muscle
        new_data['muscles'][new_name]['name'] = new_name
        for point in muscle['waypoints']:
            point[0]['link'] = point[0]['link'].replace('R', 'L', 1)
            point[1]['point'][0] *= -1
    #: Write the new data
    write_yaml(new_data, "../../data/config/hindlimb_muscle_config.yaml")


def mirror_config():
    """ Mirror left muscle config to right config  """
    def mirror_args():
        parser = ArgumentParser("mirror parser")
        parser.add_argument(
            "--mirror-side", type=str, required=True, dest="mirror_side",
            help="Left or right side to mirror"
        )
        parser.add_argument(
            "--config-path", type=str, required=True, dest="config_path",
            help="Original path config"
        )
        parser.add_argument(
            "--export-path", type=str, required=False, dest="export_path",
            help="Original path config", default=""
        )
    mirror_sides = {'RIGHT': 'LEFT', 'LEFT': 'RIGHT'}
    data = read_yaml(config_path)
    new_data = deepcopy(data)
    for name, muscle in data['muscles'].items():
        new_name = name.replace(
            mirror_sides[args.mirror_side.upper()], 'LEFT'
        )
        new_data['muscles'][new_name] = muscle
        new_data['muscles'][new_name]['name'] = new_name
        for point in muscle['waypoints']:
            point[0]['link'] = point[0]['link'].replace('R', 'L', 1)
            point[1]['point'][0] *= -1
    #: Write the new data
    write_yaml(new_data, "../../data/config/hindlimb_muscle_config.yaml")


def update_v_max_and_pennation_units():
    """ Update v_max and pennation angle units. """
    for side in ('forelimb', 'hindlimb'):
        data = read_yaml(
            "../../data/config/{}_muscle_config.yaml".format(side)
        )
        for name, muscle in data['muscles'].items():
            muscle['pennation'] = float(np.rad2deg(muscle['pennation']))
            muscle['v_max'] = abs(muscle['v_max'])
        write_yaml(data, "../../data/config/{}_muscle_config.yaml".format(side))


if __name__ == '__main__':
    # setup_full_forelimb_muscle_config_from_dev()
    # setup_full_hindlimb_muscle_config_from_dev()
    # update_v_max_and_pennation_units()
