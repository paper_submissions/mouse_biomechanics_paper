#+HEADER: :file ./contour2.svg :imagemagick yes
#+HEADER: :results output silent
#+HEADER: :headers '("\\usepackage{tikz}") :headers '("\\usetikzlibrary{snakes, backgrounds, arrows.meta, calc}")
#+HEADER: :fit yes :imoutoptions -geometry 400 :iminoptions -density 600
#+BEGIN_src latex
  \begin{tikzpicture} [scale=1, show background rectangle]
  % \draw[help lines] (0,-4) grid (15,10);
  \node [draw, circle, fill](p0) at (0, 0){};
  \node [draw, circle, fill](p1) at (5, 0){};
  \coordinate (d0) at ($(p0)+(0.0, -4.0)$);
  \coordinate (d1) at ($(p1)+(0.0, -2.0)$);
  \draw[very thick, snake=zigzag, segment amplitude=15.0, segment length=20.0, line before snake=1cm, line after snake=1cm] (p0) -- (p1)node[rotate=0,scale=1.5, below=0.3cm,pos=0.5]{SE};
  \begin{scope}[rotate=45, shift=(p1)]
    \node [draw, circle, fill] (p2) at (0.0, 0.0) {};
    \node [draw, circle, fill] (p3) at (4.0, 2.0) {};
    \node [draw, circle, fill] (p4) at (4.0, -2.0) {};
    \node [draw, circle, fill] (p5) at (7.0, 2.0) {};
    \node [draw, circle, fill] (p6) at (7.0, -2.0) {};
    \node [draw, circle, fill] (p7) at (11.0, 0.0) {};
    \coordinate (d2) at ($(p2)+(0.0, 4.0)$);
    \coordinate (d3) at ($(p7)+(0.0, 4.0)$);
    \draw[very thick] (p2) -| (2.0, 2.0) -- (p3);
    \draw[very thick] (p2) -| (2.0, -2.0) -- (p4);
    \draw[very thick] (p5) -| (9.0, 0.0) -- (p7);
    \draw[very thick] (p6) -| (9.0, 0.0) -- (p7);
    \draw[very thick] (p3)++(0.0, -1.0) rectangle node [rotate=45,scale=1.5] {CE}  ++(3.0, 2.0);
    \draw[very thick, snake=zigzag, segment amplitude=15.0, segment length=20.0] (p4) -- (p6)node[rotate=45,scale=1.5, below=0.3cm,pos=0.5]{PE};
    \draw[Latex-Latex, very thick] (d2) -- (d3) node[above, midway, rotate=45] {\Huge $L_m$};
    \draw (d2) -- (p2);
    \draw (d3) -- (p7);
  \end{scope}
  \coordinate (d4) at ($(p7)+(0.0, -11.80)$);
  \draw (d4) -- (p7);
  \draw (d0) -- (p0);
  \draw (d1) -- (p1);
  \draw (p1) -- ++(2, 0);
  \draw[-Latex, very thick] (p1)++(1.0, 0.0) node[above right] (alpha){\Huge $\alpha$} arc (0:45:1);
  \draw[Latex-Latex, very thick] ($(d0)+(0.0, 2.0)$) -- (d1) node[above, midway] {\Huge $L_t$};
  \draw[Latex-Latex, very thick] (d1) -- ($(p7)+(0.0, -9.8)$) node[above, midway] {\Huge $L_mcos\alpha$};
  \draw[Latex-Latex, very thick] (d0) -- ($(p7)+(0.0, -11.80)$) node[above, midway] {\Huge $L_{mt}$};
  \end{tikzpicture}
#+END_src

[[file:contour2.svg]]
