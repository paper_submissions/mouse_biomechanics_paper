""" Convert data exported from opensim to hdf5 """

import os

import argparse

import h5py
import numpy as np

import farms_pylog as pylog

def create_muscle_group(data_file, muscle_name, joint_names):
    """ Add new muscle group. """
    muscle_group = data_file.create_group(name=muscle_name)
    #: Add attributes
    muscle_group.attrs['is_biarticular'] = False
    return muscle_group


def main():
    """ Main """
    #: Argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--folder_path", "-fp", dest="folder_path", type=str
    )
    parser.add_argument(
        "--export_path", "-ep", dest="export_path", type=str
    )
    args = parser.parse_args()
    folder_path = args.folder_path
    export_path = args.export_path
    #: Create h5 data file
    data_file = h5py.File(export_path, 'w')
    for dir_path, dir_name, file_names in os.walk(folder_path):
        for file_name in file_names:
            if os.path.splitext(file_name)[-1] != ".txt":
                continue
            curr_dir = os.path.split(dir_path)[-1]
            if curr_dir not in data_file:
                data_file.create_group(curr_dir)
            function_group = data_file[curr_dir]
            # pylog.debug("Reading {}".format(file_name))
            #: Read data
            data = np.loadtxt(
                os.path.join(dir_path, file_name), skiprows=7, delimiter='\t'
            )
            #: Get muscle and joint name from file name
            muscle_name = file_name.split("muscle_")[-1].split("_joint")[0]
            joint_name = file_name.split("joint_")[-1].split("_function")[0]
            pylog.debug("{} - {}".format(muscle_name, joint_name))
            #: Add muscle group if not present
            if muscle_name not in function_group:
                function_group.create_group(muscle_name)
            muscle_group = function_group[muscle_name]
            joint_group = muscle_group.create_group(joint_name)
            joint_group.create_dataset(name="joint_angle", data=data[:,1])
            joint_group.create_dataset(name="data", data=data[:,2])
    data_file.close()


if __name__ == '__main__':
    main()
