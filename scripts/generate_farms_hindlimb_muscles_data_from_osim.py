""" Transfer muscle data opensim mouse hindlimb model. """

import copy
import os


import farms_pylog as pylog
import numpy as np
import opensim as osim
import pandas as pd
import yaml
from farms_opensim import opensim_utils as osim_utils
from farms_opensim import farms_utils

from generate_muscle_properties_data import generate_muscle_properties_data


def export_yaml(data, file_path):
    """ Internal method that dumps the data to yaml file.

    Parameters
    ----------
    data : <dict>
        Dictionary containing the data to dump
    file_path : <str>
        File path to dump the data

    Returns
    -------
    out : <None>
    """
    with open(file_path, 'w') as file:
        to_write = yaml.dump(
            data, default_flow_style=None,
            explicit_start=True, indent=4, width=80).replace(
                "null", "none"
        )
        file.write(to_write)


def generate_farms_muscle_config_from_osim(
        osim_model_path, export_original=True
):
    """ Generate farms muscle config from osim

    Parameters
    ----------
    osim_model_path: <str>
        Path to the *.osim model file
    """

    #: Load the model
    model = osim_utils.read_osim(osim_model_path)
    state = model.initSystem()

    #: Reset model pose to zero
    osim_utils.reset_model_to_zero_pose(model, state)

    #: Extract and convert muscle data
    muscles_data = farms_utils.get_muscle_config(
        osim_utils.extract_muscle_properties(model),
        osim_utils.extract_muscle_path_point_properties(model))

    #: Export the data to config folder
    if export_original:
        export_yaml(
            muscles_data,
            "../data/config/osim_original_hindlimb_muscle_config.yaml"
        )

    #: Clean the muscles data for farms
    #: Read the transformation matrices
    pelvis_trans = np.loadtxt(
        "../data/hind_limb_mesh_registration/RPelvisTransformation.txt"
    )
    femur_trans = np.loadtxt(
        "../data/hind_limb_mesh_registration/RFemurTransformation.txt"
    )
    tibia_trans = np.loadtxt(
        "../data/hind_limb_mesh_registration/RTibiaTransformation.txt"
    )
    foot_trans = np.loadtxt(
        "../data/hind_limb_mesh_registration/RFootTransformation.txt"
    )

    #: Rename link names
    new_link_names = {
        'pelvis_link': 'Pelvis',
        'thigh_link': 'RFemur',
        'leg_link': 'RTibia',
        'pedal_link': 'RTarsus',
    }
    for name, muscle in muscles_data['muscles'].items():
        for link, point in muscle['waypoints']:
            link['link'] = new_link_names[link['link']]

    #: transformation_matrix
    transformation_matrix = {
        "Pelvis": pelvis_trans,
        "RFemur": femur_trans,
        "RTibia": tibia_trans,
        "RTarsus": foot_trans
    }

    #: Create data for left and right hindlimbs
    farms_muscles_data = {'muscles': {}}
    for name in muscles_data["muscles"].keys():
        #: right limb data
        muscle_name = 'RIGHT_HIND_{}'.format(name)
        farms_muscles_data['muscles'][muscle_name] = (
            muscles_data['muscles'][name]
        )
        farms_muscles_data['muscles'][muscle_name]["name"] = muscle_name
        #: Change vmax
        farms_muscles_data["muscles"][muscle_name]["v_max"] = -12.0
        #: Transfer the attachment points based on mesh registration
        for link, point in (
                farms_muscles_data['muscles'][muscle_name]['waypoints']
        ):
            trans = transformation_matrix[link["link"]]
            original_point = np.array(
                [point["point"][0], point["point"][1], point["point"][2], 1]
            )
            point["point"] = ((trans@original_point).tolist())[:3]

        #: left limb data
        muscle_name = 'LEFT_HIND_{}'.format(name)
        farms_muscles_data["muscles"][muscle_name] = copy.deepcopy(
            farms_muscles_data["muscles"][muscle_name.replace("LEFT", "RIGHT")]
        )
        farms_muscles_data['muscles'][muscle_name]["name"] = muscle_name
        #: Update muscle attachment data
        for link, point in (
                farms_muscles_data['muscles'][muscle_name]['waypoints']
        ):
            point['point'][0] *= -1
            _link_name = link['link']
            # link['link'] = _link_name.replace("RIGHT", "LEFT")
            link['link'] = link['link'].replace("R", "L", 1)

    #: export farms muscle config
    export_yaml(
        farms_muscles_data,
        "../data/config/hindlimb_muscle_config.yaml"
    )

    #: Scale factors
    with open(
            "../data/config/hind_limb_muscle_joint.yaml", "r"
    ) as stream:
        muscles_joints = yaml.load(stream, yaml.SafeLoader)
    with open(
            "../data/config/hind_limb_default_pose.yaml", "r"
    ) as stream:
        default_pose = yaml.load(stream, yaml.SafeLoader)
    scale_factors = generate_muscle_length_scale_factors(
        muscles_joints, default_pose
    )

    #: Read previously exported config file
    with open("../data/config/hindlimb_muscle_config.yaml", "r") as stream:
        farms_muscles_data = yaml.load(
            stream, yaml.SafeLoader
        )

    for muscle in farms_muscles_data["muscles"].values():
        muscle_root_name = "_".join(muscle["name"].split("_")[2:])
        scale_factor = scale_factors[muscle_root_name]
        muscle["l_ce0"] = muscle["l_ce0"]*scale_factor
        muscle["l_opt"] = muscle["l_opt"]*scale_factor
        muscle["l_slack"] = muscle["l_slack"]*scale_factor

    #: export farms muscle config
    export_yaml(
        farms_muscles_data,
        "../data/config/hindlimb_muscle_config.yaml"
    )

    return farms_muscles_data


def generate_muscle_length_scale_factors(muscle_joint, default_pose, export=True):
    """
    Parameters
    ----------
    muscle_joint : <dict>
        Dict of muscle joint used to compute the scaling factor

    Returns
    -------
    scale_factor : <float>
        Scale factor for length between osim and farms model

    """
    scale_factors = {}
    for muscle, joint in muscle_joint.items():
        file_path = (
            "../data/hind_limb_muscle_tendon_lengths/"
            "muscle_tendon_length_{}/{}_{}_muscle_tendon_length.h5"
        )
        muscle_tendon_length_osim_path = (
            "../data/hind_limb_muscle_moment_arms/moment_arm_opensim/"
        )
        muscle_length_osim = pd.read_hdf(
            file_path.format("opensim", muscle, joint)
        )
        muscle_length_bullet = pd.read_hdf(
            file_path.format("pybullet", muscle, joint)
        )
        #: Get the zero angle length
        osim_zero_length = np.interp(
            np.deg2rad(default_pose[joint]),
            muscle_length_osim[joint],
            muscle_length_osim[muscle]
        )
        bullet_zero_length = np.interp(
            np.deg2rad(default_pose[joint]),
            muscle_length_bullet[joint],
            muscle_length_bullet[muscle]
        )
        #: Compute scaling factor
        scale_factors[muscle] = float(bullet_zero_length/osim_zero_length)

    #: Export scale factors
    if export:
        export_path = "../data/config/hind_limb_muscle_scale_factors.yaml"
        export_yaml(scale_factors, export_path)
    return scale_factors


if __name__ == '__main__':
    data = generate_farms_muscle_config_from_osim(
        "../data/mouse_hindlimb_2018/Mouse_hindlimb_model_2018.osim"
    )
