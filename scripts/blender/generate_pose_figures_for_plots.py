""" Generate poses for plotting """

import os
import pathlib
import sys

import numpy as np

import bpy
import mouse_scene
from farms_blender.core import muscles, sdf
from farms_blender.core.camera import create_multiview_camera
from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.display import (display_farms_types,
                                        enable_smooth_shading)
from farms_blender.core.freestyle import (configure_lineset, create_lineset,
                                          default_lineset_config,
                                          enable_freestyle, remove_lineset,
                                          set_freestyle_setting)
from farms_blender.core.objects import objs_of_farms_types
from farms_blender.core.pose import set_joint_pose
from farms_blender.core.scene import set_scene_settings
from farms_blender.core.utils import select_object
from farms_data.io.io import read_yaml
from mathutils import Euler, Matrix, Vector

SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
SDF_PATH = DATA_PATH.joinpath("models", "sdf")
FIGURES_PATH = SCRIPT_PATH.joinpath(
    "..", "..", "src", "figures", "dof"
)


def configure_scene(**kwargs):
    """ Configure scene. """
    set_scene_settings(renderer='BLENDER_EEVEE')
    # background light
    bpy.data.worlds['World'].node_tree.nodes['Background'].inputs[1].default_value = 1.0
    # Render size
    bpy.context.scene.render.resolution_x = 480
    bpy.context.scene.render.resolution_y = 480
    # Render samples
    bpy.context.scene.eevee.taa_render_samples = 16
    # Disable background
    bpy.data.scenes['Scene'].render.film_transparent = True
    # Enable freestyle
    enable_freestyle()


def configure_freestyle():
    """ Configure line style settings. """
    # Line thinckness
    bpy.data.scenes['Scene'].render.line_thickness = 1.0

    # Create collections for visual objects
    for obj, _ in objs_of_farms_types(visual=True, link=True):
        add_object_to_collection(obj, collection='visuals', create=True)

    # Genric settings
    set_freestyle_setting('crease_angle', 0)
    # Remove default lineset
    remove_lineset('LineSet')
    # Create line sets for each elem
    linesets = {
        name: create_lineset(name)
        for name in ["visuals", ]
    }
    lineset_config = default_lineset_config()
    lineset_config['select_by_visibility'] = True
    lineset_config['select_by_collection'] = True
    lineset_config['select_contour'] = True
    lineset_config['select_silhouette'] = False
    lineset_config['select_crease'] = False
    lineset_config["select_border"] = False
    lineset_config['select_external_contour'] = True

    for name, lineset in linesets.items():
        lineset_config["collection"] = bpy.data.collections['visuals']
        configure_lineset(lineset, **lineset_config)


def generate_limb(**kwargs):
    """ Generate hind and fore limbs """
    model_offset = kwargs.pop('model_offset', (0.0, 0.0, 0.0))

    # Display
    display = {
        'view': True, 'render': True, 'link': False, 'visual': True,
        'collision': False, 'inertial': False, 'com': False, 'muscle': False,
        'joint': False, 'joint_axis': False,
    }

    # Load hindlimb sdf
    hindlimb, *_, hindlimb_objs = sdf.load_sdf(
        SDF_PATH.joinpath("right_hindlimb.sdf"),
        resources_scale=5e-3
    )

    display_farms_types(objs=hindlimb_objs, **display)

    forelimb, *_, forelimb_objs = sdf.load_sdf(
        SDF_PATH.joinpath("right_forelimb.sdf"),
        resources_scale=5e-3
    )

    display_farms_types(objs=forelimb_objs, **display)

    # Offset
    base_link_hind = sdf.get_base_link(hindlimb)
    base_link_hind.location = model_offset
    base_link_fore = sdf.get_base_link(forelimb)
    base_link_fore.location[-1] = model_offset[-1]

    return hindlimb, hindlimb_objs, forelimb, forelimb_objs


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1.0, 0.01, 0.0), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'ORTHO', "lens": 50, "scale": 0.075
    }
    # Create camera 0
    camera_0 = create_multiview_camera(
        0, camera_options
    )
    # Create camera 1
    camera_options["loc"] = (0.0, 1.0, 0.0)
    camera_options["rot"] = (np.pi/2, 0.0, np.pi)
    camera_1 = create_multiview_camera(
        1, camera_options
    )

    # Create camera 2
    camera_options["loc"] = (0.0, 0.0, 1.0)
    camera_options["rot"] = (0.0, 0.0, 0.0)
    camera_2 = create_multiview_camera(
        2, camera_options
    )
    return (camera_0, camera_1, camera_2)


def render_joint(
        objs, joint_name, joint_limits, camera_obj, camera_options
):
    """ Render joint """
    display_farms_types(
        objs=objs,
        visual=True
    )
    for limit, suffix in zip(joint_limits, ("min", "max")):
        set_joint_pose(f"R{joint_name}", limit, units="degrees")
        # Choose camera 0
        bpy.data.scenes['Scene'].camera = camera_obj
        camera_obj.data.ortho_scale = camera_options["ortho_scale"]
        camera_obj.location = camera_options["location"]

        # render settings
        image_path = FIGURES_PATH.joinpath(
            f"{joint_name.lower()}_{suffix}.png"
        )
        bpy.context.scene.render.filepath = str(image_path)
        # for obj in objs: select_object(obj.name, DESELECT_ALL=False)
        # bpy.ops.view3d.camera_to_view_selected()
        bpy.ops.render.render(write_still=1)
        auto_crop_image(image_path)
        set_joint_pose(
            f"R{joint_name}", 0.0, units="degrees"
        )
    display_farms_types(
        objs=objs,
        visual=False
    )


def export_figures(cameras, hindlimb_objs, forelimb_objs):
    """Export figures

    Parameters
    ----------
    export_path : <str>
        File path for exporting the figures
    """
    # Read joint limits config
    joint_limits = read_yaml(
        CONFIG_PATH.joinpath("joint_limits.yaml")
    )

    # Get all visual objects
    hindlimb_visuals = {
        obj.name: obj
        for obj, _ in objs_of_farms_types(objs=hindlimb_objs, visual=True)
    }
    forelimb_visuals = {
        obj.name: obj
        for obj, _ in objs_of_farms_types(objs=forelimb_objs, visual=True)
    }

    # Enable smooth shading
    enable_smooth_shading(hindlimb_visuals.values())
    enable_smooth_shading(forelimb_visuals.values())

    # Hide all objects
    display_farms_types(objs=list(forelimb_visuals.values()), visual=False)
    display_farms_types(objs=list(hindlimb_visuals.values()), visual=False)

    # Render hip-flexion-extension
    joint_name = "Hip_flexion"
    render_joint(
        objs=(hindlimb_visuals['Pelvis_visual'],
              hindlimb_visuals['RFemur_visual']),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[0],
        camera_options={
            "ortho_scale": 0.05,
            "location": (1.0, 0.0, 0.0)
        }
    )
    # Render hip-abduction-adduction
    joint_name = "Hip_adduction"
    render_joint(
        objs=(hindlimb_visuals['Pelvis_visual'],
              hindlimb_visuals['RFemur_visual']),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[1],
        camera_options={
            "ortho_scale": 0.05,
            "location": (0.0, 1.0, 0.0)
        }
    )
    # Render hip-rotation-inversion
    joint_name = "Hip_rotation"
    render_joint(
        objs=(hindlimb_visuals['Pelvis_visual'],
              hindlimb_visuals['RFemur_visual']),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[2],
        camera_options={
            "ortho_scale": 0.05,
            "location": (0.0, 0.0, 1.0)
        }
    )
    # Render knee-flexion-extension
    joint_name = "Knee_flexion"
    render_joint(
        objs=(hindlimb_visuals['RFemur_visual'],
              hindlimb_visuals['RTibia_visual']),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[0],
        camera_options={
            "ortho_scale": 0.06,
            "location": (1.0, 0.0, -0.02)
        }
    )
    # Render Ankle-flexion-extension
    joint_name = "Ankle_flexion"
    render_joint(
        objs=(
            hindlimb_visuals['RTibia_visual'],
            *[
                visual for name, visual in hindlimb_visuals.items()
                if any(
                        link in name
                        for link in (
                                "RTarsus", "RMetatarsus", "RBPhalange"
                        )
                )
            ]
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[0],
        camera_options={
            "ortho_scale": 0.05,
            "location": (1.0, 0.01, -0.045)
        }
    )
    # Render Ankle-inversion-extension
    joint_name = "Ankle_inversion"
    render_joint(
        objs=(
            hindlimb_visuals['RTibia_visual'],
            *[
                visual for name, visual in hindlimb_visuals.items()
                if any(
                        link in name
                        for link in (
                                "RTarsus", "RMetatarsus", "RBPhalange"
                        )
                )
            ]
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[1],
        camera_options={
            "ortho_scale": 0.05,
            "location": (0.005, 1.0, -0.04)
        }
    )
    # Render Shoulder-flexion
    joint_name = "Shoulder_flexion"
    render_joint(
        objs=(
            forelimb_visuals['RScapula_visual'],
            forelimb_visuals['RHumerus_visual'],
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[0],
        camera_options={


            "ortho_scale": 0.04,
            "location": (1.0, 0.062, -0.0063)
        }
    )
    # Render Shoulder-adduction
    joint_name = "Shoulder_adduction"
    render_joint(
        objs=(
            forelimb_visuals['RScapula_visual'],
            forelimb_visuals['RHumerus_visual'],
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[1],
        camera_options={
            "ortho_scale": 0.05,
            "location": (0.0, 1.0, 0.0)
        }
    )
    # Render Shoulder-rotation
    joint_name = "Shoulder_rotation"
    render_joint(
        objs=(
            forelimb_visuals['RScapula_visual'],
            forelimb_visuals['RHumerus_visual'],
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[2],
        camera_options={
            "ortho_scale": 0.04,
            "location": (0.00765, 0.063, 1.0)
        }
    )
    # Render Elbow-flexion
    joint_name = "Elbow_flexion"
    render_joint(
        objs=(
            forelimb_visuals['RHumerus_visual'],
            forelimb_visuals['RRadius_visual'],
            forelimb_visuals['RUlna_visual'],
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[0],
        camera_options={
            "ortho_scale": 0.04,
            "location": (1.0, 0.062, -0.025)
        }
    )
    # Render Elbow-supination
    joint_name = "Elbow_supination"
    render_joint(
        objs=(
            forelimb_visuals['RRadius_visual'],
            forelimb_visuals['RUlna_visual'],
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[1],
        camera_options={
            "ortho_scale": 0.04,
            "location": (0.0, 1.0, -0.025)
        }
    )
    # Render Wrist-flexion-extension
    joint_name = "Wrist_flexion"
    render_joint(
        objs=(
            forelimb_visuals['RRadius_visual'],
            forelimb_visuals['RUlna_visual'],
            *[
                visual for name, visual in forelimb_visuals.items()
                if any(link in name for link in ("RCarpus", "RMetaCarpus", "RFPhalange"))
            ]
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[0],
        camera_options={
            "ortho_scale": 0.05,
            "location": (1.0, 0.062, -0.03)
        }
    )
    # Render Wrist-adduction
    joint_name = "Wrist_adduction"
    render_joint(
        objs=(
            forelimb_visuals['RRadius_visual'],
            forelimb_visuals['RUlna_visual'],
            *[
                visual for name, visual in forelimb_visuals.items()
                if any(link in name for link in ("RCarpus", "RMetaCarpus", "RFPhalange"))
            ]
        ),
        joint_name=joint_name,
        joint_limits=joint_limits[f"R{joint_name}"],
        camera_obj=cameras[1],
        camera_options={
            "ortho_scale": 0.05,
            "location": (0.0, 1.0, -0.03)
        }
    )
    # Render Wrist-inversion
    # joint_name = "Wrist_inversion"
    # render_joint(
    #     objs=(
    #         forelimb_visuals['RRadius_visual'],
    #         forelimb_visuals['RUlna_visual'],
    #         *[
    #             visual for name, visual in forelimb_visuals.items()
    #             if any(link in name for link in ("RCarpus", "RMetaCarpus", "RFPhalange"))
    #         ]
    #     ),
    #     joint_name=joint_name,
    #     joint_limits=joint_limits[f"R{joint_name}"],
    #     camera_obj=cameras[1],
    #     camera_options={
    #         "ortho_scale": 0.05,
    #         "location": (0.0, 1.0, -0.03)
    #     }
    # )


def auto_crop_image(image_path):
    """
    Auto crop image with imagemagick tool
    """
    os.system(f"convert {image_path} -trim +repage {image_path}")


if __name__ == '__main__':
    mouse_scene.scene(add_floor=False)

    # Configure scene
    configure_scene()

    # Load model
    hindlimb, hindlimb_objs, forelimb, forelimb_objs = generate_limb(
        model_offset=(0.0, 0.0, 0.0)
    )

    # Configure freestyle
    configure_freestyle()

    # Create cameras
    cameras = add_cameras()

    # Export figures
    export_figures(cameras, hindlimb_objs, forelimb_objs)
