""" Generate figure for muscle attachments. """

import os
import sys

import yaml
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import networkx as nx
from scipy.spatial import Delaunay
from scipy.spatial.transform import Rotation

import bpy
from bpy_extras.object_utils import world_to_camera_view
import mathutils
from farms_blender.core.camera import create_multiview_camera
from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.display import display_farms_types
from farms_blender.core.duplicate import get_all_model_objs
from farms_blender.core.materials import farms_material, flat_shade_material
from farms_blender.core.modifiers import apply_shrinkwrap
from farms_blender.core.objects import join, objs_of_farms_types
from farms_blender.core.primitives import create_sphere
from farms_blender.core.resources import get_resource_object
from farms_blender.core.transforms import set_parent, blender_global_coordinates
from farms_utils.plotting import get_fig_size, PlotType


script_path = os.path.abspath(bpy.data.filepath)
sys.path.append(script_path)
import mouse_scene
from loading import load_model, load_muscles

#: Load default plot params
with open('../../data/config/plotting.yaml', 'r') as stream:
    plot_params = yaml.load(stream, yaml.FullLoader)

#: Set plot properties
plot_params['text.usetex'] = False
plt.rcParams.update(plot_params)

muscles_config_path = os.path.join(
        script_path, "..", "..", "data", "config", "muscles", "{}.yaml"
)

model_config_path = os.path.join(
        script_path, "..", "..", "data", "models", "sdf", "{}.sdf"
)

figures_path = os.path.join(
    script_path, "..", "..", "src", "figures", "{}.png"
)


def spring_layout(ax, data, annotations, iterations = 50, k=None):
    """
    - data: coordinates of your points [(x1,y1), (x2,y2), ..., (xn,yn)]
    - annotations: text for your annotation ['str_1', 'str_2', ..., 'str_n']
    - iterations: number of iterations for spring layout
    - k: optimal distance between nodes
    """
    if k is None:
        k = 1 / np.sqrt(len(data))
    G = nx.Graph()
    init_pos = {} # initial position
    x, y = [e[0] for e in data], [e[1] for e in data]
    xmin, xmax = min(x), max(x)
    ymin, ymax = min(y), max(y)
    ax.scatter(x, y)
    tri = Delaunay(data)
    for i, text in enumerate(annotations):
        xy = data[i]
        G.add_node(xy)
        G.add_node(text)
        G.add_edge(xy, text)
        init_pos[xy] = xy
        init_pos[text] = xy
    for ijk in tri.simplices.copy():
        edges = zip(ijk, ijk[1:])
        for edge in edges:
            G.add_edge(annotations[edge[0]], annotations[edge[1]])
    pos = nx.spring_layout(G ,pos=init_pos, fixed=data, iterations=iterations,\
    k=k)
    for i, name in enumerate(annotations):
        xy = data[i]
        xytext = pos[name] * [xmax-xmin, ymax-ymin] + [xmin, ymin]
        ax.annotate(name, xy, xycoords='data', xytext=xytext, textcoords='data', \
        bbox=dict(boxstyle='round,pad=0.2', fc='yellow', alpha=0.3),\
                 arrowprops=dict(arrowstyle="->", connectionstyle="arc3", \
                 color='gray'))


def convert_coord_to_matplotlib(camera, coord, scene=bpy.context.scene):
    """ Convert coordinates from camera view to matplotlib.
    reference : https://blender.stackexchange.com/a/1257
    """

    # needed to rescale 2d coordinates
    render = scene.render
    res_x = render.resolution_x
    res_y = render.resolution_y
    bpy.context.view_layer.update()
    coords_2d = world_to_camera_view(scene, camera, coord)
    return (
        res_x-round(res_x*coords_2d[0]), res_y-round(res_y*coords_2d[1])
    )


def homogeneous_matrix_2d(translation=(0., 0.), rotation=0, scale=1.0):
    """ Construct a homogeneous_matrix for 2d transformations. """
    #: Identity matrix
    homogeneous_matrix = np.identity(3)
    #: Rotation matrix
    homogeneous_matrix[:2, :2] = (Rotation.from_euler(
        'xyz', (0.0, 0.0, rotation))).as_matrix()[:2, :2]
    #: Translation matrix
    homogeneous_matrix[:2, -1] = np.asarray(translation)
    #: Scale matrix
    scale_matrix = np.identity(3)
    scale_matrix[0, 0], scale_matrix[1, 1] = scale, scale
    homogeneous_matrix = np.einsum(
        'ij, jk -> ik', homogeneous_matrix, scale_matrix)
    return homogeneous_matrix


def load_muscle_attachments():
    """ Load the hindlimb segments """
    #: Create materials
    flat_shade_material()
    flat_shade_material(name="origin", color=(1.0, 0.0, 0.0, 1.0))
    flat_shade_material(name="insertion", color=(0.0, 1.0, 0.0, 1.0))

    #: Create attachment objects
    origin_sphere = create_sphere(radius=5e-2)
    origin_sphere.name = 'origin_sphere'
    origin_sphere.active_material = farms_material("origin")
    insertion_sphere = create_sphere(radius=5e-2)
    insertion_sphere.name = 'insertion_sphere'
    insertion_sphere.active_material = farms_material("insertion")
    origin_sphere.hide_set(True)
    origin_sphere.hide_render = True
    insertion_sphere.hide_set(True)
    insertion_sphere.hide_render = True

    #: Load muscles data
    with open(muscles_config_path.format("right_hindlimb"), "r") as stream:
        muscles = yaml.load(stream, yaml.SafeLoader)

    #: Loop over muscles
    origin_objs = [
        attach_point_to_object(
            data['waypoints'][0][0]['link'],
            data['waypoints'][0][1]['point'],
            ptype='origin',
            muscle_name=muscle
        )
        for muscle, data in muscles['muscles'].items()
    ]
    insertion_objs = [
        attach_point_to_object(
            data['waypoints'][-1][0]['link'],
            data['waypoints'][-1][1]['point'],
            ptype='insertion',
            muscle_name=muscle
        )
        for muscle, data in muscles['muscles'].items()
    ]
    return origin_objs, insertion_objs


def attach_point_to_object(link_name, point, ptype, muscle_name):
    """ Attach point to object. """
    link_obj = bpy.data.objects[link_name]
    visual_obj = bpy.data.objects[f"{link_name}_visual"]
    transform = link_obj.matrix_world.to_euler().to_matrix().to_4x4()
    transform.translation = link_obj.matrix_world.to_translation()
    origin_global = transform@mathutils.Vector(point).to_4d()
    obj = bpy.data.objects.new(
        '{}_{}'.format(muscle_name, ptype),
        bpy.data.objects['{}_sphere'.format(ptype)].data
    )
    obj.scale = [1e-2]*3
    bpy.context.collection.objects.link(obj)
    obj.location = origin_global[:3]
    set_parent(obj, link_obj)
    #: Add to appropriate collection
    add_object_to_collection(
        obj, f'{link_name}_attachments', create=True
    )
    #: Add to farms types
    obj['farms_type'] = f'{ptype}'
    #: Add projection with shrink modifier
    apply_shrinkwrap(obj, visual_obj, offset=1e-4)
    return obj


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1.0, 0.01, 0.025), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'ORTHO', "lens": 50, "scale": 0.075
    }
    #: Create camera 0
    camera_0 = create_multiview_camera(
        0, camera_options
    )
    #: Create camera 1
    camera_options["loc"] = (0.0, 1.0, 0.025)
    camera_options["rot"] = (np.pi/2, 0.0, np.pi)
    camera_1 = create_multiview_camera(
        1, camera_options
    )
    return (camera_0, camera_1)


def render_pelvis(camera, **kwargs):
    """ Render pelvis images

    Parameters
    ----------
    **kwargs :
    """

    #: Pelvis
    objs = [
        obj
        for obj, _ in objs_of_farms_types(
                visual=True
        )
    ]
    display_farms_types(
        objs=[
            *[obj for obj in objs if 'Pelvis' in obj.name],
            *[obj for obj in bpy.data.collections['Pelvis_attachments'].objects],
         ],
        visual=True, insertion=True, origin=True
    )
    bpy.data.scenes['Scene'].camera = camera
    camera.location = (1.0, 0.0, 0.0 )
    bpy.context.scene.render.filepath =  "./temp.png"
    bpy.ops.render.render(write_still=1)

    #: Read the image
    image = mpimg.imread(os.path.join(
        "./", "temp.png"))
    #: Figure in matplotlib
    fig, ax = plt.subplots(
        figsize=get_fig_size(PlotType.PUBLICATION)
    )
    ax.imshow(image)

    att_pos = [
        convert_coord_to_matplotlib(camera, obj.location)
        for obj in bpy.data.collections['Pelvis_attachments'].objects
    ]

    att_name = [
        obj.name
        for obj in bpy.data.collections['Pelvis_attachments'].objects
    ]

    spring_layout(
        ax, att_pos, att_name
    )

    # ax.annotate(
    #     'test', att_pos[0], xycoords='data', xytext=att_pos[0],
    #     textcoords='data',
    #     bbox=dict(boxstyle='round,pad=0.2', fc='yellow', alpha=0.3),
    #     arrowprops=dict(arrowstyle="->", connectionstyle="arc3",
    #                     color='gray')
    # )

    # ax.scatter(att_pos[0][0], att_pos[0][1])

    # ax.set_axis_off()

    plt.show()
    fig.savefig(
        os.path.join("./", 'coordinate_frame_joint.pdf'), dpi=300,
        transparent=True
    )


def main():
    """ Main """
    mouse_scene.scene(
        scale=1e-2, add_floor=False, freestyle=True,
        resolution_x=1920, resolution_y=1080
    )

    #: Load model
    model_name, model_objs = load_model(sdf_name="right_hindlimb")

    #: Load mucles
    model_objs += load_muscles("right_hindlimb")

    #: Attach combine visual objects
    objects_to_fuse = [*[f'RMetatarsus{n}' for n in range(1, 6)],
        *[f'RBPhalange{n}' for n in range(1, 6)]]
    foot = ['RTarsus', *objects_to_fuse]

    farms_properties ={
        name : value
        for name, value in bpy.data.objects['RTarsus_visual'].items()
        if 'farms' in name
    }

    join(
        [bpy.data.objects[f"{name}_visual"]for name in foot],
        "RTarsus_visual"
    )
    set_parent(
        bpy.data.objects['RTarsus_visual'], bpy.data.objects['RTarsus']
    )
    for name, prop in farms_properties.items():
        bpy.data.objects['RTarsus_visual'][name] = prop

    model_objs = [
        obj for obj in get_all_model_objs(model_name)
    ]

    #: Load attachments
    origin_objs, insertion_objs =  load_muscle_attachments()
    model_objs += origin_objs
    model_objs += insertion_objs

    #: Update display
    display_farms_types(
        link=False, collision=False, inertial=False, com=False,
        joint=False, joint_axis=False, visual=True, muscle=False,
        muscle_attachment=False
    )

    #: Configure freestyle
    mouse_scene.enable_freestyle()
    mouse_scene.configure_freestyle(
        objs=[
            *origin_objs, *insertion_objs,
            *[obj for obj, _ in objs_of_farms_types(visual=True)]
        ]
    )

    #: Add camera
    cameras = add_cameras()

    #: Hide all
    display_farms_types(
        objs = model_objs,
        visual=False, insertion=False, origin=False
    )

    #: render
    render_pelvis(cameras[0])


if __name__ == '__main__':
    main()
