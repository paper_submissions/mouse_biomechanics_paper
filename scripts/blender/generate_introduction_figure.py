"""Generate figure for the introduction of the paper.

Contains all the muscles, bones, inertials, center of mass

"""

import pathlib
import sys
import time

import farms_pylog as pylog
import h5py
import numpy as np

import bpy
import mathutils
from farms_blender.core.camera import create_multiview_camera
from farms_blender.core.display import display_farms_types
from farms_blender.core.muscles import (analyze_muscle,
                                        extract_muscle_properties_data,
                                        get_muscle_joints)
from farms_blender.core.pose import set_model_pose
from farms_blender.core.sdf import get_base_link
from farms_blender.core.utils import clear_world
from farms_data.io.yaml import read_yaml
from loading import load_scene

# Global config paths
SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
POSE_CONFIG_PATH = CONFIG_PATH.joinpath("pose")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")


def setup_scene():
    """ Load mouse model with muscles. """
    # Clear the world
    clear_world()
    # Load animat
    m_name, m_objs, forelimb_muscle_objs, hindlimb_muscle_objs = load_scene(
        set_pose=False
    )

    # Elements to display. Can be changed later
    display = {'view': True, 'render': False, 'link': False,
               'visual': True, 'collision': False, 'inertial': False,
               'com': False, 'muscle': True, 'joint': False, 'joint_axis': False
               }
    display_farms_types(objs=m_objs, **display)
    # Set the pose of the model to locomotion
    model_pose = read_yaml(POSE_CONFIG_PATH.joinpath("locomotion_pose.yaml"))
    set_model_pose(model_pose, units='degrees')
    # Update base link position and rotation
    base_link = get_base_link(m_name)
    base_link.location = (0.0, 0.0, 0.016)
    base_link.rotation_euler = ((np.deg2rad(45.0), 0.0, 0.0))

    return m_name, m_objs, forelimb_muscle_objs, hindlimb_muscle_objs


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1., 0.0, 0.03), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'PERSP', "lens": 50, "scale": 0.126
    }
    #: Create camera 0
    camera_link = create_multiview_camera(
        0, camera_options
    )


def generate_figure():
    """ Generate the introduction figure """
    # Setup the model with muscles in the scene
    setup_scene()
    # Add camera
    add_cameras()


if __name__ == '__main__':
    generate_figure()
