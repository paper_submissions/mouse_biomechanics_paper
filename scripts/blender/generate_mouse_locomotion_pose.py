""" Generate default pose figure """

import os
import sys
from pathlib import Path

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib.collections import PatchCollection
from matplotlib.patches import Ellipse, Rectangle
from scipy.spatial.transform import Rotation

import bpy
import mathutils
import mouse_scene
from bpy_extras.object_utils import world_to_camera_view
from farms_blender.core.camera import create_multiview_camera
from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.display import display_farms_types
from farms_blender.core.freestyle import (configure_lineset, create_lineset,
                                          default_lineset_config,
                                          enable_freestyle, remove_lineset,
                                          set_freestyle_setting)
from farms_blender.core.materials import farms_material, flat_shade_material
from farms_blender.core.objects import objs_of_farms_types
from farms_blender.core.pose import set_model_pose
from farms_blender.core.resources import get_resource_object
from farms_blender.core.render import auto_crop_image
from farms_blender.core.sdf import get_base_link, load_sdf
from farms_data.io.yaml import read_yaml
from farms_models.utils import get_sdf_path
from loading import load_model, load_muscles

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
FIGURES_PATH = SCRIPT_PATH.joinpath(
    "..", "..", "src", "figures", "skeleton"
)


def configure_scene(**kwargs):
    """ Configure scene. """
    #: Disable background
    bpy.data.scenes['Scene'].render.film_transparent = True
    #: Enable freestyle
    enable_freestyle()


def configure_freestyle():
    """ Configure line style settings. """
    #: Create collections for visual objects
    for obj, _ in objs_of_farms_types(visual=True):
        add_object_to_collection(obj, collection='visuals', create=True)

    #: Genric settings
    set_freestyle_setting('crease_angle', 0)
    #: Remove default lineset
    remove_lineset('LineSet')
    #: Create line sets for each elem
    linesets = {
        name: create_lineset(name)
        for name in ["visuals", ]
    }
    lineset_config = default_lineset_config()
    lineset_config['select_by_visibility'] = True
    lineset_config['select_by_collection'] = True
    lineset_config['select_contour'] = True
    lineset_config['select_silhouette'] = False
    lineset_config['select_crease'] = False
    lineset_config["select_border"] = False
    lineset_config['select_external_contour'] = True

    for name, lineset in linesets.items():
        lineset_config["collection"] = bpy.data.collections['visuals']
        configure_lineset(lineset, **lineset_config)


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1., 0.0038, 0.0076), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'ORTHO', "lens": 50, "scale": 0.175
    }
    #: Create camera 0
    camera_link = create_multiview_camera(
        0, camera_options
    )
    return camera_link


def add_axes():
    """ Add axis """
    axes = get_resource_object(
        resource_object="link", name="axes"
    )
    axes.scale = [1e-2]*3
    axes.location = (0.0, 0.075, -0.025)
    return axes


def export_figures(export_path: str):
    """Export figures

    Parameters
    ----------
    export_path : <str>
        File path for exporting the figures
    """
    #: Choose camera 0
    bpy.data.scenes['Scene'].camera = bpy.data.objects['Camera_0']
    # render settings
    bpy.context.scene.render.filepath = os.path.join(export_path)
    bpy.ops.render.render(write_still=1)
    # Auto crop image
    auto_crop_image(export_path)


def main():
    """ main """
    #: Load default scene
    mouse_scene.scene(scale=1e-2)

    #: Configure scene
    configure_scene()

    #: Load model
    model_name, model_objs = load_model("mouse_with_joint_limits.sdf")

    # Add axes
    add_axes()

    #: add cameras
    add_cameras()

    #: Configure freestyle
    configure_freestyle()

    #: Display
    display = {
        'view': True,
        'render': True,
        'link': False,
        'visual': True,
        'collision': False,
        'inertial': False,
        'com': False,
        'muscle': False,
        'joint': False,
        'joint_axis': False,
    }
    display_farms_types(objs=model_objs, **display)

    #: Set the pose of base_link
    base_link = get_base_link(model_name)
    base_link.location = (0.0, 0.0, 0.0125)

    #: Export figures
    export_figures(export_path=FIGURES_PATH.joinpath("neutral_pose.png"))

    #: Set pose
    model_pose = read_yaml('../../data/config/locomotion_pose.yaml')
    set_model_pose(model_pose, units='degrees')
    base_link.rotation_euler = ((np.deg2rad(45.0), 0.0, 0.0))

    # Export figures
    export_figures(export_path=FIGURES_PATH.joinpath("locomotion_pose.png"))


if __name__ == '__main__':
    main()
