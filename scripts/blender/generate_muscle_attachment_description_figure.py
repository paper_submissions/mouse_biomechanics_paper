""" Generate figure for muscle attachments example. """
import os
import pathlib

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib.collections import PatchCollection
from matplotlib.patches import Ellipse, Rectangle
from scipy.spatial.transform import Rotation

import bpy
import mathutils
from bpy_extras.object_utils import world_to_camera_view
from farms_blender.core.camera import create_multiview_camera
from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.display import display_farms_types
from farms_blender.core.freestyle import (configure_lineset, create_lineset,
                                          default_lineset_config,
                                          enable_freestyle, remove_lineset,
                                          set_freestyle_setting)
from farms_blender.core.materials import flat_shade_material, transparent_material
from farms_blender.core.primitives import create_sphere
from farms_blender.core.resources import get_resource_object
from farms_blender.core.scene import set_scene_settings
from farms_blender.core.sdf import get_base_link
from farms_blender.core.transforms import (blender_global_coordinates,
                                           set_parent)
from farms_blender.core.utils import clear_world
from farms_utils.plotting import PlotType, get_fig_size
from loading import load_model, load_muscles


#: Set plot properties
#: Load default plot params
with open('../../data/config/plotting.yaml', 'r') as stream:
    plot_params = yaml.load(stream, yaml.FullLoader)

#: Set plot properties
plt.rcParams.update(plot_params)

SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
SDF_PATH = DATA_PATH.joinpath("models", "sdf")
FIGURES_PATH = SCRIPT_PATH.joinpath(
    "..", "..", "src", "figures", "muscles", "attachment"
)

def setup_model():
    """Set up the model """
    # Load sdf model
    model_name, model_objs = load_model("right_femur_tibia.sdf")
    # Load muscles
    muscle_objs = load_muscles(model_name, "dummy.yaml")
    # Visual elements
    #: Elements to display. Can be changed later
    display = {
        'view': True, 'render': True, 'link': False, 'visual': True,
        'collision': False, 'inertial': False, 'com': False,
        'muscle': True, 'joint': False, 'joint_axis': False
    }
    display_farms_types(**display)
    # Create material for bones
    material = transparent_material()
    for visual in ("RFemur_visual", "RTibia_visual"):
        obj = bpy.data.scenes["Scene"].objects.get(visual)
        obj.active_material = material
    # Create material for muscle
    obj = bpy.data.scenes["Scene"].objects.get("DUMMY")
    obj.active_material = flat_shade_material(color=(0.0, 0.0, 0.75, 1.0))
    return model_name, model_name, muscle_objs


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1.11, 0.005357, -0.022), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'ORTHO', "lens": 50, "scale": 0.025
    }
    #: Create camera 0
    camera_joint = create_multiview_camera(
        0, camera_options
    )
    #: Configure camera settings
    bpy.data.scenes['Scene'].render.resolution_x = 1080
    bpy.data.scenes['Scene'].render.resolution_y = 1920


def add_lights():
    """ Add lights """
    #: Add sun
    sun = bpy.data.lights.new(name="sun", type='SUN')
    sun_obj = bpy.data.objects.new(name="sun", object_data=sun)
    bpy.context.collection.objects.link(sun_obj)
    bpy.data.lights["sun"].energy = 4.0


def configure_scene():
    """ Configure scene. """
    #: Disable background
    bpy.data.scenes['Scene'].render.film_transparent = True
    #: Enable freestyle
    enable_freestyle()


def configure_lineset(lineset, **kwargs):
    """ Configure freestyle lineset. """
    for key, value in kwargs.items():
        setattr(lineset, key, value)


def configure_freestyle():
    """ Configure line style settings. """
    #: Genric settings
    bpy.data.scenes["Scene"].render.line_thickness = 2.0
    set_freestyle_setting('crease_angle', 0)
    #: Remove default lineset
    remove_lineset('LineSet')
    #: Create line sets for each elem
    linesets = {
        name: create_lineset(name)
        for name in ["RFemur_visual", "RTibia_visual"]
    }
    # Add collections
    for name in ["RFemur_visual", "RTibia_visual"]:
        add_object_to_collection(
            bpy.data.scenes["Scene"].objects[name], name, create=True
        )
    lineset_config = default_lineset_config()
    lineset_config['select_by_visibility'] = False
    lineset_config['select_by_collection'] = True
    lineset_config['select_contour'] = True
    lineset_config['select_silhouette'] = False
    lineset_config['select_crease'] = False
    lineset_config["select_border"] = False
    lineset_config['select_external_contour'] = True

    for name, lineset in linesets.items():
        lineset_config["collection"] = bpy.data.collections[name]
        if name == "RFemur_visual":
            lineset_config["select_contour"] = False
        if name == "RTibia_visual":
            lineset_config["select_border"] = True
            lineset_config["select_contour"] = False
        configure_lineset(lineset, **lineset_config)


def export_figures(export_path: str):
    """Export figures

    Parameters
    ----------
    export_path : <str>
        File path for exporting the figures
    """
    # Choose camera 0
    bpy.data.scenes['Scene'].camera = bpy.data.objects['Camera_0']
    for joint_angle, name in zip((-60, -120), ("min", "max")):
        bpy.context.scene.render.filepath = str(
            export_path.joinpath(f"muscle_path_force_blender_{name}.png")
        )
        joint_name = "RKnee_flexion"
        joint = bpy.data.scenes["Scene"].objects.get(joint_name)
        xyz = mathutils.Vector(joint['farms_axis_xyz'])
        joint.rotation_euler = (
            joint.matrix_world.to_euler().to_matrix()
        )@(xyz*np.deg2rad(joint_angle))
        bpy.ops.render.render(write_still=1)


def main():
    """ Main """
    # Clear world
    clear_world()
    # Joint frame
    setup_model()
    # Add cameras
    add_cameras()
    # Add lights
    add_lights()
    # Configure scene
    configure_scene()
    # Configure freestyle
    configure_freestyle()
    # Export figures
    export_figures(export_path=FIGURES_PATH)


if __name__ == '__main__':
    main()
