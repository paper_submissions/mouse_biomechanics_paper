import os
import pathlib
import sys
import time
from argparse import ArgumentParser

import numpy as np

import bpy
import mathutils
import mouse_scene
from farms_blender.core import (camera, materials, muscles, pose, scene, sdf,
                                utils)
from farms_blender.core.display import display_farms_types
from farms_blender.core.scene import set_scene_settings
from farms_blender.core.sdf import get_base_link, load_sdf
from mathutils import Euler, Matrix, Vector

from loading import load_scene

sys.path.append(bpy.data.filepath)

SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()


def parse_args():
    """ Parge arguments """
    argv = sys.argv
    argv = argv[argv.index('--') + 1:] if '--' in argv else ''
    parser = ArgumentParser()
    #: render
    parser.add_argument("--hindlimbmuscle", "-hm", required=False,
                        default=True, dest="hindlimb_muscle")
    parser.add_argument("--forelimbmuscle", "-fm", required=False,
                        default=True, dest="forelimb_muscle")
    return parser.parse_args(argv)


def main(args):
    """ Main """
    #: Setup scene
    mouse_scene.scene(clear_world=False)

    #: Load animat
    model_name, model_objs, _, _ = load_scene(
        load_hind_muscles=True, load_fore_muscles=True,
        set_pose=True, model_pose="../../data/config/locomotion_pose.yaml"
    )
    #: Elements to display. Can be changed later
    display = {
        'view': True,
        'render': True,
        'link': False,
        'visual': True,
        'collision': False,
        'inertial': False,
        'com': False,
        'muscle': True,
        'joint': True,
        'joint_axis': False
    }
    display_farms_types(**display)

    #: set pose
    # model_pose = utils.read_yaml("../../data/config/pose/neutral_pose_with_joint_constrains.yaml")
    # model_pose = utils.read_yaml("../../data/config/pose/default_link_pose.yaml")
    # model_pose = utils.read_yaml('../../data/config/locomotion_pose.yaml')
    # pose.set_model_default_pose(model_pose, units='degrees')["joints"]

    set_scene_settings(renderer='BLENDER_EEVEE')

    bpy.context.scene.view_settings.view_transform = 'Raw'
    bpy.context.scene.cycles.samples = 32
    bpy.context.scene.render.engine = 'CYCLES'
    bpy.context.scene.render.film_transparent = True
    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.image_settings.color_mode = 'RGBA'

    bpy.context.scene.render.resolution_x = 1080
    bpy.context.scene.render.resolution_y = 1920

    #: Create camera
    camera_ortho_scale = 0.025
    camera_view = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([-1.0, -0.1, 0.0]),
            'rot': camera.look_at(
                Vector([-1.0, -0.1, 0.0]), Vector([0., 0., 0.])
            ),
            'type': 'ORTHO',
            'lens': 50,
            'scale': camera_ortho_scale
        }
    )
    camera_view.name = "side_view"

    camera.remove_default_multiview_cameras()


if __name__ == '__main__':
    import time
    start_time = time.time()
    main(args=parse_args())
    print("--- %s seconds ---" % (time.time() - start_time))
