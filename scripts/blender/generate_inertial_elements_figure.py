import os
import pathlib
import sys

from argparse import ArgumentParser

from farms_blender.core import muscles, scene, utils, materials
from farms_blender.core.display import display_farms_types

import bpy
import mathutils

import time


SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()


def add_hdri_image_for_lighting(hdri_file_path, strength=1.0):
    """ Add a hdri image for lighting the scene """
    world = bpy.context.scene.world
    world.use_nodes = True
    node_tree = bpy.context.scene.world.node_tree
    enode = node_tree.nodes.new("ShaderNodeTexEnvironment")
    enode.image = bpy.data.images.load(hdri_file_path)
    node_tree.links.new(
        enode.outputs['Color'],
        node_tree.nodes['Background'].inputs['Color']
    )
    #: Set strength
    node_tree.nodes["Background"].inputs[1].default_value = strength


def main(args):
    """ Main """
    sdf_path = os.path.join(
        SCRIPT_PATH, "..", "..", "data", "models", "sdf", "{}.sdf"
    )

    #: Clear the world
    utils.clear_world()

    #: Load animat
    scene.load_animat(
        filename=sdf_path.format("mouse"),
        resources_scale=0.05
    )
    #: Elements to display. Can be changed later
    display = {
        'view': True,
        'render': True,
        'link': True,
        'visual': True,
        'collision': False,
        'inertial': True,
        'com': False,
        'muscle': True,
        'joint': False,
        'joint_axis': False,
        'position': False,
        'velocity': False,
        'torque': False,
        'reaction': False,
        'friction': False,
        'hydro_force': False,
        'hydro_torque': False,
    }
    display_farms_types(**display)

    #:
    display['link'] = False
    display['joint'] = False
    display_farms_types(**display)

    set_scene_settings(renderer='BLENDER_EEVEE', freestyle=True)
    #: Add hdri
    add_hdri_image_for_lighting(
        "./photo_studio_01_2k.hdr", strength=0.7
    )
    bpy.context.scene.view_settings.view_transform = 'Raw'
    bpy.context.scene.cycles.samples = 32
    bpy.context.scene.render.engine = 'CYCLES'
    bpy.context.scene.render.film_transparent = True
    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.image_settings.color_mode = 'RGBA'

    #: Load model
    generate_hindlimb()

    #: Create camera
    camera_ortho_scale = 0.025
    camera_view = camera.create_multiview_camera(
        index=1, options={
            'loc': np.asarray([-1.0, -0.1, 0.0]),
            'rot': camera.look_at(
                Vector([-1.0, -0.1, 0.0]), Vector([0., 0., 0.])
            ),
            'type': 'ORTHO',
            'lens': 50,
        }
    )
    camera_view.data.ortho_scale = camera_ortho_scale
    camera_view.name = "side_view"

    camera.remove_default_multiview_cameras()


if __name__ == '__main__':
    import time
    start_time = time.time()
    main(args=parse_args())
    print("--- %s seconds ---" % (time.time() - start_time))
