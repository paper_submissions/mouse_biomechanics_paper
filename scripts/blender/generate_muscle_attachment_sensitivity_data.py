"""Generate data for muscle insertion and origin points.

Origin and insertion are varied by +/- 0.5mm by default

"""
""" Generate muscle-data for hindlimb and forelimb. """

import os
import pathlib
import sys
import time
from collections import defaultdict

from farms_data.io import io
import farms_pylog as pylog
import h5py

import bpy
import mathutils
from farms_blender.core import muscles, scene, utils
from farms_blender.core.display import display_farms_types

script_path = os.path.abspath(bpy.data.filepath)
sys.path.append(script_path)
from loading import load_model, load_muscles


def create_data_groups(data_file, muscle_joint_set):
    """ Add new muscle group. """
    for limb, muscle_joint in muscle_joint_set.items():
        limb_group = data_file.create_group(name=limb)
        for muscle, joints in muscle_joint.items():
            muscle_group = limb_group.create_group(name=muscle)
            offset_groups = [
                muscle_group.create_group(name=offset)
                for offset in (
                        "no_offset",
                        "origin_positive_offset",
                        "origin_negative_offset",
                        "insertion_positive_offset",
                        "insertion_negative_offset",
                )
            ]
            joint_groups = [
                offset_group.create_group(name=joint)
                for offset_group in offset_groups
                for joint in joints
            ]


def generate_muscle_data(
        limb_name, muscle_name, joint_names, offset_group, **kwargs
):
    """ Compute muscle-joint data. """
    num_steps = kwargs.pop("num_steps", 25)
    muscle_suffix = "RIGHT_{}_{}"
    joint_suffix = "R{}"
    muscle_full_name = muscle_suffix.format(limb_name, muscle_name)
    # muscle_params = muscles.extract_muscle_properties_data(
    #     bpy.data.objects[muscle_full_name], extract_waypoints=False
    # )
    # for key, value in muscle_params.items():
    #     muscle_group.attrs[key] = value
    for joint_name in joint_names:
        joint_full_name = joint_suffix.format(joint_name)
        pylog.debug(
            "Generating offset data : {} - {}".format(
                muscle_name, joint_full_name
            )
        )
        data = muscles.analyze_muscle(
            muscle_full_name, joint_full_name, primary_num_steps=num_steps
        )
        joint_group = offset_group[joint_name]
        joint_group.attrs['primary_joint_name'] = joint_name
        for key, value in data.items():
            joint_group.create_dataset(name=key, data=value)


def set_offset_to_muscle_attachments(
        muscle_objs: list, attachment_type: str, offset: tuple
):
    """ set offset to muscle attachments of either origin or insertion """
    for muscle in muscle_objs:
        if attachment_type == "origin":
            hook = muscle.modifiers[0]
        elif attachment_type == "insertion":
            hook = muscle.modifiers[-1]
        else:
            return None
        #: Update the location
        attachment = hook.object
        attachment.location = [
            loc + offset
            for loc in attachment.location
        ]


def main():
    """ Main """
    export_path = os.path.join(
        script_path, "..", "..", "data", "results", "muscle_analysis"
    )

    muscles_config_path = os.path.join(
        script_path, "..", "..", "data", "config", "muscles", "{}.yaml"
    )

    config_path = os.path.join(
        script_path, "..", "..", "data", "config", "{}.yaml"
    )

    #: Load model
    utils.clear_world()
    #: Load animat
    model_name, model_objs = load_model("mouse_with_joint_limits")

    #: Load muscle
    forelimb_muscle_config = io.read_yaml(
        muscles_config_path.format("forelimb")
    )

    hindlimb_muscle_config = io.read_yaml(
        muscles_config_path.format("hindlimb")
    )

    muscle_objs = load_muscles(forelimb_muscle_config)
    muscle_objs += load_muscles(hindlimb_muscle_config)

    #: Update display
    #: Elements to display. Can be changed later
    display = { 'view': True, 'render': False, 'link': False,
        'visual': False, 'collision': False, 'inertial': False,
        'com': False, 'muscle': True, 'joint': False, 'joint_axis': False
    }
    display_farms_types(objs=[*model_objs, *muscle_objs], **display)

    #: Read the muscle joint relationship
    joint_muscle_relation = utils.read_yaml(
        config_path.format("joint_muscle_relationship")
    )

    #: Create h5 data-file
    file_path = os.path.join(export_path, "muscle_attachment_sensitivity_data.h5")
    data_file = h5py.File(file_path, 'w')

    #: Generate muscle_joint set
    muscle_joint_set = {
        limb_name : defaultdict(list)
        for limb_name in joint_muscle_relation.keys()
    }

    for limb_name, limb_data in joint_muscle_relation.items():
        for joint_group, joint_data in limb_data.items():
            for muscle_name, joint_name in joint_data:
                muscle_joint_set[limb_name][muscle_name].append(joint_name)

    #: Create data
    create_data_groups(data_file, muscle_joint_set)

    #: offsets
    offsets = {
        "no_offset" : 0.0,
        "origin_positive_offset" : 0.5*1e-3,
        "origin_negative_offset" : -0.5*1e-3,
        "insertion_positive_offset" : 0.5*1e-3,
        "insertion_negative_offset" : -0.5*1e-3,
    }
    #: Loop over the muscles
    for offset_name, offset_value in offsets.items():
        if "origin" in  offset_name:
            attachment_type = "origin"
        elif "insertion" in offset_name:
            attachment_type = "insertion"
        else:
            attachment_type = None
        set_offset_to_muscle_attachments(
            muscle_objs, attachment_type=attachment_type,
            offset=offset_value
        )
        for limb_name, muscle_joint in muscle_joint_set.items():
            for muscle_name, joint_names in muscle_joint.items():
                offset_group = data_file[limb_name][muscle_name][offset_name]
                generate_muscle_data(
                    limb_name, muscle_name, joint_names, offset_group
                )
        set_offset_to_muscle_attachments(
            muscle_objs, attachment_type=attachment_type,
            offset=-1*offset_value
        )
    #: Close h5 file
    data_file.close()


if __name__ == '__main__':
    import time
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
