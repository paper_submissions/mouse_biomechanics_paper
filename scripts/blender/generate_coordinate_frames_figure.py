""" Generate figure for coordinates frames. """
import os
from pathlib import Path

import matplotlib.gridspec as gridspec
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib.collections import PatchCollection
from matplotlib.patches import Ellipse, Rectangle
from scipy.spatial.transform import Rotation

import bpy
import mathutils
from bpy_extras.object_utils import world_to_camera_view
from farms_blender.core.camera import create_multiview_camera
from farms_blender.core.collections import add_object_to_collection
from farms_blender.core.display import display_farms_types
from farms_blender.core.freestyle import (configure_lineset, create_lineset,
                                          default_lineset_config,
                                          enable_freestyle, remove_lineset,
                                          set_freestyle_setting)
from farms_blender.core.materials import farms_material, transparent_material
from farms_blender.core.primitives import create_sphere
from farms_blender.core.resources import get_resource_object
from farms_blender.core.scene import set_scene_settings
from farms_blender.core.sdf import get_base_link
from farms_blender.core.transforms import (blender_global_coordinates,
                                           set_parent)
from farms_blender.core.utils import clear_world
from farms_utils.plotting import PlotType, get_fig_size

#: Set plot properties
#: Load default plot params
with open('../../data/config/plotting.yaml', 'r') as stream:
    plot_params = yaml.load(stream, yaml.FullLoader)

#: Set plot properties
plt.rcParams.update(plot_params)

SCRIPT_PATH = Path(__file__).parent.absolute()
DATA_PATH = SCRIPT_PATH.joinpath("..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
MODELS_PATH = DATA_PATH.joinpath("models")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
FIGURES_PATH = SCRIPT_PATH.joinpath("..", "..", "src", "figures", "frames")


def convert_coord_to_matplotlib(camera, coord, scene=bpy.context.scene):
    """ Convert coordinates from camera view to matplotlib.
    reference : https://blender.stackexchange.com/a/1257
    """

    # needed to rescale 2d coordinates
    render = scene.render
    res_x = render.resolution_x
    res_y = render.resolution_y
    bpy.context.view_layer.update()
    coords_2d = world_to_camera_view(scene, camera, coord)
    return (
        res_x-round(res_x*coords_2d[0]), res_y-round(res_y*coords_2d[1])
    )


def homogeneous_matrix_2d(translation=(0., 0.), rotation=0, scale=1.0):
    """ Construct a homogeneous_matrix for 2d transformations. """
    #: Identity matrix
    homogeneous_matrix = np.identity(3)
    #: Rotation matrix
    homogeneous_matrix[:2, :2] = (Rotation.from_euler(
        'xyz', (0.0, 0.0, rotation))).as_matrix()[:2, :2]
    #: Translation matrix
    homogeneous_matrix[:2, -1] = np.asarray(translation)
    #: Scale matrix
    scale_matrix = np.identity(3)
    scale_matrix[0, 0], scale_matrix[1, 1] = scale, scale
    homogeneous_matrix = np.einsum(
        'ij, jk -> ik', homogeneous_matrix, scale_matrix)
    return homogeneous_matrix


def draw_axis(ax, **kwargs):
    """

    Parameters
    ----------
    **kwargs :


    Returns
    -------
    out :

    """
    #: Default coordinates
    xaxis_coordinates = np.asarray([[0., 0., 1.], [1., 0., 1.]])
    yaxis_coordinates = np.asarray([[0., 0., 1.], [0., 1., 1.]])
    #: Transformations
    origin = kwargs.pop('origin', (0, 0))
    scale = kwargs.pop('scale', 1)
    rotation = kwargs.pop('rotation', np.deg2rad(0))
    #: Arrow props
    width = kwargs.pop('width', 0.01)*scale
    head_width = kwargs.pop('head_width',  5*width)
    head_length = kwargs.pop('head_length', 1.5*head_width)
    x_color = kwargs.pop('x_color', 'r')
    y_color = kwargs.pop('y_color', 'g')
    #: Compute transformation matrix
    hmat = homogeneous_matrix_2d(origin, rotation, scale)
    #: Compute the transformed coordinates
    trans_xaxis_coordinates = np.einsum(
        'ij, kj -> ki', hmat, xaxis_coordinates)
    trans_yaxis_coordinates = np.einsum(
        'ij, kj -> ki', hmat, yaxis_coordinates)
    ax.arrow(
        x=trans_xaxis_coordinates[0, 0], y=trans_xaxis_coordinates[0, 1],
        dx=trans_xaxis_coordinates[1, 0]-trans_xaxis_coordinates[0, 0],
        dy=trans_xaxis_coordinates[1, 1]-trans_xaxis_coordinates[0, 1],
        width=width, head_width=head_width, head_length=head_length,
        color=x_color, length_includes_head=True, head_starts_at_zero=True
    )
    ax.arrow(
        x=trans_yaxis_coordinates[0, 0], y=trans_yaxis_coordinates[0, 1],
        dx=trans_yaxis_coordinates[1, 0]-trans_yaxis_coordinates[0, 0],
        dy=trans_yaxis_coordinates[1, 1]-trans_yaxis_coordinates[0, 1],
        width=width, head_width=head_width, head_length=head_length,
        color=y_color, length_includes_head=True, head_starts_at_zero=True
    )
    # ax.annotate(
    #     "", xy=trans_xaxis_coordinates[1, :2], xycoords='data',
    #     xytext=trans_xaxis_coordinates[0, :2], textcoords='data',
    #     color=x_color, arrowprops=dict(
    #         width=width, headwidth=head_width, headlength=head_length,
    #         color=x_color,
    #     ),
    # )
    # ax.annotate(
    #     "", xy=trans_yaxis_coordinates[1, :2], xycoords='data',
    #     xytext=trans_yaxis_coordinates[0, :2], textcoords='data',
    #     color='k', arrowprops=dict(
    #         width=width, headwidth=head_width, headlength=head_length,
    #         color=y_color,
    #     ),
    # )


def load_link_frame():
    """Load link frame elements """
    #: Create materials
    material = transparent_material()
    #: Load Femur
    bpy.ops.import_mesh.stl(
        filepath="../../data/models/meshes/stl/LFemur.stl"
    )
    obj = bpy.context.active_object
    obj.active_material = material
    obj.location = (0.0, -0.2, 0.0)
    obj.name = "link_femur"
    add_object_to_collection(obj, "link_femur", create=True)


def load_joint_frame():
    """Load joint frame elements """
    #: Create materials
    material = transparent_material()
    #: Load Femur
    bpy.ops.import_mesh.stl(
        filepath="../../data/models/meshes/stl/LFemur.stl"
    )
    obj = bpy.context.active_object
    obj.active_material = material
    obj.location = (0.005700721168518066,
                    0.0060809830904006955, 0.00046869623064994814)
    obj.name = "joint_femur"
    add_object_to_collection(obj, "joint_femur", create=True)
    #: Load Tibia
    bpy.ops.import_mesh.stl(
        filepath="../../data/models/meshes/stl/LTibia.stl"
    )
    obj = bpy.context.active_object
    obj.active_material = material
    obj.location = (0.005694888854026794,
                    0.006371295547485351, -0.020586770057678224)
    obj.name = "joint_tibia"
    add_object_to_collection(obj, "joint_tibia", create=True)


def add_cameras():
    """Add cameras """
    camera_options = {
        "loc": (1., -0.2, -0.01), "rot": (np.pi/2, 0., np.pi/2.),
        "type": 'ORTHO', "lens": 50, "scale": 0.03
    }
    #: Create camera 0
    camera_link = create_multiview_camera(0, camera_options)

    #: Create camera 1
    camera_options['loc'] = (1.11, 0.0065, -0.022)
    camera_options['scale'] = 0.06
    camera_joint = create_multiview_camera(
        1, camera_options
    )
    #: Configure camera settings
    bpy.data.scenes['Scene'].render.resolution_x = 1080
    bpy.data.scenes['Scene'].render.resolution_y = 1920
    return camera_link, camera_joint


def add_lights():
    """ Add lights """
    #: Add sun
    sun = bpy.data.lights.new(name="sun", type='SUN')
    sun_obj = bpy.data.objects.new(name="sun", object_data=sun)
    bpy.context.collection.objects.link(sun_obj)


def configure_scene():
    """ Configure scene. """
    #: Disable background
    bpy.data.scenes['Scene'].render.film_transparent = True
    #: Enable freestyle
    enable_freestyle()


def configure_lineset(lineset, **kwargs):
    """ Configure freestyle lineset. """
    for key, value in kwargs.items():
        setattr(lineset, key, value)


def configure_freestyle():
    """ Configure line style settings. """
    #: Genric settings
    set_freestyle_setting('crease_angle', 0)
    #: Remove default lineset
    remove_lineset('LineSet')
    #: Create line sets for each elem
    linesets = {
        name: create_lineset(name)
        for name in ["link_femur", "joint_femur", "joint_tibia"]
    }
    lineset_config = default_lineset_config()
    lineset_config['select_by_visibility'] = False
    lineset_config['select_by_collection'] = True
    lineset_config['select_contour'] = True
    lineset_config['select_silhouette'] = False
    lineset_config['select_crease'] = False
    lineset_config["select_border"] = False
    lineset_config['select_external_contour'] = True

    for name, lineset in linesets.items():
        lineset_config["collection"] = bpy.data.collections[name]
        if name == "joint_tibia":
            lineset_config["select_border"] = True
            lineset_config["select_contour"] = False
        configure_lineset(lineset, **lineset_config)


def export_figures(export_path: Path):
    """Export figures

    Parameters
    ----------
    export_path : <str>
        File path for exporting the figures
    """
    #: Choose camera 0
    bpy.data.scenes['Scene'].camera = bpy.data.objects['Camera_0']
    # render settings
    bpy.context.scene.render.filepath = str(export_path.joinpath(
        "coordinate_frame_link_blender"
    ))
    bpy.ops.render.render(write_still=1)
    #: Choose camera 0
    bpy.data.scenes['Scene'].camera = bpy.data.objects['Camera_1']
    # render settings
    bpy.context.scene.render.filepath = str(export_path.joinpath(
        "coordinate_frame_joint_blender"
    ))
    bpy.ops.render.render(write_still=1)


def edit_link_frame_in_matplotlib():
    """Edit link frame figure exported from blender with matplotlib.
    """
    #: Link camera
    camera = bpy.data.objects['Camera_0']
    link_femur_obj = bpy.data.objects['link_femur']
    #: Inertial position
    inertial = (0.0005167, -0.00055, -0.01068)
    #: Read the image
    image = mpimg.imread(
        FIGURES_PATH.joinpath("coordinate_frame_link_blender.png")
    )
    #: Figure in matplotlib
    fig, ax = plt.subplots(
        figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="single", subplots=(1, 1), fraction=0.5,
            height_fraction=1.0
        )
    )
    ax.imshow(image)
    #: Link
    link_frame_pos = convert_coord_to_matplotlib(
        camera, link_femur_obj.location)
    link_frame_text_pos = mathutils.Vector(
        link_frame_pos) + mathutils.Vector((50, -125))
    plt.text(
        *link_frame_text_pos, s="Link",
        size=plot_params['legend.fontsize'],
        rotation=0., ha="left", va="top", usetex=True
    )
    draw_axis(
        ax,
        origin=link_frame_pos,
        scale=150, width=1e-7, head_width=2.25, head_length=4,
        rotation=np.deg2rad(-90), x_color='k', y_color='k'
    )
    #: Inertial
    inertial_frame_pos = convert_coord_to_matplotlib(
        camera,
        blender_global_coordinates(
            link_femur_obj, mathutils.Vector(inertial))
    )
    inertial_frame_text_pos = mathutils.Vector(inertial_frame_pos) + \
        mathutils.Vector((50, -125))
    plt.text(*inertial_frame_text_pos, s="Inertial",
             size=plot_params['legend.fontsize'], rotation=0.,
             ha="left", va="top", usetex=True)
    draw_axis(
        ax,
        origin=inertial_frame_pos,
        scale=150, width=1e-7, head_width=2.25, head_length=4,
        rotation=np.deg2rad(-90), x_color='b', y_color='b'
    )
    #: Collision
    collision_rect_anchor = convert_coord_to_matplotlib(
        camera, mathutils.Vector((0.00188, -0.1965, 0.002)))
    collision_frame_pos = convert_coord_to_matplotlib(
        camera,
        link_femur_obj.location + mathutils.Vector((0., -0.003, -0.006))
    )
    collision_frame_text_pos = mathutils.Vector(collision_frame_pos) + \
        mathutils.Vector((50, -125))
    plt.text(*collision_frame_text_pos, s="Collision",
             size=plot_params['legend.fontsize'],
             rotation=0., ha="left", va="top", usetex=True)
    rect = Rectangle(
        collision_rect_anchor, 375, 1600,
        linewidth=1, edgecolor='r', facecolor='none', alpha=0.5
    )
    ax.add_patch(rect)
    draw_axis(
        ax,
        origin=collision_frame_pos,
        scale=150, width=1e-7, head_width=2.25, head_length=4,
        rotation=np.deg2rad(-90), x_color='r', y_color='r'
    )
    #: Visual
    # origin = convert_coord_to_matplotlib(
    #     camera,
    #     link_femur_obj.location + mathutils.Vector((0., 0.001, -0.01))
    # )
    # vis = Ellipse(
    #     origin, 400, 1600,
    #     linewidth=3, edgecolor='g', facecolor='g', fill=False
    # )
    # ax.add_patch(vis)
    visual_frame_pos = convert_coord_to_matplotlib(
        camera,
        link_femur_obj.location + mathutils.Vector((0., 0.001, -0.005))
    )
    visual_frame_text_pos = mathutils.Vector(visual_frame_pos) + \
        mathutils.Vector((-25, -125))
    plt.text(*visual_frame_text_pos, s="Visual", size=plot_params['legend.fontsize'],
             rotation=0., ha="right", va="top", usetex=True)
    draw_axis(
        ax,
        origin=visual_frame_pos,
        scale=150, width=1e-7, head_width=2.25, head_length=4,
        rotation=np.deg2rad(-90), x_color='g', y_color='g'
    )
    #: Transformations
    ax.annotate("",
                xy=collision_frame_pos, xycoords='data',
                xytext=link_frame_pos, textcoords='data',
                size=plot_params['legend.fontsize'], va="center", ha="center",
                bbox=dict(boxstyle="round4", fc="w"),
                arrowprops=dict(arrowstyle="-|>",
                                connectionstyle="arc3,rad=-0.2",
                                fc="w")
                )
    ax.annotate("",
                xy=visual_frame_pos, xycoords='data',
                xytext=link_frame_pos, textcoords='data',
                size=plot_params['legend.fontsize'], va="center", ha="center",
                bbox=dict(boxstyle="round4", fc="w"),
                arrowprops=dict(arrowstyle="-|>",
                                connectionstyle="arc3,rad=-0.2",
                                fc="w"),
                )
    ax.annotate("",
                xy=inertial_frame_pos, xycoords='data',
                xytext=link_frame_pos, textcoords='data',
                size=plot_params['legend.fontsize'], va="center", ha="center",
                bbox=dict(boxstyle="round4", fc="w"),
                arrowprops=dict(arrowstyle="-|>",
                                connectionstyle="arc3,rad=-0.2",
                                fc="w"),
                )
    ax.set_axis_off()
    # plt.show()
    fig.savefig(
        FIGURES_PATH.joinpath('coordinate_frame_link.pdf'), dpi=300,
        bbox_inches='tight',
        transparent=True,
    )
    fig.savefig(
        FIGURES_PATH.joinpath('coordinate_frame_link.png'), dpi=300,
        bbox_inches='tight',
        transparent=True,
    )


def edit_joint_frame_in_matplotlib():
    """Edit joint frame figure exported from blender with matplotlib.
    """
    #: joint camera
    camera = bpy.data.objects['Camera_1']
    #: Femur Parent frame
    joint_femur_obj = bpy.data.objects['joint_femur']
    #: Read the image
    image = mpimg.imread(
        FIGURES_PATH.joinpath("coordinate_frame_joint_blender.png")
    )
    #: Figure in matplotlib
    fig, ax = plt.subplots(
        figsize=get_fig_size(
            PlotType.PUBLICATION, journal_name="ieee_access",
            columns="single", subplots=(1, 1), fraction=0.5,
            height_fraction=1.0
        )
    )
    ax.imshow(image)
    #: Parent
    parent_frame_pos = convert_coord_to_matplotlib(
        camera, joint_femur_obj.location)
    parent_frame_text_pos = mathutils.Vector(
        parent_frame_pos) + mathutils.Vector((50, -125))
    plt.text(
        *parent_frame_text_pos, s="Parent",
        size=plot_params['legend.fontsize'], rotation=0.,
        ha="left", va="top", usetex=True
    )
    draw_axis(
        ax,
        origin=parent_frame_pos,
        scale=150, width=1e-7, head_width=2.25, head_length=4,
        rotation=np.deg2rad(-90), x_color='k', y_color='k'
    )
    ax.set_axis_off()
    #: Child
    #: Tibia Child frame
    joint_tibia_obj = bpy.data.objects['joint_tibia']
    child_frame_pos = convert_coord_to_matplotlib(
        camera, joint_tibia_obj.location)
    child_frame_text_pos = mathutils.Vector(
        child_frame_pos) + mathutils.Vector((100, -125))
    plt.text(
        *child_frame_text_pos, s="Child",
        size=plot_params['legend.fontsize'], rotation=0.,
        ha="left", va="top", usetex=True
    )
    draw_axis(
        ax,
        origin=child_frame_pos,
        scale=150, width=1e-7, head_width=2.25, head_length=4,
        rotation=np.deg2rad(-90), x_color='k', y_color='k'
    )
    ax.set_axis_off()
    #: Add joint
    joint = Ellipse(
        child_frame_pos, 50, 50,
        linewidth=3, edgecolor='b', facecolor='b', fill=True
    )
    ax.add_patch(joint)
    ax.text(
        child_frame_pos[0]*0.5, child_frame_pos[1], "Joint \n Origin",
        ha="center", va="center", size=plot_params['legend.fontsize']
    )
    #: Transformations
    ax.annotate("",
                xy=child_frame_pos, xycoords='data',
                xytext=parent_frame_pos, textcoords='data',
                size=7, va="center", ha="center",
                color="m",
                arrowprops=dict(arrowstyle="-|>",
                                connectionstyle="arc3,rad=-0.4",
                                fc="w", color="m"),
                )
    # ax.text(
    #     child_frame_pos[0]*1.3, child_frame_pos[1]*0.6, "Joint Origin",
    #     ha="left", va="center", size=plot_params['legend.fontsize'], rotation=-90
    # )
    fig.savefig(
        FIGURES_PATH.joinpath('coordinate_frame_joint.pdf'), dpi=300,
        transparent=True, bbox_inches='tight')
    fig.savefig(
        FIGURES_PATH.joinpath('coordinate_frame_joint.png'), dpi=300,
        transparent=True, bbox_inches='tight')
    # plt.show()


def generate_coordinate_figure():
    """Generate coordinate figure for paper release.
    """



def main():
    """ Main """
    #: Clear world
    clear_world()
    #: Link frame
    load_link_frame()
    #: Joint frame
    load_joint_frame()
    #: Add cameras
    add_cameras()
    #: Add lights
    add_lights()
    #: Configure scene
    configure_scene()
    #: Configure freestyle
    configure_freestyle()
    # # #: Export figures
    export_figures(export_path=FIGURES_PATH)
    # #: Edit figure in matplotlib
    edit_link_frame_in_matplotlib()
    # #: Edit figure in matplotlib
    edit_joint_frame_in_matplotlib()


if __name__ == '__main__':
    main()
