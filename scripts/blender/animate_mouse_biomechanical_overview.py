"""Generate figure for the introduction of the paper.

Contains all the muscles, bones, inertials, center of mass

"""

import pathlib
import sys
import time

import farms_pylog as pylog
import h5py
import numpy as np

import bpy
import mathutils
from farms_blender.core.camera import create_camera
from farms_blender.core.display import display_farms_types
from farms_blender.core.objects import objs_of_farms_types
from farms_blender.core import pose
from farms_blender.core import transforms
from farms_blender.core.sdf import get_base_link
from farms_blender.core.utils import clear_world
from farms_data.io.yaml import read_yaml
from loading import load_scene

# Global config paths
SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
POSE_CONFIG_PATH = CONFIG_PATH.joinpath("pose")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")


def setup_scene():
    """ Load mouse model with muscles. """
    # Clear the world
    clear_world()
    # Load animat
    m_name, m_objs, forelimb_muscle_objs, hindlimb_muscle_objs = load_scene(
        set_pose=False
    )

    # Elements to display. Can be changed later
    display = {'view': True, 'render': False, 'link': True,
               'visual': True, 'collision': False, 'inertial': True,
               'com': True, 'muscle': True, 'joint': False, 'joint_axis': True
               }
    display_farms_types(objs=m_objs, **display)
    # Set the pose of the model to locomotion
    model_pose = read_yaml(POSE_CONFIG_PATH.joinpath("locomotion_pose.yaml"))
    pose.set_model_pose(model_pose, units='degrees')
    # Update base link position and rotation
    base_link = get_base_link(m_name)
    base_link.location = (0.0, 0.0, 0.016)
    base_link.rotation_euler = ((np.deg2rad(45.0), 0.0, 0.0))

    return m_name, m_objs, forelimb_muscle_objs, hindlimb_muscle_objs


def setup_camera_rig():
    """Add camera """
    camera_options = {
        "location": (0., 0.0, 0.0), "rotation_euler": (0, 0., 0),
        "type": 'PERSP', "lens": 50,
    }
    # Create camera 0
    camera = create_camera("camera", **camera_options)
    bpy.context.scene.camera = camera
    # Add empty cube
    ecube = bpy.data.objects.new("ecube", None)
    bpy.context.scene.collection.objects.link(ecube)
    ecube.empty_display_size = 0.02
    ecube.empty_display_type = 'CUBE'
    # Parent
    transforms.set_parent(camera, ecube)
    # Add bezier curve
    bpy.ops.curve.primitive_bezier_circle_add(radius=0.226, location=(0, 0, 0.043657))
    # Track obj
    esphere = bpy.data.objects.new("esphere", None)
    bpy.context.scene.collection.objects.link(esphere)
    esphere.empty_display_size = 0.02
    esphere.empty_display_type = 'SPHERE'
    esphere.location = (0, 0, 0.016)
    constraint = bpy.data.objects['camera'].constraints.new('TRACK_TO')
    constraint.target = bpy.data.objects['esphere']
    # Constraints
    constraint = bpy.data.objects['ecube'].constraints.new('FOLLOW_PATH')
    constraint.target = bpy.data.objects['BezierCircle']
    # Define camera movements
    # Spin for explosive view
    ecube.constraints[0].offset = 25.0
    ecube.constraints[0].keyframe_insert(data_path="offset", frame=0)
    ecube.constraints[0].offset = 125.0
    ecube.constraints[0].keyframe_insert(data_path="offset", frame=200)
    # Stay in place for joints
    ecube.constraints[0].offset = 125.0
    ecube.constraints[0].keyframe_insert(data_path="offset", frame=300)
    # Spin half for inertial
    ecube.constraints[0].offset = 175.0
    ecube.constraints[0].keyframe_insert(data_path="offset", frame=500)
    # Spin half for muscles
    ecube.constraints[0].offset = 225.0
    ecube.constraints[0].keyframe_insert(data_path="offset", frame=700)


def animate():
    """ Animate """

    # Hide everything except visual at the beginning
    for obj, _ in objs_of_farms_types(
            visual=False
    ):
        obj.hide_render = False
        obj.keyframe_insert(data_path="hide_render", frame=0)
    for obj, _ in objs_of_farms_types(
            inertial=True, link=True, com=True, muscle=True, joint_axis=True,
            joint=True
    ):
        obj.hide_render = True
        obj.keyframe_insert(data_path="hide_render", frame=0)
    # Show joints
    for obj, _ in objs_of_farms_types(joint_axis=True):
        obj.hide_render = False
        obj.keyframe_insert(data_path="hide_render", frame=200)
        obj.hide_render = True
        obj.keyframe_insert(data_path="hide_render", frame=300)
    # Show inertials
    for obj, _ in objs_of_farms_types(inertial=True, com=True):
        obj.hide_render = False
        obj.keyframe_insert(data_path="hide_render", frame=300)
        obj.hide_render = True
        obj.keyframe_insert(data_path="hide_render", frame=500)
    # Show muscles
    for obj, _ in objs_of_farms_types(muscle=True):
        obj.hide_render = False
        obj.keyframe_insert(data_path="hide_render", frame=500)
        obj.hide_render = True
        obj.keyframe_insert(data_path="hide_render", frame=700)

    # Explosion view for the visuals
    offset = mathutils.Vector((1e-2, 1e-2, 1e-2))
    bpy.data.scenes['Scene'].tool_settings.use_transform_pivot_point_align = True
    for obj, _ in objs_of_farms_types(visual=True):
        obj.keyframe_insert(data_path="location", frame=0)
        obj.keyframe_insert(data_path="rotation_euler", frame=0)
        obj.keyframe_insert(data_path="location", frame=200)
        obj.keyframe_insert(data_path="rotation_euler", frame=200)
        obj.location += offset
        obj.keyframe_insert(data_path="location", frame=100)
        obj.keyframe_insert(data_path="rotation_euler", frame=100)
        obj.location -= offset
    bpy.data.scenes['Scene'].tool_settings.use_transform_pivot_point_align = False

    # Show joint rotations
    pose.set_joint_pose(bpy.data.objects['Lumbar1_flexion'], np.deg2rad(0))
    for obj, _ in objs_of_farms_types(visual=True, joint=True):
        obj.keyframe_insert(data_path="location", frame=200)
        obj.keyframe_insert(data_path="rotation_euler", frame=200)
    pose.set_joint_pose(bpy.data.objects['Lumbar1_flexion'], np.deg2rad(25))
    for obj, _ in objs_of_farms_types(visual=True, joint=True):
        obj.keyframe_insert(data_path="location", frame=225)
        obj.keyframe_insert(data_path="rotation_euler", frame=225)
    pose.set_joint_pose(bpy.data.objects['Lumbar1_flexion'], np.deg2rad(0))
    for obj, _ in objs_of_farms_types(visual=True, joint=True):
        obj.keyframe_insert(data_path="location", frame=250)
        obj.keyframe_insert(data_path="rotation_euler", frame=250)
    pose.set_joint_pose(bpy.data.objects['RShoulder_flexion'], np.deg2rad(20))
    for obj, _ in objs_of_farms_types(visual=True, joint=True):
        obj.keyframe_insert(data_path="location", frame=275)
        obj.keyframe_insert(data_path="rotation_euler", frame=275)
    pose.set_joint_pose(bpy.data.objects['RShoulder_flexion'], np.deg2rad(-45))
    for obj, _ in objs_of_farms_types(visual=True, joint=True):
        obj.keyframe_insert(data_path="location", frame=300)
        obj.keyframe_insert(data_path="rotation_euler", frame=300)


def export():
    """ Export the animation """
    frame_start = 0
    frame_end = 700
    frame_step = 1

    bpy.context.scene.eevee.taa_render_samples = 32
    bpy.context.scene.render.film_transparent = True
    bpy.data.scenes['Scene'].render.resolution_x = 1920
    bpy.data.scenes['Scene'].render.resolution_y = 1080
    bpy.data.scenes['Scene'].render.fps = 24
    bpy.data.scenes['Scene'].frame_start = frame_start
    bpy.data.scenes['Scene'].frame_end = frame_end
    bpy.data.scenes['Scene'].frame_step = frame_step
    bpy.data.scenes['Scene'].render.filepath = "/tmp/biomech.mp4"
    bpy.data.scenes['Scene'].render.image_settings.file_format = 'FFMPEG'
    bpy.data.scenes['Scene'].render.image_settings.views_format = 'INDIVIDUAL'
    bpy.data.scenes['Scene'].render.ffmpeg.format = 'MPEG4'
    bpy.data.scenes['Scene'].render.ffmpeg.constant_rate_factor = 'HIGH'
    bpy.ops.render.render(animation=True)


def generate_video():
    """ Generate the introduction figure """
    # Setup the model with muscles in the scene
    setup_scene()
    # Add camera
    setup_camera_rig()
    # Animate
    animate()
    # Export
    export()


if __name__ == '__main__':
    generate_video()
