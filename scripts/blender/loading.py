""" Load mouse model """
import os
import sys
from pathlib import Path

import bpy
from farms_blender.core import muscles, pose, sdf, primitives, materials
from farms_blender.core.camera import create_multiview_camera
from farms_data.io.yaml import read_yaml
from farms_models.utils import get_sdf_folder

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
POSE_CONFIG_PATH = CONFIG_PATH.joinpath("pose")
RESULTS_PATH = DATA_PATH.joinpath("results")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")
SDF_MODEL_PATH =  DATA_PATH.joinpath("models", "sdf")


def add_floor(**kwargs):
    """Add floor"""
    # Create floor
    floor_offset = kwargs.pop('floor_offset', [0.0, 0.0, -0.05])
    floor_size = kwargs.pop("floor_size", 2e-1)
    floor_tile_size = kwargs.pop("floor_tile_size", 1e-1) # Centimeter tiles
    floor_obj = primitives.create_grid()
    floor_obj.dimensions = [floor_size]*3
    floor_obj.name = "floor"
    # floor_obj.mesh.name = "floor"
    # Add texture
    mat_floor = materials.create_material(
        name="floor", texture=str(Path(
            get_sdf_folder("arena_flat", version="v0")
        ).joinpath("BIOROB2_blue.png"))
    )
    texture_image = mat_floor.node_tree.nodes["Image Texture"]
    texture_coord = mat_floor.node_tree.nodes.new('ShaderNodeTexCoord')
    mapping = mat_floor.node_tree.nodes.new('ShaderNodeMapping')
    mapping.inputs[3].default_value = [floor_size/floor_tile_size]*3
    mat_floor.node_tree.links.new(
        texture_coord.outputs['UV'],
        mapping.inputs['Vector'],
    )
    mat_floor.node_tree.links.new(
        mapping.outputs['Vector'],
        texture_image.inputs['Vector'],
    )
    floor_obj.active_material = mat_floor


def add_lights(**kwargs):
    """ Add lights """
    # Add sun
    sun = bpy.data.lights.new(name="sun", type='SUN')
    sun.energy = kwargs.pop('sun_energy', 4.0)
    sun.angle = kwargs.pop('sun_angle', 3.14)
    sun_obj = bpy.data.objects.new(name="sun", object_data=sun)
    bpy.context.collection.objects.link(sun_obj)
    # background light
    bpy.data.worlds['World'].node_tree.nodes['Background'].inputs[1].default_value = 1.0


def load_model(sdf_name, **kwargs):
    """ Generate model """
    # Load sdf
    model_name, *_, model_objs = sdf.load_sdf(
        SDF_MODEL_PATH.joinpath(sdf_name),
        resources_scale=kwargs.pop("resources_scale", 5e-3)
    )
    return model_name, model_objs


def load_muscles(model_name, muscle_config_name, **kwargs):
    """ Load muscle config"""
    if isinstance(muscle_config_name, str) or isinstance(muscle_config_name, Path):
        muscle_config = MUSCLE_CONFIG_PATH.joinpath(muscle_config_name)
    elif isinstance(muscle_config_name, dict):
        muscle_config =  muscle_config_name
    # Load muscle
    muscle_objs = muscles.load_muscles(
        model_name=model_name,
        muscle_config=muscle_config,
        muscle_radius=kwargs.pop("muscle_radius", 1e-4),
        attachment_radius=kwargs.pop("attachment_radius", 1.5e-4)
    )
    return muscle_objs


def load_scene(**kwargs):
    """ Load the full mouse model with forelimb and hindlimb mucsles """
    model_sdf_name = kwargs.pop("sdf_path", "mouse_with_joint_limits.sdf")
    # Load model
    model_name, model_objs = load_model(model_sdf_name, **kwargs)
    if kwargs.pop("load_fore_muscles", True):
        # Load forelimb muscles
        forelimb_config_name = kwargs.pop("forelimb_config_name", "forelimb.yaml")
        forelimb_muscle_objs = load_muscles(model_name, forelimb_config_name, **kwargs)
    else:
        forelimb_muscle_objs = []
    if kwargs.pop("load_hind_muscles", True):
        # Load hindlimb muscles
        hindlimb_config_name = kwargs.pop("hindlimb_config_name", "hindlimb.yaml")
        hindlimb_muscle_objs = load_muscles(model_name, hindlimb_config_name, **kwargs)
    else:
        hindlimb_muscle_objs = []
    if kwargs.pop("set_pose", True):
        # Set the model pose
        model_pose = read_yaml(
            kwargs.pop(
                "model_pose",
                POSE_CONFIG_PATH.joinpath("neutral_pose_with_joint_constrains.yaml")
            )
        )
        pose.set_model_pose(model_pose, units='degrees')
    if kwargs.pop("add_floor", True):
        # Add floor
        add_floor(**kwargs)
    if kwargs.pop("add_lights", True):
        # Add light
        add_lights(**kwargs)
    return model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs
