""" Generate muscle-data for hindlimb and forelimb. """

import pathlib
import sys
import time

import farms_pylog as pylog
import h5py
import numpy as np

import bpy
from farms_blender.core.utils import clear_world
from farms_blender.core.muscles import (
    extract_muscle_properties_data, get_muscle_joints, analyze_muscle
)
from farms_blender.core.display import display_farms_types
from farms_data.io.yaml import read_yaml
from loading import load_scene

# Global config paths
SCRIPT_PATH = pathlib.Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
CONFIG_PATH = SCRIPT_PATH.joinpath("..", "..", "data", "config")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
MUSCLE_GROUP_CONFIG = MUSCLE_CONFIG_PATH.joinpath(
    "muscle_groups_definition.yaml")
RESULTS_PATH = SCRIPT_PATH.joinpath("..", "..", "data", "results")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")


def setup_scene():
    """ Load mouse model with muscles. """
    # Clear the world
    clear_world()
    # Load animat
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = load_scene()

    # Update display
    # Elements to display. Can be changed later
    display = {'view': True, 'render': False, 'link': False,
               'visual': False, 'collision': False, 'inertial': False,
               'com': False, 'muscle': True, 'joint': False, 'joint_axis': False
               }
    display_farms_types(objs=model_objs, **display)
    return model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs


def main(**kwargs):
    """ Main """
    # Load model
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = setup_scene()

    # Create h5 data-file
    file_path = MUSCLE_RESULTS_PATH.joinpath("main.h5")
    data_file = h5py.File(file_path, 'w')

    # Read muscle names config
    muscle_names = read_yaml(MUSCLE_CONFIG_PATH.joinpath("names.yaml"))

    # Generate muscle groups in data
    for side, muscles in muscle_names.items():
        for muscle in muscles:
            data_file.create_group(f"{side}/{muscle}")

    # Defaults
    muscle_suffix = "RIGHT_{}_{}"
    joint_suffix = "R{}"

    # moment_threshold
    # Minimum moment considered useful produced by the muscle
    moment_threshold = kwargs.pop("moment_threshold", 1e-6)

    # Generate the muscle data
    for side, side_data in data_file.items():
        for muscle, muscle_data in side_data.items():
            # Get muscle object
            muscle_full_name = muscle_suffix.format(side, muscle)
            muscle_obj = bpy.context.scene.objects[muscle_full_name]
            # Update muscle attrs
            for key, value in extract_muscle_properties_data(
                muscle_obj, extract_waypoints=False
            ).items():
                muscle_data.attrs[key] = value
            pylog.debug(f"Analyzing {side.lower()} {muscle_full_name}")
            # Compute data for muscle analysis
            muscle_data.attrs["span_joints"] = [
                joint[1:]
                for joint in get_muscle_joints(model_name, muscle_full_name)
            ]
            active_joints = []
            for joint_name in muscle_data.attrs["span_joints"]:
                joint_full_name = joint_suffix.format(joint_name)
                # Create joint group
                joint_data = muscle_data.create_group(f"{joint_name}")
                for name, value in analyze_muscle(
                    muscle_full_name, joint_full_name, num_steps=25
                ).items():
                    if name == 'moment':
                        joint_moment = value
                    joint_data.create_dataset(name=name, data=value)
                if np.max(np.abs(joint_moment)) > moment_threshold:
                    active_joints.append(joint_name)
            muscle_data.attrs["active_joints"] = active_joints

    # close data-set
    data_file.close()


if __name__ == '__main__':
    import time
    start_time = time.time()
    main()
    print(f"--- {(time.time() - start_time)} seconds ---")
