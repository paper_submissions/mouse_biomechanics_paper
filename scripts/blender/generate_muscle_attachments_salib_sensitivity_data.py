"""

Generate data for muscle attachments sensitivity.

Muscle attachments (origin and insertion) varied by +5% by default

"""

import argparse
import os
import sys
import time
from collections import defaultdict
from pathlib import Path

import farms_pylog as pylog
import h5py
import numpy as np
from SALib.analyze import fast, sobol
from SALib.sample import fast_sampler, saltelli
from tqdm import trange, tqdm

import bpy
import mathutils
from farms_blender.core import scene, utils
from farms_blender.core.muscles import analyze_muscle, get_joints_between_links
from farms_blender.core.display import display_farms_types
from farms_data.io.yaml import read_yaml
from loading import load_scene

# Global config paths
SCRIPT_PATH = Path(__file__).parent.absolute()
sys.path.append(SCRIPT_PATH)
DATA_PATH = SCRIPT_PATH.joinpath("..", "..", "data")
CONFIG_PATH = DATA_PATH.joinpath("config")
RESULTS_PATH = DATA_PATH.joinpath("results")
MUSCLE_CONFIG_PATH = CONFIG_PATH.joinpath("muscles")
MUSCLE_GROUP_CONFIG = MUSCLE_CONFIG_PATH.joinpath(
    "muscle_groups_definition.yaml")
MUSCLE_RESULTS_PATH = RESULTS_PATH.joinpath("muscle_analysis")

pylog.set_level('debug')


def setup_scene():
    """ Load mouse model with muscles. """
    # Clear the world
    utils.clear_world()
    # Load animat
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = load_scene()
    # Update display
    # Elements to display. Can be changed later
    display = {'view': True, 'render': False, 'link': False,
               'visual': False, 'collision': False, 'inertial': False,
               'com': False, 'muscle': True, 'joint': False, 'joint_axis': False
               }
    display_farms_types(objs=model_objs, **display)
    return model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs


def generate_muscle_data(
        limb_name, muscle_name, joint_name, **kwargs
):
    """ Compute muscle-joint data. """
    num_steps = kwargs.pop("num_steps", 10)
    muscle_suffix = "RIGHT_{}_{}"
    joint_suffix = "R{}"
    muscle_full_name = muscle_suffix.format(limb_name, muscle_name)
    joint_full_name = joint_suffix.format(joint_name)
    data = analyze_muscle(
        muscle_full_name, joint_full_name, num_steps=num_steps
    )
    return data['moment_arm'], data['primary_joint_angles']


def get_muscle_attachments(
        model_name: str, limb: str, muscle: str, joint: str
):
    """ set offset to muscle attachments of the via-points that span over
    a joint """
    # Get blender objects from names
    muscle_obj = bpy.context.scene.objects[f"RIGHT_{limb}_{muscle}"]
    joint_obj = bpy.context.scene.objects[f"R{joint}"]
    # Find path points that span over the given joint
    for p1, p2 in zip(muscle_obj.modifiers, muscle_obj.modifiers[1:]):
        # Skip if the consecutive attachments are attached to the same link
        if p1.object.parent.name == p2.object.parent.name:
            continue
        joints = list(get_joints_between_links(
            model_name, p1.object.parent, p2.object.parent
        ))
        if joint in [j.split("_")[2:] for j in joints]:
            break
    return p1, p2


def set_offset_to_muscle_attachments(
        point_1, point_2, offset: tuple
):
    """ set offset to muscle attachments of the via-points that span over
    a joint """
    #: Update the location
    origin_obj = point_1.object
    origin_obj.location = [
        loc + off
        for loc, off in zip(origin_obj.location, offset[:3])
    ]
    insertion_obj = point_2.object
    insertion_obj.location = [
        loc + off
        for loc, off in zip(insertion_obj.location, offset[3:])
    ]


def compute_rms(signal, axis=-1):
    """Compute the rms value between observed moment_arms for each sample
    and the default moment_arm"""
    rms = np.sqrt(np.mean(np.square(signal), axis=axis))
    assert np.all(rms > 0.0), "Negative rms values!"
    return rms


def parse_args():
    """ Parse arguments to blender """
    argv = sys.argv
    argv = argv[argv.index("--") + 1:]
    # Argparse
    parser = argparse.ArgumentParser("SALib-attachment")
    parser.add_argument(
        "--bounds", "-b", type=float, dest="bounds", required=False,
        default=5*1e-4
    )
    args = parser.parse_args(argv)
    return args


def main():
    """ Main """
    # Setup scene
    model_name, model_objs, forelimb_muscle_objs, hindlimb_muscle_objs = setup_scene()

    #: Read the muscle analysis data
    main_data = h5py.File(MUSCLE_RESULTS_PATH.joinpath("main.h5"), "r")

    # Args
    args = parse_args()
    bounds = args.bounds

    #: Initialize
    num_steps = 15
    problem = {
        'num_vars': 6,
        'names': ['origin_x', 'origin_y', 'origin_z',
                  'insertion_x', 'insertion_y', 'insertion_z'],
        'bounds': [[-1*bounds, 1*bounds]]*6
    }
    print(problem['bounds'])
    # Generate samples
    num_samples = 128
    param_values = saltelli.sample(
        problem, num_samples, calc_second_order=False
    )

    #: Create data
    file_path = MUSCLE_RESULTS_PATH.joinpath(
        f"muscle_attachment_salib_{bounds*1e3}mm_sensitivity.h5"
    )
    data_file = h5py.File(file_path, 'w')

    #: Loop over the muscles
    for limb_name, muscles in main_data.items():
        pylog.debug(limb_name)
        for muscle_name, muscle_data in muscles.items():
            pylog.debug(muscle_name)
            for joint_name in muscle_data.attrs['active_joints']:
                pylog.debug(
                    f"Computing SA of {limb_name}_{muscle_name}_{joint_name}"
                )
                # Update data
                iter_data = data_file.create_group(
                    f'{limb_name}/{muscle_name}/{joint_name}'
                )
                #: compute default moment
                default_moment_arm, _ = generate_muscle_data(
                    limb_name, muscle_name, joint_name, num_steps=num_steps
                )
                result = np.zeros((num_steps, len(param_values)))
                # Get the path points that span the joint
                p1, p2 = get_muscle_attachments(
                    model_name, limb_name, muscle_name, joint_name
                )
                for j, param in enumerate(param_values):
                    set_offset_to_muscle_attachments(p1, p2, offset=param)
                    result[:, j], joint_angle = generate_muscle_data(
                        limb_name, muscle_name, joint_name, num_steps=num_steps
                    )
                    set_offset_to_muscle_attachments(p1, p2, offset=-1*param)
                rms = compute_rms(result, axis=0) - \
                    compute_rms(default_moment_arm)
                sobol_si = sobol.analyze(
                    problem, rms, print_to_console=False,
                    calc_second_order=False, parallel=True, n_processors=6
                )
                #: Update data
                iter_data.create_dataset('moment_arms', data=result)
                iter_data.create_dataset('parameters', data=param_values)
                iter_data.create_dataset('joint_angles', data=joint_angle)
                iter_data.create_dataset(
                    'default_moment_arm', data=default_moment_arm)
                iter_data.create_dataset('rms', data=rms)
                for name, value in sobol_si.items():
                    iter_data.create_dataset(f'sobol_{name}', data=value)


if __name__ == '__main__':
    import time
    start_time = time.time()
    main()
    # cProfile.run("main()", "restats")
    # import pstats
    # p = pstats.Stats('restats')
    # p.sort_stats('time').print_stats(10)
    print("--- %s seconds ---" % (time.time() - start_time))
